//TEMPLATE CONFIG, PLEASE KEEP SECRET AT ALL TIMES

module.exports = {
    mysql: {
        // mysql://user:pass@host/db
        connectionLimit : 100,
        host            : process.env.MYSQL_HOST,
        user            : process.env.MYSQL_USER,
        password        : process.env.MYSQL_PASSWORD,
        database        : process.env.MYSQL_DATABASE
    },

    email: {
        service: process.env.EMAIL_SERVICE,
        auth: {
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PASSWORD,
        }
    }
};