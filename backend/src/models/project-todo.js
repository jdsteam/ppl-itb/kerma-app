const mysql = require('mysql');
const moment = require('moment');
const { MysqlManager } =  require('../mysql');

class Todo {
    static getProjectTodos(pid, phase, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` SELECT todo_list.id, todo_list.description, todo_list.is_checked
                        FROM todo_list JOIN phase
                        ON todo_list.phase_id = phase.id AND todo_list.project_id = phase.project_id
                        JOIN project
                        ON project.id = phase.project_id
                        WHERE project.category = ? AND phase.stage_num = ? AND todo_list.project_id = ?
                        ORDER BY todo_list.id ASC
            `;
                        
            connection.query(sql, [category, phase, pid], function(err, results, fields) {
                connection.release();

                if (err) return callbackFunction(err);

                if (results.length == 0) return callbackFunction(null, []);
                return callbackFunction(null, results);
            })
        });
    }

    static addNewTodo(pid, phase, todo, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` SELECT id FROM phase
                        WHERE phase.project_id = ? AND stage_num = ?
            `;

            connection.query(sql, [pid, phase], function(err, results, fields) {
                if (err) return callbackFunction(err);

                if (results.length == 0) return callbackFunction("phase not found");
                var phase_id = results[0].id;

                sql = ` INSERT INTO todo_list (phase_id, project_id, description, is_checked)
                        VALUES (?, ?, ?, ?)
                `;

                connection.query(sql, [phase_id, pid, todo, 0], function(err, results, fields) {
                    if (err) return callbackFunction(err);

                    var sql = ` UPDATE project
                            SET updated_at = NOW()
                            WHERE id = ?
                    `;

                    connection.query(sql, [pid], function(err, results, fields) {
                        connection.release();

                        if (err) return callbackFunction(err);
                    });

                    return callbackFunction(null, results);
                });
            });
        });
    }

    static deleteProjectTodo(pid, todo_id, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` DELETE todo_list
                        FROM todo_list JOIN project
                        ON todo_list.project_id = project.id
                        WHERE project.id = ? AND todo_list.id = ? AND project.category = ?
            `;

            connection.query(sql, [pid, todo_id, category], function(err, results, fields) {
                if (err) return callbackFunction(err);

                var sql = ` UPDATE project
                            SET updated_at = NOW()
                            WHERE id = ?
                `;

                connection.query(sql, [pid], function(err, results, fields) {
                    connection.release();

                    if (err) return callbackFunction(err);
                });

                return callbackFunction(null, results);
            });
        });
    }

    static updateProjectTodo(pid, todo_id, todo, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` UPDATE todo_list JOIN project ON todo_list.project_id = project.id
                        SET todo_list.description = ?
                        WHERE todo_list.id = ? AND project.category = ? AND project.id = ?
            `;

            connection.query(sql, [todo, todo_id, category, pid], function(err, results, fields) {
                if (err) return callbackFunction(err);

                var sql = ` UPDATE project
                            SET updated_at = NOW()
                            WHERE id = ?
                `;

                connection.query(sql, [pid], function(err, results, fields) {
                    connection.release();

                    if (err) return callbackFunction(err);
                });

                return callbackFunction(null, results);
            })

        });
    }

    static toggleTodo(pid, todo_id, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` UPDATE todo_list JOIN project ON todo_list.project_id = project.id
                        SET todo_list.is_checked = (todo_list.is_checked + 1) % 2
                        WHERE todo_list.id = ? AND project.category = ? AND project.id = ?
            `;

            connection.query(sql, [todo_id, category, pid], function(err, results, fields) {
                if (err) return callbackFunction(err);

                var sql = ` UPDATE project
                            SET updated_at = NOW()
                            WHERE id = ?
                `;

                connection.query(sql, [pid], function(err, results, fields) {
                    connection.release();

                    if (err) return callbackFunction(err);
                });

                return callbackFunction(null, results);
            })

        });
    }

}

module.exports = { Todo }