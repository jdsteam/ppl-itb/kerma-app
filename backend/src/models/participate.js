const mysql = require('mysql');
const { MysqlManager } =  require('../mysql');

class Participate {
    static getParticipate(user_id, project_id, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = "SELECT 1 FROM participate WHERE user_id = ? AND project_id = ? LIMIT 1";

            connection.query(sql, [user_id, project_id], function(error, results, fields) {
                connection.release();

                if (error) return callbackFunction(error);

                if (results.length == 0) return callbackFunction();

                return callbackFunction(null, results);
            });
        });
    }
}

module.exports = { Participate }