const mysql = require('mysql');
const moment = require('moment');
const { MysqlManager } =  require('../mysql');

class Project {
    static getProject(pid, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` SELECT project.name, project.curr_phase_stage, project.status
                        FROM project WHERE id = ? AND category = ? LIMIT 1`;

            var inserts = [pid, category];
            sql = mysql.format(sql, inserts);
            connection.query(sql, function(error, results, fields) {
                if (error) return callbackFunction(error);

                if (results.length == 0) return callbackFunction();

                let sql = `SELECT stage_num, due_date FROM phase WHERE ?`;
                let inserts = {project_id: pid};
                sql = mysql.format(sql, inserts);

                connection.query(sql, function(error, phase_results, fields) {
                    connection.release();
                    if (error) return callbackFunction(error);
                    
                    results[0]['phase_deadline'] = {}
                    for (let stage of phase_results) {
                        results[0]['phase_deadline'][stage['stage_num']] = moment(stage['due_date']).format('YYYY-MM-DD HH:mm:ss');
                    }

                    return callbackFunction(null, results[0]);
                });
            });
        });
    }

    static getProjectDetails(pid, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            let final_result;

            let project_promise = new Promise((resolve,reject) => {
                var sql = ` SELECT * FROM project WHERE id = ? AND category = ? LIMIT 1`;

                connection.query(sql, [pid, category], function(err, results, fields) {
                    if (err) return reject(err);

                    resolve(results[0]);
                })
            }).then(project_data => {
                final_result = project_data;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT stage_num, due_date FROM phase WHERE project_id = ?`;

                    connection.query(sql, pid, function(error, phase_results, fields) {
                        if (err) return reject(err);

                        resolve(phase_results);
                    });
                })
            }, err => {
                return callbackFunction(err);
            }).then(phase_rows => {
                final_result['phase_deadline'] = {};
                for (let stage of phase_rows) {
                    final_result['phase_deadline'][stage['stage_num']] = moment(stage['due_date']).format('YYYY-MM-DD HH:mm:ss');
                }

                return new Promise((resolve, reject) => {
                    var sql = ` SELECT perangkat_id from perangkat_involved
                                WHERE project_id = ?
                    `;

                    connection.query(sql, [pid], function(error, phase_results, fields) {
                        if (err) return reject(err);

                        resolve(phase_results);
                    });
                })
            }, err => {
                return callbackFunction(err);
            }).then(perangkat => {
                final_result['perangkat'] = perangkat.map(function(value) {
                    return value.perangkat_id;
                })

                return new Promise((resolve, reject) => {
                    var sql = `
                        SELECT name FROM perangkat ORDER BY id ASC
                    `;

                    connection.query(sql, function(err, perangkat_list, fields) {
                        if (err) return reject(err);

                        resolve(perangkat_list);
                    })
                })
            }, err => {
                return callbackFunction(err);
            }).then(perangkat_list => {
                final_result['list_perangkat'] = perangkat_list.map(value => {
                    return value.name;
                })

                return callbackFunction(null, final_result);
            }, err => {
                return callbackFunction(err);
            });
        });
    }

    //req.query.status, req.query.phase, req.query.sort, req.query.participate
    static getAllProject(category, status, phase, sort, participate, uid, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` SELECT DISTINCT project.id AS pid, name, pic, curr_phase_stage, due_date, project.updated_at 
                        FROM project LEFT JOIN phase ON
                            project_id = project.id AND 
                            stage_num = curr_phase_stage
            `;

            if (participate && participate == 1) {
                sql += ` JOIN (SELECT project_id FROM participate WHERE ?) AS p1 ON p1.project_id = project.id`
                sql = mysql.format(sql, {user_id: uid})
            }

            sql += ` WHERE ?`
            var inserts = {category: category};
            sql = mysql.format(sql, inserts);

            sql += ` AND ?`;
            inserts = (status) ? {status: parseInt(status)} : {status: 1};
            sql = mysql.format(sql, inserts);
            if (phase && phase != 0) {
                sql += ` AND ?`;
                inserts = {curr_phase_stage: parseInt(phase)}; 
                sql = mysql.format(sql, inserts);
            }

            if (sort) {
                switch (parseInt(sort)) {
                    case 1:
                        sql += ` ORDER BY project.id DESC`
                        break;

                    case 2:
                        sql += ` ORDER BY project.updated_at`
                        break;
                    
                    case 3:
                        sql += ` ORDER BY curr_phase_stage`
                        break;
                    
                    case 4:
                        sql += ` ORDER BY name`
                
                    default:
                        break;
                }
            }

            connection.query(sql, function(error, results, fields) {
                connection.release();

                if (error) return callbackFunction(error);

                results.map(function(result) {
                    if (result.due_date) {
                        result.due_date = moment(result.due_date).format('YYYY-MM-DD HH:mm:ss');
                    }
                    if (result.updated_at) {
                        result.updated_at = moment(result.updated_at).format('YYYY-MM-DD HH:mm:ss');
                    }
                });

                return callbackFunction(null, results);
            });
        });
    }

    static addProject(name, category, pic_name, cp_name, company_name, company_address, cp_phone, cp_email, description,
        country,
        due_date1,
        due_date2,
        due_date3,
        due_date4,
        due_date5,
        due_date6,
        due_date7,
        perangkat,
        callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            
            if (err) return callbackFunction(err);

            let capitalize = function (string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            }
            var sql = "INSERT INTO project SET ?";
            var inserts = { name: name, 
                            category: category, 
                            pic: pic_name, 
                            curr_phase_stage: 1, 
                            company_name: company_name,
                            company_address: company_address, 
                            cp_name: cp_name, 
                            cp_phone: cp_phone, 
                            cp_email: cp_email, 
                            description: description,
                            country: capitalize(country)};
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(err, results, fields) {
                if (err) return callbackFunction(err);
            });

            let promise = new Promise((resolve, reject) => {
                var sql = ` SELECT id FROM project WHERE name = ?
                `;

                connection.query(sql, [name], function(err, results, fields) {
                    if (err) return reject(err);

                    resolve(results[0].id);
                })
            })

            promise = promise.then(pid => {
                let len;
                if(typeof(perangkat) === 'undefined') {
                    len = 0;
                } else {
                    len = perangkat.length;
                }
                for(var i = 0; i < len; i++) {
                    var sql = ` INSERT INTO perangkat_involved
                                SET project_id = ?, perangkat_id = ?
                    `;
                    
                    connection.query(sql, [pid, perangkat[i]], function(err, results, fields) {
                        if (err) return callbackFunction(err);
                    });
                }
            })

            sql = ` SELECT project.id as 'pid', user.id as 'uid' 
                    FROM project JOIN user ON project.pic = user.username
                    WHERE project.name = ?
            `;
            connection.query(sql, name, function(error, results, fields) {
                
                var new_project_id = results[0].pid;
                var pic_id = results[0].uid

                sql = "INSERT INTO participate SET ?";
                inserts = {user_id: pic_id, project_id: new_project_id};
                sql = mysql.format(sql, inserts);
    
                connection.query(sql, function(err, results, fields) {
                    if (err) return callbackFunction(err);
                });

                var due_dates = [due_date1,due_date2,due_date3,due_date4,due_date5,due_date6,due_date7];
                for (var i = 1; i <= 7; i++) { 
                    sql = "INSERT INTO phase (project_id, stage_num, due_date) values (?, ?, STR_TO_DATE(?, '%Y-%m-%d'))";
                    var inserts = [new_project_id, i, due_dates[i-1]];

                    sql = mysql.format(sql, inserts);

                    if (i==7) {
                        connection.query(sql, function(err, results, fields) {
                            connection.release();
                            if (err) return callbackFunction(err);

                            return callbackFunction(null);
                        });
                    } else {
                        connection.query(sql, function(err, results, fields) {
                            if (err) return callbackFunction(err);
                        });
                    }
                }
            })
        });
    }

    static editProject(pid, name, category, pic_name, cp_name, company_name, company_address, cp_phone, cp_email, description, country,
        due_date1,
        due_date2,
        due_date3,
        due_date4,
        due_date5,
        due_date6,
        due_date7, 
        perangkat,
        updated_at, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            let capitalize = function (string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            }

            var sql = "UPDATE project SET name = ?, category = ?, pic = ?, company_name = ?, company_address = ?, cp_name = ?, cp_phone = ?, cp_email = ?, description = ?, country = ?, updated_at = STR_TO_DATE(?, '%Y-%m-%d') + INTERVAL 1 DAY WHERE id = ?";
            var inserts = [name, category, pic_name, company_name, company_address, cp_name, cp_phone, cp_email, description, capitalize(country), updated_at, pid];
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(error, results, fields) {
                if (error) return connection.rollback(function() {
                    return callbackFunction(error);
                });
            });

            let promise = new Promise((resolve, reject) => {
                var sql = `DELETE FROM perangkat_involved WHERE project_id = ?`;

                connection.query(sql, pid, function(err, results, fields) {
                    if (err)return callbackFunction(err);
                    resolve();
                })
            })

            promise = promise.then(() => {
                var sql = ` INSERT INTO perangkat_involved (project_id, perangkat_id)
                            VALUES ?
                `;

                perangkat = perangkat.map(function(value) {
                    return [Number(pid), Number(value)];
                })

                connection.query(sql, [perangkat], function(err, results, fields) {
                    if (err) return callbackFunction(err);
                    return;
                })
            }).then(() => {
                var sql_2 = "SELECT id FROM phase WHERE project_id = ? ORDER BY stage_num ASC";
                connection.query(sql_2, pid, function(error, results, fields) {

                    var len = results.length;
                    var due_dates = [due_date1,due_date2,due_date3,due_date4,due_date5,due_date6,due_date7];

                    for (var i = 1; i <= len; i++) { 
                        var sql_3 = "UPDATE phase SET due_date = STR_TO_DATE(?, '%Y-%m-%d') where project_id = ? AND stage_num = ?";
                        var inserts_3 = [due_dates[i-1], pid, i];

                        sql_3 = mysql.format(sql_3, inserts_3);
                        if (i == len) {
                            connection.query(sql_3, function(err, results, fields) {
                                if (err) return connection.rollback(function() {
                                    return callbackFunction(error);
                                });
                                return callbackFunction(null, results);
                            });
                        } else {
                            connection.query(sql_3, function(err, results, fields) {
                                if (err) return connection.rollback(function() {
                                    return callbackFunction(error);
                                });
                            });
                        }
                    }
                });
            });
        });
    }

    static deleteProject(pid, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = "DELETE FROM todo_list WHERE ?";
            var inserts = {project_id: pid};
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(error, results, fields) {
                if (error) return callbackFunction(error);
            });

            var sql = "DELETE FROM photos WHERE ?";
            var inserts = {project_id: pid};
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(error, results, fields) {
                if (error) return callbackFunction(error);
            });

            var sql = "DELETE FROM participate WHERE ?";
            var inserts = {project_id: pid};
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(error, results, fields) {
                if (error) return callbackFunction(error);
            });

            var sql = "DELETE FROM documents WHERE ?";
            var inserts = {project_id: pid};
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(error, results, fields) {
                if (error) return callbackFunction(error);
            });

            var sql = "DELETE FROM comments WHERE ?";
            var inserts = {project_id: pid};
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(error, results, fields) {
                if (error) return callbackFunction(error);
            });

            var sql = "DELETE FROM phase WHERE ?";
            var inserts = {project_id: pid};
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(error, results, fields) {
                if (error) return callbackFunction(error);
            });

            sql = "DELETE FROM project WHERE ?";
            inserts = {id: pid};
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(error, results, fields) {
                connection.release();

                if (error) return callbackFunction(error);

                return callbackFunction(null);
            });
        });
    }

    static changeProjectStatus(pid, new_status, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` UPDATE project SET status = ?
                        WHERE id = ? AND category = ?
            `;

            connection.query(sql, [new_status, pid, category], function(err, results, fields) {
                connection.release();
                if (err) return callbackFunction(err);

                return callbackFunction(null, results);
            })

        });        
    }

    static getProjectParticipants(pid, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` SELECT user.id AS id, user.name AS name
                        FROM user JOIN participate
                        ON participate.user_id = user.id
                        WHERE participate.project_id = ?
            `;

            connection.query(sql, [pid], function(err, results, fields) {
                connection.release();

                if (err) return callbackFunction(err);

                if (results.length == 0) return callbackFunction(null, []);

                var users = results.map(function(row) {
                    var user_object = new Object();
                    user_object["name"] = row.name;
                    user_object["uid"] = row.id;
                    return user_object;
                })

                return callbackFunction(null, users);
            })
        });
    }

    static deleteProjectParticipant(pid, uid, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` DELETE FROM participate
                        WHERE user_id = ? AND project_id = ?
            `;

            connection.query(sql, [uid, pid], function(err, results, fields) {
                connection.release();

                if (err) return callbackFunction(err);

                if (results.length == 0) return callbackFunction(null, []);
                return callbackFunction(null, results);
            })
        });    
    }

    static getProjectStatus(pid, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` SELECT status FROM project WHERE id = ?`;

            connection.query(sql, [pid], function(err, results, fields) {
                connection.release();

                if (err) return callbackFunction(err);

                if (results.length == 0) return callbackFunction();
                return callbackFunction(null, results[0]['status']);
            })
        });  
    }

    static getProjectPic(pid, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` SELECT pic FROM project WHERE id = ?`;

            connection.query(sql, [pid], function(err, results, fields) {
                connection.release();

                if (err) return callbackFunction(err);

                if (results.length == 0) return callbackFunction();
                return callbackFunction(null, results[0]['pic']);
            })
        });  
    }

    static changeProjectPhase(pid, new_phase, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` UPDATE project
                        SET project.curr_phase_stage = ?
                        WHERE project.id = ? AND project.category = ?
            `;

            connection.query(sql, [new_phase, pid, category], function(err, results, fields) {
                if (err) return callbackFunction(err);
                
                var sql = ` UPDATE project
                            SET updated_at = NOW()
                            WHERE id = ?
                `;

                connection.query(sql, [pid], function(err, results, fields) {
                    connection.release();

                    if (err) return callbackFunction(err);
                });

                return callbackFunction(null, results);
            })
        });  
    }

    static addParticipant(pid, participants, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            participants = participants.map(function(value) {
                return "'" + value + "'";
            });

            let joiner = function(arr) {
                let len = arr.length;
                let result = arr[0];
                for (var i = 1; i < len; i++) {
                    result += ',' + arr[i];
                }
                return result;
            };

            participants = joiner(participants);

            var sql = ` SELECT user.id as 'id'
                        FROM user
                        WHERE user.username IN (` + participants + ")";

            connection.query(sql, function(err, results, fields) {
                if (err) return callbackFunction(err);
                
                var insert_values = results.map(function(value) {
                    return [value.id, pid];
                })

                var sql = ` INSERT INTO participate (user_id, project_id) VALUES ?`;

                connection.query(sql, [insert_values], function(err, results, fields) {
                    if (err) return callbackFunction(err);

                    return callbackFunction(null, results);
                });
            })
        });  
    }

    static changeLastUpdate(pid, new_date, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` UPDATE project
                        SET project.updated_at = STR_TO_DATE(?, '%Y-%m-%d')
                        WHERE project.id = ? AND project.category = ?
            `;

            connection.query(sql, [new_date, pid, category], function(err, results, fields) {
                if (err) return callbackFunction(err);

                return callbackFunction(null, results);
            })
        });
    }
}

module.exports = { Project }