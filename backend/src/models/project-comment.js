const mysql = require('mysql');
const moment = require('moment');
const { MysqlManager } =  require('../mysql');

class Comment {
    static getProjectComments(pid, phase, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` SELECT comments.id, comments.comment
                        FROM comments JOIN phase
                        ON comments.phase_id = phase.id AND comments.project_id = phase.project_id
                        JOIN project
                        ON project.id = phase.project_id
                        WHERE project.category = ? AND phase.stage_num = ? AND comments.project_id = ?
                        ORDER BY comments.id ASC
            `;
                        
            connection.query(sql, [category, phase, pid], function(err, results, fields) {
                connection.release();

                if (err) return callbackFunction(err);

                if (results.length == 0) return callbackFunction(null, []);
                return callbackFunction(null, results);
            })
        });
    }

    static addNewComment(pid, phase, comment, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` SELECT id FROM phase
                        WHERE phase.project_id = ? AND stage_num = ?
            `;

            connection.query(sql, [pid, phase], function(err, results, fields) {
                if (err) return callbackFunction(err);

                if(results.length == 0) return callbackFunction("Phase not found");
                var phase_id = results[0].id;

                sql = ` INSERT INTO comments (phase_id, project_id, comment)
                        VALUES (?, ?, ?)
                `;

                connection.query(sql, [phase_id, pid, comment], function(err, results, fields) {
                    if (err) return callbackFunction(err);
                    var sql = ` UPDATE project
                            SET updated_at = NOW()
                            WHERE id = ?
                    `;

                    connection.query(sql, [pid], function(err, results, fields) {
                        connection.release();

                        if (err) return callbackFunction(err);
                    });
                    return callbackFunction(null, results);
                })
            })
        })
    }

    static deleteProjectComment(pid, comment_id, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` DELETE comments
                        FROM comments JOIN project
                        ON comments.project_id = project.id
                        WHERE project.id = ? AND comments.id = ? AND project.category = ?
            `;

            connection.query(sql, [pid, comment_id, category], function(err, results, fields) {
                if (err) return callbackFunction(err);
                if (results.length == 0) return callbackFunction(null, []);

                var sql = ` UPDATE project
                            SET updated_at = NOW()
                            WHERE id = ?
                `;

                connection.query(sql, [pid], function(err, results, fields) {
                    connection.release();

                    if (err) return callbackFunction(err);
                });
                return callbackFunction(null, results);
            })
        })
    }

}

module.exports = { Comment }