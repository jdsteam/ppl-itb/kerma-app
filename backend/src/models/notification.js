const { MysqlManager } =  require('../mysql');
const nodemailer = require('nodemailer');
const moment = require('moment');
const config = require('../../config');
const { CATEGORIES } = require('../enum');

moment.locale('id');

class Notification {
    static sendDailyNotification(callbackFunction) {
        let transporter = nodemailer.createTransport(config.email);

        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);
            
            var sql = `SELECT id, name, email FROM user`;

            connection.query(sql, function(err, results, fields) {
                connection.release();
                if (err) return callbackFunction(err);

                for (let result of results) {
                    MysqlManager.getConnection(function(err, connection) {
                        if (err) return callbackFunction(err);

                        let resultDict = {username: result['username']}

                        let sql = ` SELECT status, COUNT(project.id) AS count_project
                                    FROM project JOIN (SELECT project_id FROM participate WHERE user_id = ?) AS project_participate
                                    ON project.id = project_participate.project_id
                                    WHERE TO_DAYS(project.updated_at) = TO_DAYS(NOW())
                                    GROUP BY project.status `;
                        
                        connection.query(sql, [result['id']], function(err, results, fields) {
                            if (err) return callbackFunction(err);

                            for (let result of results) {
                                resultDict[result['status']] = result['count_project'];
                            }

                            if (!resultDict[1]) resultDict[1] = 0;
                            if (!resultDict[2]) resultDict[2] = 0;
                            if (!resultDict[3]) resultDict[3] = 0;

                            let message = 'Kepada ' + result['name'] + ',\n\n'
                            message += 'Berikut pengingat harian pada '+ moment().format("dddd, D MMMM YYYY, h:mm:ss a") +' untuk proyek dimana Anda berpartisipasi.\n\n'
                            message += 'Jumlah proyek yang berprogres: ' + resultDict[1] +'\n'
                            message += 'Jumlah proyek yang selesai hari ini: ' + resultDict[2] +'\n'
                            message += 'Jumlah proyek yang diberhentikan hari ini: ' + resultDict[3] +'\n\n'
                            message += 'Terima kasih.\n\n'
                            message += '-KERMA APP'

                            let mailData = {
                                from: 'birokerjasama.test@gmail.com',
                                to: result['email'],
                                subject: 'Daily Message Notification, ' + moment().format("D MMMM YYYY"),
                                text: message
                            };
    
                            transporter.sendMail(mailData, function(err, res) {
                                if (err) return callbackFunction(err);
                            });
                        });
                    })
                }
            })
        });

        return callbackFunction(null);
    }

    static sendMonthlyNotification(callbackFunction) {
        let transporter = nodemailer.createTransport(config.email);

        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            let monthly_data = new Object();

            let promise = new Promise((resolve, reject) => {
                var sql = ` SELECT COUNT(project.id) as "ongoing"
                            FROM project
                            WHERE status = 1 AND category = ? AND created_at < LAST_DAY(NOW()) - INTERVAL 30 DAY;
                `;

                connection.query(sql, [CATEGORIES.INVESTASI], function(err, results, fields) {
                    if (err) return reject(err);

                    resolve(results[0].ongoing);
                })
            });

            promise = promise.then(ongoing_investasi => {
                monthly_data["investasi"] = new Object();
                monthly_data["investasi"]["ongoing"] = ongoing_investasi;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as "finished"
                            FROM project
                            WHERE status = 2 AND category = ? AND updated_at >= LAST_DAY(NOW()) - INTERVAL 30 DAY;
                    `;

                    connection.query(sql, [CATEGORIES.INVESTASI], function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].finished);
                    })
                })
            });

            promise = promise.then(finished_investasi => {
                monthly_data["investasi"]["finished"] = finished_investasi;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as "cancelled"
                            FROM project
                            WHERE status = 3 AND category = ? AND updated_at >= LAST_DAY(NOW()) - INTERVAL 30 DAY;
                    `;

                    connection.query(sql, [CATEGORIES.INVESTASI], function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].cancelled);
                    })
                })
            });

            promise = promise.then(cancelled_investasi => {
                monthly_data["investasi"]["cancelled"] = cancelled_investasi;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as "new"
                            FROM project
                            WHERE status = 1 AND category = ? AND created_at >= LAST_DAY(NOW()) - INTERVAL 30 DAY;
                    `;

                    connection.query(sql, [CATEGORIES.INVESTASI], function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].new);
                    })
                })
            });

            promise = promise.then(new_investasi => {
                monthly_data["investasi"]["new"] = new_investasi;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as "ongoing"
                            FROM project
                            WHERE status = 1 AND category = ? AND created_at < LAST_DAY(NOW()) - INTERVAL 30 DAY;
                `;

                    connection.query(sql, [CATEGORIES.SOSIAL], function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].ongoing);
                    })
                })
            });

            promise = promise.then(ongoing_sosial => {
                monthly_data["sosial"] = new Object();
                monthly_data["sosial"]["ongoing"] = ongoing_sosial;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as "finished"
                            FROM project
                            WHERE status = 2 AND category = ? AND updated_at >= LAST_DAY(NOW()) - INTERVAL 30 DAY;
                    `;

                    connection.query(sql, [CATEGORIES.SOSIAL], function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].finished);
                    })
                })
            });

            promise = promise.then(finished_sosial => {
                monthly_data["sosial"]["finished"] = finished_sosial;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as "cancelled"
                            FROM project
                            WHERE status = 3 AND category = ? AND updated_at >= LAST_DAY(NOW()) - INTERVAL 30 DAY;
                    `;

                    connection.query(sql, [CATEGORIES.SOSIAL], function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].cancelled);
                    })
                })
            });

            promise = promise.then(cancelled_sosial => {
                monthly_data["sosial"]["cancelled"] = cancelled_sosial;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as "new"
                            FROM project
                            WHERE status = 1 AND category = ? AND created_at >= LAST_DAY(NOW()) - INTERVAL 30 DAY;
                    `;

                    connection.query(sql, [CATEGORIES.SOSIAL], function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].new);
                    })
                })
            });

            promise = promise.then(new_sosial => {
                monthly_data["sosial"]["new"] = new_sosial;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as "ongoing"
                            FROM project
                            WHERE status = 1 AND category = ? AND created_at < LAST_DAY(NOW()) - INTERVAL 30 DAY;
                `;

                    connection.query(sql, [CATEGORIES.DAGANG], function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].ongoing);
                    })
                })
            });

            promise = promise.then(ongoing_dagang => {
                monthly_data["dagang"] = new Object();
                monthly_data["dagang"]["ongoing"] = ongoing_dagang;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as "finished"
                            FROM project
                            WHERE status = 2 AND category = ? AND updated_at >= LAST_DAY(NOW()) - INTERVAL 30 DAY;
                    `;

                    connection.query(sql, [CATEGORIES.DAGANG], function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].finished);
                    })
                })
            });

            promise = promise.then(finished_dagang => {
                monthly_data["dagang"]["finished"] = finished_dagang;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as "cancelled"
                            FROM project
                            WHERE status = 3 AND category = ? AND updated_at >= LAST_DAY(NOW()) - INTERVAL 30 DAY;
                    `;

                    connection.query(sql, [CATEGORIES.DAGANG], function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].cancelled);
                    })
                })
            });

            promise = promise.then(cancelled_dagang => {
                monthly_data["dagang"]["cancelled"] = cancelled_dagang;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as "new"
                            FROM project
                            WHERE status = 1 AND category = ? AND created_at >= LAST_DAY(NOW()) - INTERVAL 30 DAY;
                    `;

                    connection.query(sql, [CATEGORIES.DAGANG], function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].new);
                    })
                })
            });

            promise = promise.then(new_dagang => {
                monthly_data["dagang"]["new"] = new_dagang;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as "ongoing"
                            FROM project
                            WHERE status = 1 AND category = ? AND created_at < LAST_DAY(NOW()) - INTERVAL 30 DAY;
                `;

                    connection.query(sql, [CATEGORIES.WISATA], function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].ongoing);
                    })
                })
            });

            promise = promise.then(ongoing_wisata => {
                monthly_data["wisata"] = new Object();
                monthly_data["wisata"]["ongoing"] = ongoing_wisata;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as "finished"
                            FROM project
                            WHERE status = 2 AND category = ? AND updated_at >= LAST_DAY(NOW()) - INTERVAL 30 DAY;
                    `;

                    connection.query(sql, [CATEGORIES.WISATA], function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].finished);
                    })
                })
            });

            promise = promise.then(finished_wisata => {
                monthly_data["wisata"]["finished"] = finished_wisata;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as "cancelled"
                            FROM project
                            WHERE status = 3 AND category = ? AND updated_at >= LAST_DAY(NOW()) - INTERVAL 30 DAY;
                    `;

                    connection.query(sql, [CATEGORIES.WISATA], function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].cancelled);
                    })
                })
            });

            promise = promise.then(cancelled_wisata => {
                monthly_data["wisata"]["cancelled"] = cancelled_wisata;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as "new"
                            FROM project
                            WHERE status = 1 AND category = ? AND created_at >= LAST_DAY(NOW()) - INTERVAL 30 DAY;
                    `;

                    connection.query(sql, [CATEGORIES.WISATA], function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].new);
                    })
                })
            });

            promise = promise.then(new_wisata => {
                monthly_data["wisata"]["new"] = new_wisata;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT user.email as 'email', has_role.role as 'role'
                                FROM
                                (
                                    SELECT has_role.user_id as 'user_id', has_role.role as 'role'
                                    FROM
                                        user JOIN has_role ON has_role.user_id = user.id
                                    WHERE has_role.role = 6
                                ) AS managers
                                JOIN user ON user.id = managers.user_id
                                JOIN has_role ON has_role.user_id = managers.user_id
                                WHERE has_role.role != 6
                                ORDER BY managers.user_id
                    `;

                    connection.query(sql, function(err, results, fields) {
                        if (err) return reject(err);
                        resolve(results);
                    })
                })
            });

            Promise.all([promise]);

            let managers;
            promise = promise.then(manager_list => {
                manager_list = manager_list.map(function(value) {
                    var manager = new Object();
                    manager["email"] = value.email;
                    switch(value.role) {
                        case 1:
                            manager["role"] = "investasi";
                            break;
                        case 2:
                            manager["role"] = "sosial";
                            break;
                        case 3:
                            manager["role"] = "dagang";
                            break;
                        case 4:
                            manager["role"] = "wisata";
                            break;
                        default:
                            break;
                    }
                    return manager
                })
                managers = manager_list;
            });

            promise = promise.then(() => {
                let prev = '';
                let emailHeaderText = 'Berikut pengingat bulanan anda';
                let investasiEmailText = 'Kategori Investasi\n';
                investasiEmailText += '--------------------------------------\n';
                investasiEmailText += 'Proyek baru: ' + monthly_data["investasi"]["new"].toString() + '\n';
                investasiEmailText += 'Proyek dalam progress: ' + monthly_data["investasi"]["ongoing"].toString() + '\n';
                investasiEmailText += 'Proyek selesai: ' + monthly_data["investasi"]["finished"].toString() + '\n';
                investasiEmailText += 'Proyek dibatalkan: ' + monthly_data["investasi"]["cancelled"].toString() + '\n\n';

                let sosialEmailText = 'Kategori Sosial\n';
                sosialEmailText += '--------------------------------------\n';
                sosialEmailText += 'Proyek baru: ' + monthly_data["sosial"]["new"].toString() + '\n';
                sosialEmailText += 'Proyek dalam progress: ' + monthly_data["sosial"]["ongoing"].toString() + '\n';
                sosialEmailText += 'Proyek selesai: ' + monthly_data["sosial"]["finished"].toString() + '\n';
                sosialEmailText += 'Proyek dibatalkan: ' + monthly_data["sosial"]["cancelled"].toString() + '\n\n';

                let dagangEmailText = 'Kategori Dagang\n';
                dagangEmailText += '--------------------------------------\n';
                dagangEmailText += 'Proyek baru: ' + monthly_data["dagang"]["new"].toString() + '\n';
                dagangEmailText += 'Proyek dalam progress: ' + monthly_data["dagang"]["ongoing"].toString() + '\n';
                dagangEmailText += 'Proyek selesai: ' + monthly_data["dagang"]["finished"].toString() + '\n';
                dagangEmailText += 'Proyek dibatalkan: ' + monthly_data["dagang"]["cancelled"].toString() + '\n\n';

                let wisataEmailText = 'Kategori Wisata\n';
                wisataEmailText += '--------------------------------------\n';
                wisataEmailText += 'Proyek baru: ' + monthly_data["wisata"]["new"].toString() + '\n';
                wisataEmailText += 'Proyek dalam progress: ' + monthly_data["wisata"]["ongoing"].toString() + '\n';
                wisataEmailText += 'Proyek selesai: ' + monthly_data["wisata"]["finished"].toString() + '\n';
                wisataEmailText += 'Proyek dibatalkan: ' + monthly_data["wisata"]["cancelled"].toString() + '\n\n';
                
                let emailFooterText = 'Terima kasih.\n\n';
                emailFooterText += '-KERMA APP'

                let message = emailHeaderText;

                for (var i = 0; i < Object.keys(managers).length; i++) {
                    if (prev != managers[i].email) {
                        message += emailFooterText;
                        let mailData = {
                            from: 'birokerjasama.test@gmail.com',
                            to: prev,
                            subject: 'Monthly Message Notification',
                            text: message
                        };

                        if (prev != '') {
                            transporter.sendMail(mailData, function(err, res) {
                                if (err) return callbackFunction(err);
                            });
                        }

                        prev = managers[i].email;
                        message = emailHeaderText;
                    }

                    if(managers[i].role == 'wisata') {
                        message += wisataEmailText;
                    } else if(managers[i].role == 'dagang') {
                        message += dagangEmailText;
                    } else if(managers[i].role == 'investasi') {
                        message += investasiEmailText;
                    } else if(managers[i].role == 'sosial') {
                        message += sosialEmailText;
                    }
                }

                message += emailFooterText;

                let mailData = {
                    from: 'birokerjasama.test@gmail.com',
                    to: prev,
                    subject: 'Monthly Message Notification',
                    text: message
                };

                transporter.sendMail(mailData, function(err, res) {
                    if (err) return callbackFunction(err);
                });
            })
        });
    }
}

module.exports = { Notification }