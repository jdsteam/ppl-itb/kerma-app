const mysql = require('mysql');
const moment = require('moment');
const { MysqlManager } =  require('../mysql');

class Document {
    static getProjectDocuments(pid, phase, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` SELECT documents.id, documents.name, documents.document
                        FROM documents JOIN phase
                        ON documents.phase_id = phase.id AND documents.project_id = phase.project_id
                        JOIN project
                        ON project.id = phase.project_id
                        WHERE project.category = ? AND phase.stage_num = ? AND documents.project_id = ?
            `;
                        
            connection.query(sql, [category, phase, pid], function(err, results, fields) {
                connection.release();

                if (err) return callbackFunction(err);

                if (results.length == 0) return callbackFunction(null, []);
                return callbackFunction(null, results);
            });
        });
    }

    static addNewDocument(pid, phase, name, document, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` SELECT id FROM phase
                        WHERE phase.project_id = ? AND stage_num = ?
            `;

            connection.query(sql, [pid, phase], function(err, results, fields) {
                if (err) return callbackFunction(err);

                if (results.length == 0) return callbackFunction("phase not found");
                var phase_id = results[0].id;

                sql = ` INSERT INTO documents (phase_id, project_id, name, document)
                        VALUES (?, ?, ?, ?)
                `;

                connection.query(sql, [phase_id, pid, name, document], function(err, results, fields) {
                    if (err) return callbackFunction(err);

                    var sql = ` UPDATE project
                            SET updated_at = NOW()
                            WHERE id = ?
                    `;

                    connection.query(sql, [pid], function(err, results, fields) {
                        connection.release();

                        if (err) return callbackFunction(err);
                    });

                    return callbackFunction(null, results);
                });
            });
        });
    }

    static deleteProjectDocument(pid, document_id, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` DELETE documents
                        FROM documents JOIN project
                        ON documents.project_id = project.id
                        WHERE project.id = ? AND documents.id = ? AND project.category = ?
            `;

            connection.query(sql, [pid, document_id, category], function(err, results, fields) {
                if (err) return callbackFunction(err);

                var sql = ` UPDATE project
                            SET updated_at = NOW()
                            WHERE id = ?
                `;

                connection.query(sql, [pid], function(err, results, fields) {
                    connection.release();

                    if (err) return callbackFunction(err);
                });

                return callbackFunction(null, results);
            });
        });
    }

    static updateProjectDocument(pid, document_id, name, document, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` UPDATE documents JOIN project ON documents.project_id = project.id
                        SET documents.name = ?, documents.document = ?
                        WHERE documents.id = ? AND project.category = ? AND project.id = ?
            `;

            connection.query(sql, [name, document, document_id, category, pid], function(err, results, fields) {
                if (err) return callbackFunction(err);

                var sql = ` UPDATE project
                            SET updated_at = NOW()
                            WHERE id = ?
                `;

                connection.query(sql, [pid], function(err, results, fields) {
                    connection.release();

                    if (err) return callbackFunction(err);
                });

                return callbackFunction(null, results);
            })

        });
    }
}

module.exports = { Document }