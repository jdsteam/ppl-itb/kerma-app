const mysql = require('mysql');
const { MysqlManager } =  require('../mysql');

class User {
    static getUser(id, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = "SELECT name, username, email FROM user WHERE ? LIMIT 1";
            var inserts = {id};
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(error, results, fields) {
                connection.release();

                if (error) return callbackFunction(error);

                if (results.length == 0) return callbackFunction();

                var sql = "SELECT role FROM has_role WHERE user_id = ?";
                var inserts = [id];
                sql = mysql.format(sql, inserts);

                connection.query(sql, function(error, role_results, fields) {
                    if (error) return callbackFunction(error);
                    var role_arr = [];
                    role_results.map(function(role) {
                        role_arr.push(role.role);
                    })
                    results[0]['roles'] = role_arr;
                    return callbackFunction(null, results[0]);
                })
            });
        });
    }

    static getUserLogin(username, password, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = "SELECT id FROM user WHERE username = ? AND password = ? LIMIT 1";
            var inserts = [username, password];
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(error, results, fields) {
                connection.release();

                if (error) return callbackFunction(error);

                if (results.length == 0) return callbackFunction();
                return callbackFunction(null, results[0]);
            });
        });
    }

    static getAllUser(callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = "SELECT id AS uid, name, username, email FROM user";

            connection.query(sql, function(error, results, fields) {
                connection.release();

                if (error) return callbackFunction(error);

                let promises = results.map(function(result) {
                    return new Promise((resolve, reject) => {
                        var sql = "SELECT role FROM has_role WHERE user_id = ?";
                        var inserts = [result.uid];
                        sql = mysql.format(sql, inserts);

                        connection.query(sql, function(error, role_results, fields) {
                            if (error) return reject(error)
                            else {
                                var result_arr = [];
                                role_results.map(function(role) {
                                    result_arr.push(role.role);
                                })
                                resolve(result_arr);
                            }
                        });
                    }).then(result_arr => {
                        result['roles'] = result_arr;
                        return result;
                    });
                });

                Promise.all(promises).then(results => {
                    return callbackFunction(null, results);
                })
                .catch(e => {
                    return callbackFunction(e);
                });
            });
        });
    }

    static addUser(name, username, email, password, roleArr, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            connection.beginTransaction(function(err) {
                if (err) return callbackFunction(err);

                var sql_1 = "INSERT INTO user SET ?";
                var inserts_1 = {name: name, username: username, email: email, password:password};
                sql_1 = mysql.format(sql_1, inserts_1);

                connection.query(sql_1, function(error, results, fields) {
                    if (error) return connection.rollback(function() {
                        return callbackFunction(error);
                    });

                    if (!roleArr) roleArr = [];
                    let inserts_2 = [];
                    for (let role of roleArr) {
                        inserts_2.push([results.insertId, role]);
                    }

                    var sql_2 = "INSERT INTO has_role (user_id, role) VALUES ?";

                    if (inserts_2.length != 0) {
                        connection.query(sql_2, [inserts_2], function(error, results_role, fields) {
                            if (error) return connection.rollback(function() {
                                return callbackFunction(error);
                            });

                            connection.commit(function(err) {
                                if (err) return connection.rollback(function() {
                                    return callbackFunction(err);
                                });

                                return callbackFunction(null);
                            });
                        });
                    } else {
                        connection.commit(function(err) {
                            if (err) return connection.rollback(function() {
                                return callbackFunction(err);
                            });

                            return callbackFunction(null);
                        }); 
                    }
                });
            })
        });
    }

    static editUser(id, name, username, email, password, roleArr, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            connection.beginTransaction(function(err) {
                if (err) return callbackFunction(err);

                if (password == "") {
                    var sql = "UPDATE user SET name = ?, username = ?, email = ? WHERE id = ?";
                    var inserts = [name, username, email, id];
                }
                else {
                    var sql = "UPDATE user SET name = ?, username = ?, email = ?, password = ? WHERE id = ?";
                    var inserts = [name, username, email, password, id];
                }
                sql = mysql.format(sql, inserts);

                connection.query(sql, function(error, results, fields) {
                    if (error) return connection.rollback(function() {
                        return callbackFunction(error);
                    });

                    var sql_1 = "DELETE FROM has_role WHERE user_id = ?";
                    var inserts_1 = [id];
                    sql_1 = mysql.format(sql_1, inserts_1);

                    connection.query(sql_1, function(error, results, fields) {
                        if (error) return connection.rollback(function() {
                            return callbackFunction(error);
                        });

                        var sql_2 = "INSERT INTO has_role (user_id, role) VALUES ?";

                        if (!roleArr) roleArr = [];
                        let inserts_2 = [];
                        for (let role of roleArr) {
                            inserts_2.push([id, role]);
                        }

                        if (inserts_2.length != 0) {
                            connection.query(sql_2, [inserts_2], function(error, results_role, fields) {
                                if (error) return connection.rollback(function() {
                                    return callbackFunction(error);
                                });

                                connection.commit(function(err) {
                                    if (err) return connection.rollback(function() {
                                        return callbackFunction(err);
                                    });

                                    return callbackFunction(null);
                                });
                            });
                        } else {
                            connection.commit(function(err) {
                                if (err) return connection.rollback(function() {
                                    return callbackFunction(err);
                                });

                                return callbackFunction(null);
                            });
                        }
                    });
                });
            });
        });
    }

    static deleteUser(id, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            connection.beginTransaction(function(err) {
                if (err) return callbackFunction(err);

                var sql = "DELETE FROM user WHERE ?";
                var inserts = {id};
                sql = mysql.format(sql, inserts);

                connection.query(sql, function(error, results, fields) {

                    if (error) return connection.rollback(function() {
                        return callbackFunction(error);
                    });

                    var sql_1 = "DELETE FROM has_role WHERE user_id = ?";
                    var inserts_1 = [id];
                    sql_1 = mysql.format(sql_1, inserts_1);

                    connection.query(sql_1, function(error, results, fields) {
                        if (error) return connection.rollback(function() {
                            return callbackFunction(error);
                        });

                        connection.commit(function(err) {
                            if (err) return connection.rollback(function() {
                                return callbackFunction(err);
                            });

                            return callbackFunction(null);
                        });
                    });
                });
            });
        });
    }

    static getUserIdByUsername(username, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = "SELECT id FROM user WHERE ? LIMIT 1";
            var inserts = {username};
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(error, results, fields) {
                connection.release();

                if (error) return callbackFunction(error);

                if (results.length == 0) return callbackFunction();
                return callbackFunction(null, results);
            });
        });
    }
}

module.exports = { User }