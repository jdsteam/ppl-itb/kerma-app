const mysql = require('mysql');
const moment = require('moment');
const { MysqlManager } =  require('../mysql');

class Photo {
    static getProjectPhotos(pid, phase, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` SELECT photos.id, photos.name, photos.photos
                        FROM photos JOIN phase
                        ON photos.phase_id = phase.id AND photos.project_id = phase.project_id
                        JOIN project
                        ON project.id = phase.project_id
                        WHERE project.category = ? AND phase.stage_num = ? AND photos.project_id = ?
                        ORDER BY photos.id ASC
            `;
                        
            connection.query(sql, [category, phase, pid], function(err, results, fields) {
                connection.release();

                if (err) return callbackFunction(err);

                if (results.length == 0) return callbackFunction(null, []);
                return callbackFunction(null, results);
            })
        });
    }

    static addNewPhoto(pid, phase, name, photo, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` SELECT id FROM phase
                        WHERE phase.project_id = ? AND stage_num = ?
            `;

            connection.query(sql, [pid, phase], function(err, results, fields) {
                if (err) return callbackFunction(err);

                if(results.length == 0) return callbackFunction("phase not found");
                var phase_id = results[0].id;

                sql = ` INSERT INTO photos (phase_id, project_id, name, photos)
                        VALUES (?, ?, ?, ?)
                `;

                connection.query(sql, [phase_id, pid, name, photo], function(err, results, fields) {
                    if (err) return callbackFunction(err);

                    var sql = ` UPDATE project
                            SET updated_at = NOW()
                            WHERE id = ?
                    `;

                    connection.query(sql, [pid], function(err, results, fields) {
                        connection.release();

                        if (err) return callbackFunction(err);
                    });

                    return callbackFunction(null, results);
                });
            });
        });
    }

    static deleteProjectPhoto(pid, photo_id, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` DELETE photos
                        FROM photos JOIN project
                        ON photos.project_id = project.id
                        WHERE project.id = ? AND photos.id = ? AND project.category = ?
            `;

            connection.query(sql, [pid, photo_id, category], function(err, results, fields) {
                if (err) return callbackFunction(err);

                var sql = ` UPDATE project
                            SET updated_at = NOW()
                            WHERE id = ?
                `;

                connection.query(sql, [pid], function(err, results, fields) {
                    connection.release();

                    if (err) return callbackFunction(err);
                });

                return callbackFunction(null, results);
            })
        });
    }

    static updateProjectPhoto(pid, photo_id, name, photo, category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` UPDATE photos JOIN project ON photos.project_id = project.id
                        SET photos.name = ?, photos.photos = ?
                        WHERE photos.id = ? AND project.category = ? AND project.id = ?
            `;

            connection.query(sql, [name, photo, photo_id, category, pid], function(err, results, fields) {
                if (err) return callbackFunction(err);

                var sql = ` UPDATE project
                            SET updated_at = NOW()
                            WHERE id = ?
                `;

                connection.query(sql, [pid], function(err, results, fields) {
                    connection.release();

                    if (err) return callbackFunction(err);
                });

                return callbackFunction(null, results);
            })

        });
    }
}

module.exports = { Photo }