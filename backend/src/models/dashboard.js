const mysql = require('mysql');
const moment = require('moment');
const { MysqlManager } =  require('../mysql');

class Dashboard {
    static getOngoingProjectPerPhase(category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = ` SELECT phase_identifier.stage_num, COUNT(category_project.id) AS count_project
                        FROM phase_identifier LEFT JOIN 
                            (SELECT id, curr_phase_stage FROM project WHERE ? AND status = 1 ) AS category_project 
                        ON phase_identifier.stage_num = category_project.curr_phase_stage 
                        GROUP BY phase_identifier.stage_num`;
            var inserts = {category};
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(error, results, fields) {
                connection.release();

                if (error) return callbackFunction(error);

                if (results.length == 0) return callbackFunction();

                let resultArr = [];
                for (let result of results) {
                    resultArr.push(result['count_project']);
                }
                return callbackFunction(null, {project_count: resultArr});
            });
        });
    }

    static getCurrentMonthProjectPerStatus(category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            let resultArr = [];

            var sql = ` SELECT status, COUNT(project.id) AS count_project
                        FROM project
                        WHERE category = ? AND project.status = 2 AND MONTH(project.updated_at) = MONTH(NOW()) AND YEAR(project.updated_at) = YEAR(NOW())
                        GROUP BY project.status`;
            
            connection.query(sql, [category], function(error, results, fields) {
                if (error) callbackFunction(error);
                
                if (results.length == 0) resultArr.push(0)
                else resultArr.push(results[0]['count_project']);

                sql = ` SELECT COUNT(project.id) AS count_project
                        FROM project
                        WHERE category = ? AND project.status = 1 AND MONTH(project.created_at) = MONTH(NOW()) AND YEAR(project.created_at) = YEAR(NOW())
                        GROUP BY project.status`;

                connection.query(sql, [category], function(error, results, fields) {
                    if (error) callbackFunction(error);

                    if (results.length == 0) resultArr.push(0)
                    else resultArr.push(results[0]['count_project']);

                    sql = ` SELECT COUNT(project.id) AS count_project
                            FROM project
                            WHERE category = ? AND project.status = 1 AND (MONTH(project.created_at) < MONTH(NOW()) OR YEAR(project.created_at) < YEAR(NOW()))
                            GROUP BY project.status`;
                    
                    connection.query(sql, [category], function(error, results, fields) {
                        if (error) callbackFunction(error);

                        if (results.length == 0) resultArr.push(0)
                        else resultArr.push(results[0]['count_project']);

                        sql = ` SELECT COUNT(project.id) AS count_project
                                FROM project
                                WHERE category = ? AND project.status = 3 AND MONTH(project.created_at) = MONTH(NOW()) AND YEAR(project.created_at) = YEAR(NOW())
                                GROUP BY project.status`;

                        connection.query(sql, [category], function(error, results, fields) {
                            connection.release();
                            if (error) callbackFunction(error);

                            if (results.length == 0) resultArr.push(0)
                            else resultArr.push(results[0]['count_project']);

                            return callbackFunction(null, {project_count: resultArr});
                        })
                    })
                })
            })
        })
    }

    static getCompletedProjectAnnually(category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            let resultArr = Array(12);
            let promises = [];

            for (let i = 0; i < 12; i++) {
                promises.push(new Promise((resolve, reject) => {
                    let sql = ` SELECT COUNT(project.id) AS count_project
                                FROM project
                                WHERE category = ? AND status = 2 AND MONTH(updated_at) = ? AND YEAR(updated_at) = YEAR(NOW())
                                GROUP BY status`;
                    connection.query(sql, [category, (i+1)], function(error, results, fields) {
                        if (error) return reject(error);
                        else {
                            resultArr[i] = (results.length == 0) ? 0 : results[0]['count_project'];
                            resolve();
                        }
                    })
                }));
            }

            Promise.all(promises).then(results => {
                connection.release();
                return callbackFunction(null, {project_count: resultArr});
            })
            .catch(e => {
                return callbackFunction(e);
            });
        });
    }

    static getNewProjectAnnually(category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            let resultArr = Array(12);
            let promises = [];

            for (let i = 0; i < 12; i++) {
                promises.push(new Promise((resolve, reject) => {
                    let sql = ` SELECT COUNT(project.id) AS count_project
                                FROM project
                                WHERE category = ? AND status = 1 AND MONTH(created_at) = ? AND YEAR(created_at) = YEAR(NOW())
                                GROUP BY status`;
                    connection.query(sql, [category, (i+1)], function(error, results, fields) {
                        if (error) return reject(error);
                        else {
                            resultArr[i] = (results.length == 0) ? 0 : results[0]['count_project'];
                            resolve();
                        }
                    })
                }));
            }

            Promise.all(promises).then(results => {
                connection.release();
                return callbackFunction(null, {project_count: resultArr});
            })
            .catch(e => {
                return callbackFunction(e);
            });
        });
    }

    static getOngoingProjectAnnually(category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            let resultArr = Array(12);
            let promises = [];

            for (let i = 0; i < 12; i++) {
                promises.push(new Promise((resolve, reject) => {
                    let sql = ` SELECT COUNT(project.id) AS count_project
                                FROM project
                                WHERE category = ? AND status = 1 AND ((? <= MONTH(NOW()) AND MONTH(created_at) < ?) OR YEAR(created_at) < YEAR(NOW()))
                                GROUP BY status`;
                    connection.query(sql, [category, (i+1), (i+1)], function(error, results, fields) {
                        if (error) return reject(error);
                        else {
                            resultArr[i] = (results.length == 0) ? 0 : results[0]['count_project'];
                            resolve();
                        }
                    })
                }));
            }

            Promise.all(promises).then(results => {
                connection.release();
                return callbackFunction(null, {project_count: resultArr});
            })
            .catch(e => {
                return callbackFunction(e);
            });
        });
    }

    static getCancelledProjectAnnually(category, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            let resultArr = Array(12);
            let promises = [];

            for (let i = 0; i < 12; i++) {
                promises.push(new Promise((resolve, reject) => {
                    let sql = ` SELECT COUNT(project.id) AS count_project
                                FROM project
                                WHERE category = ? AND status = 3 AND MONTH(updated_at) = ? AND YEAR(updated_at) = YEAR(NOW())
                                GROUP BY status`;
                    connection.query(sql, [category, (i+1)], function(error, results, fields) {
                        if (error) return reject(error);
                        else {
                            resultArr[i] = (results.length == 0) ? 0 : results[0]['count_project'];
                            resolve();
                        }
                    })
                }));
            }

            Promise.all(promises).then(results => {
                connection.release();
                return callbackFunction(null, {project_count: resultArr});
            })
            .catch(e => {
                return callbackFunction(e);
            });
        });
    }

    static getObserverStatistics(callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            let statistics = new Object();

            let promise = new Promise((resolve, reject) => {
                var sql = ` SELECT COUNT(project.id) as 'ongoing'
                            FROM project
                            WHERE project.status = 1 AND project.created_at < LAST_DAY(NOW()) - INTERVAL 30 DAY;
                `;

                connection.query(sql, function(err, results, fields) {
                    if (err) return reject(err);

                    resolve(results[0].ongoing);
                });
            })

            promise = promise.then(ongoing_count => {
                statistics["ongoing"] = ongoing_count;
                return new Promise((resolve, reject) => {
                    var sql = ` SELECT COUNT(project.id) as 'new'
                            FROM project
                            WHERE project.status = 1 AND project.created_at >= LAST_DAY(NOW()) - INTERVAL 30 DAY;
                    `;

                    connection.query(sql, function(err, results, fields) {
                        if (err) return reject(err);

                        resolve(results[0].new);
                    });
                })
            })

            promise = promise.then(new_count => {
                statistics["new"] = new_count;
                connection.release();
                return callbackFunction(null, statistics);
            })
        });
    }
}

module.exports = { Dashboard }