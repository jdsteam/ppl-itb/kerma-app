const { MysqlManager } =  require('../mysql');
const config = require('../../config');
const fs = require('fs');

class Data {
    static generateCountryCSV(callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

                var sql = "SELECT country, COUNT(id) as 'count' FROM project WHERE status = 1 GROUP BY country";

            connection.query(sql, function(error, results, fields) {
                connection.release();

                if (error) return callbackFunction(error);

                if (results.length == 0) return callbackFunction();

                var header = fields.map(value => {
                    return value.name;
                });

                results = results.map(value => {
                    return [value.country, value.count];
                })

                var create_csv = function(header, rows) {
                    var csv = header[0];
                    for (var i = 1; i < header.length; i++) {
                        csv += ',' + header[i];
                    }

                    csv += '\n';

                    for (var i = 0; i < rows.length; i++) {
                        csv += rows[i].join(',');
                        csv += '\n';
                    }

                    return csv;
                };

                const csv_path = './data/country.csv'

                fs.writeFileSync(csv_path, create_csv(header, results), (err) => {
                    if (err) {
                        return callbackFunction(err);
                    }
                })

                return callbackFunction(null, csv_path);
            });
        });
    }
}

module.exports = { Data }