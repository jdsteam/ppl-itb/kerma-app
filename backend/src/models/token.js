const mysql = require('mysql');
const { MysqlManager } =  require('../mysql');
const randomize = require('randomatic');

class Token {
    static getToken(uid, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = "SELECT token FROM token WHERE ? LIMIT 1";
            var inserts = {user_id: uid};
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(error, results, fields) {
                connection.release();

                if (error) return callbackFunction(error);

                if (results.length == 0) return callbackFunction();
                return callbackFunction(null, results[0].token);
            });
        });
    }
    
    static addToken(uid, callbackFunction) {
        Token.getToken(uid, function(err, result) {
            if (err) return callbackFunction(err)
            else if (result) callbackFunction(null, result)
            else {
                MysqlManager.getConnection(function(err, connection) {
                    if (err) return callbackFunction(err);
                    
                    var token = randomize('Aa0', 32);
                    var sql = "INSERT INTO token SET ?";
                    var inserts = {user_id: uid, token: token};
                    sql = mysql.format(sql, inserts);
        
                    connection.query(sql, function(error, results, fields) {
                        connection.release();
        
                        if (error) return callbackFunction(error);
        
                        return callbackFunction(null, token);
                    });
                });
            }
        });
    }

    static deleteToken(uid, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = "DELETE FROM token WHERE ?";
            var inserts = {user_id: uid};
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(error, results, fields) {
                connection.release();

                if (error) return callbackFunction(error);

                return callbackFunction(null);
            });
        });
    }
}

module.exports = { Token }