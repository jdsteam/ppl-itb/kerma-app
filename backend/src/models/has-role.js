const mysql = require('mysql');
const { MysqlManager } =  require('../mysql');

class Role {
    static getRole(user_id, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            var sql = "SELECT role FROM has_role WHERE ?";
            var inserts = {user_id};
            sql = mysql.format(sql, inserts);

            connection.query(sql, function(error, results, fields) {
                connection.release();

                if (error) return callbackFunction(error);

                if (results.length == 0) return callbackFunction();
                
                let resultArr = []
                for (let result of results) {resultArr.push(result.role)};

                return callbackFunction(null, resultArr);
            });
        });
    }

    static addRole(user_id, roleArr, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            if (!roleArr) roleArr = [];
            let inserts = [];
            for (let role of roleArr) {
                inserts.push([user_id, role]);
            }

            var sql = "INSERT INTO has_role (user_id, role) VALUES ?";

            if (inserts.length != 0) {
                connection.query(sql, [inserts], function(error, results, fields) {
                    connection.release();

                    if (error) return callbackFunction(error);

                    return callbackFunction(null, results.insertId);
                });
            } else {
                return callbackFunction(null);
            }
        });
    }

    static deleteRole(user_id, roleArr, callbackFunction) {
        MysqlManager.getConnection(function(err, connection) {
            if (err) return callbackFunction(err);

            if (!roleArr) roleArr = [];
            let inserts = [];
            for (let role of roleArr) {
                inserts.push([user_id, role]);
            }

            var sql = "DELETE FROM has_role WHERE (user_id, role) IN (?)";

            connection.query(sql, [inserts], function(error, results, fields) {
                connection.release();

                if (error) return callbackFunction(error);

                return callbackFunction(null);
            });
        });
    }
}

module.exports = { Role }