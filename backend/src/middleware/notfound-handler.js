const notFoundHandler = function(req, res, next) {
    res.status(404).json({
        code: 404,
        message: "NOT FOUND"
    }).end();
}

module.exports = { notFoundHandler };