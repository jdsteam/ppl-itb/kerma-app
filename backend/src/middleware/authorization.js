const jwt = require('jsonwebtoken');
const { Role, Project } = require('../models');

const isAuthorized = function(arrRole, accessCurrentUser = false) {
    return function(req, res, next) {
        let userId = res.locals.auth.userId;
        if (!userId) {
            return res.status(401).json({
                status: 401,
                message: 'UNAUTHORIZED'
            });
        }
        
        if (accessCurrentUser && req.params.uid && (req.params.uid == userId)) {
            next();
        }
        else {
            Role.getRole(userId, function(err, results) {
                if (err) {
                    return res.status(500).json({
                        status: 500,
                        message: 'INTERNAL ERROR'
                    }); 
                }
                else if (!results) {
                    return res.status(401).json({
                        status: 401,
                        message: 'UNAUTHORIZED'
                    }); 
                }
                else if ((arrRole) && Array.isArray(arrRole)) {
                    for (let role of arrRole) {
                        if (!results.includes(role)) {
                            return res.status(401).json({
                                status: 401,
                                message: 'UNAUTHORIZED'
                            });
                        }
                    }
                    next();
                }
                else {
                    return res.status(500).json({
                        status: 500,
                        message: 'INTERNAL ERROR'
                    }); 
                }
            })
        }
    }
}

const isAuthorizedToEditProject = function(req, res, next) {
    if (!req.body.pid) {
        return res.status(401).json({
            status: 401,
            message: 'UNAUTHORIZED'
        });
    }
    else {
        let projectId = req.body.pid;
        let username = res.locals.auth.username;

        Project.getProjectPic(projectId, function(err, result) {
            if (err) {
                return res.status(500).json({
                    status: 500,
                    message: 'INTERNAL ERROR'
                }); 
            }
            else if (!result || result != username) {
                return res.status(401).json({
                    status: 401,
                    message: 'UNAUTHORIZED'
                }); 
            }
            else {
                next();
            }
        })
    }
}

const isProjectEditable = function(req, res, next) {
    if (req.body.pid) {
        let projectId = req.body.pid;
        Project.getProjectStatus(projectId, function(err, result) {
            if (err) {
                return res.status(500).json({
                    status: 500,
                    message: 'INTERNAL ERROR'
                }); 
            }
            else if (result && result != 1) {
                return res.status(401).json({
                    status: 401,
                    message: 'UNAUTHORIZED'
                });
            }
            else {
                next();
            }
        })
    }
    else next();
}

module.exports = { isAuthorized, isAuthorizedToEditProject, isProjectEditable };