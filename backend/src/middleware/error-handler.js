const { STATUS_CODES } = require('http');

const errorHandler = (err, req, res, next) => {
  //dev mode error
  if (process.env.NODE_ENV !== 'production') { 
    // do something here
  } 

  //401: Unauthorized
  if (err.status == 401) {
    return res
      .status(401)
      .json({
        code: 401,
        message: "UNAUTHORIZED"
      }).end();
  }

  //403: Forbidden
  if (err.status == 403) {
    return res
      .status(403)
      .json({
        code: 403,
        message: "FORBIDDEN"
      }).end();
  }

  //404: Not found
  if (err.status == 404) {
    return res
      .status(404)
      .json({
        code: 404,
        message: "NOT FOUND"
      }).end();
  }
  
  if (['ValidationError', 'UserExistsError'].includes(err.name)) {
    return res.status(405).json(err);
  }

  //duplicate error
  if (("errno" in err) && (err.errno == 1062)) {
    return res.status(403).json({
      code: 403,
      message: "Forbidden"
    })
  }

  //others (500: Internal error)
  return res // return 500 for user
    .status(err.status || 500)
    .json({
      code: 500,
      message: err.message
    }).end();
};

module.exports = { errorHandler };