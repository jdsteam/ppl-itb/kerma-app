const jwt = require('jsonwebtoken');
const { Token } = require('../models');

const isAuthenticated = function(req, res, next) {
    let token = req.headers['x-access-token'];

    if (!token) {
        return res.status(403).json({
            status: 403,
            message: 'FORBIDDEN'
        });
    }
    else {
        let userId = jwt.decode(token).id;
        if (userId) {
            Token.getToken(userId, function(err, results) {
                if (err) {
                    return res.status(500).json({
                        status: 500,
                        message: 'INTERNAL ERROR'
                    }); 
                }
                else if (!results) {
                    return res.status(403).json({
                        status: 403,
                        message: 'FORBIDDEN'
                    });
                }
                else {
                    jwt.verify(token, results, function(err, decoded) {
                        if (err) {
                            return res.status(401).json({
                                status: 401,
                                message: 'UNAUTHORIZED'
                            });
                        }
                        else {
                            res.locals.auth = {
                                userId : decoded.id,
                                username : decoded.username
                            }
                            next();
                        }
                    })
                }
            });
        }
        else {
            return res.status(403).json({
                status: 403,
                message: 'FORBIDDEN'
            });
        }
    }
}

module.exports = { isAuthenticated };