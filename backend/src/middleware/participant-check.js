const jwt = require('jsonwebtoken');
const { Participate } = require('../models');

const isParticipant = function(req, res, next) {
    let userId = res.locals.auth.userId;
    let projectId = req.body.pid;

    if (!userId || !projectId) {
        return res.status(401).json({
            status: 401,
            message: 'UNAUTHORIZED'
        });
    }

    Participate.getParticipate(userId, projectId, function(err, results) {
        if (err) return res.status(500).json({
            status: 500,
            message: 'INTERNAL ERROR'
        }); 
        else {
            if (results) {
                next();
            }
            else {
                return res.status(401).json({
                    status: 401,
                    message: 'UNAUTHORIZED'
                });
            }
        }
    });

    
    
}

module.exports = { isParticipant };