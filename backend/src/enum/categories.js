const CATEGORIES = Object.freeze({
    INVESTASI: 1,
    SOSIAL: 2,
    DAGANG: 3,
    WISATA: 4,
    ADMIN: 5,
    MANAGER: 6
});

module.exports = CATEGORIES;