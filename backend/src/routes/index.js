var express = require('express');

const { errorHandler } = require('../middleware');

//list of controllers
const { loginController, logoutController, projectController, registerController, roleController, userController, dashboardController, notificationController, dataController } = require('../controllers');

//router config
const router = config => {
  const router = express();

  //register api points
  router.use('/login', loginController);
  router.use('/logout', logoutController);
  router.use('/project', projectController);
  router.use('/register', registerController);
  router.use('/role', roleController);
  router.use('/user', userController);
  router.use('/dashboard', dashboardController);
  router.use('/notification', notificationController);
  router.use('/data', dataController);

  //catch error
  router.use(errorHandler);
  return router;
};

module.exports = router;
