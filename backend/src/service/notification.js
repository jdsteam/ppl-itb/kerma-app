const scheduler = require('node-schedule');
const { Notification} = require('../models');

var daily_notification = scheduler.scheduleJob({hour: 22, minute: 00, second:50, dayOfWeek: [new scheduler.Range(1, 6)]}, function(fireDate) {
    Notification.sendDailyNotification(function(err) {
        if (err) console.log(err.message);
        else console.log("COMMENCING DAILY MESSAGE BROADCAST");
    })
});

var monthly_scheduler = scheduler.scheduleJob({date: 30, hour: 20, minute: 0, second: 0}, function() {
    Notification.sendMonthlyNotification(function(err) {
        if(err) console.log(err.message);
        else console.log("COMMENCING MONTHLY MESSAGE BROADCAST");
    })
});

module.exports = { daily_notification, monthly_scheduler };
