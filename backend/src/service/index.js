const { DailyNotification, MonthlyNotification } = require('./notification');

module.exports = { DailyNotification, MonthlyNotification };