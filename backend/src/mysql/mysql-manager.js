const mysql = require('mysql');

class MysqlManager {
    constructor (config) {
        MysqlManager._config = config;
    }

    static connect() {
        MysqlManager._pool = mysql.createPool(MysqlManager._config);
    }

    static getConnection(calllbackFunction) {
        MysqlManager._pool.getConnection(calllbackFunction);
    }

}

module.exports = { MysqlManager }