-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: projectcontrol
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `phase_id` int(6) unsigned NOT NULL,
  `project_id` int(6) unsigned NOT NULL,
  `comment` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `phase_id` (`phase_id`),
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (3,5,18,'Random comment','2019-05-09 14:40:05','2019-05-09 14:40:05'),(4,5,18,'Random comment 2','2019-05-09 14:40:09','2019-05-09 14:40:09');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `phase_id` int(6) unsigned NOT NULL,
  `project_id` int(6) unsigned NOT NULL,
  `document` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `phase_id` (`phase_id`),
  CONSTRAINT `documents_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `documents_ibfk_2` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
INSERT INTO `documents` VALUES (1,5,18,'google docs','2019-05-09 14:30:26','2019-05-09 14:30:26','Document');
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `has_role`
--

DROP TABLE IF EXISTS `has_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `has_role` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(6) unsigned NOT NULL,
  `role` int(6) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `has_role`
--

LOCK TABLES `has_role` WRITE;
/*!40000 ALTER TABLE `has_role` DISABLE KEYS */;
INSERT INTO `has_role` VALUES (1,1,1,'2019-05-09 09:23:57','2019-05-09 09:23:57'),(2,1,2,'2019-05-09 09:24:00','2019-05-09 09:24:00'),(3,1,3,'2019-05-09 09:24:03','2019-05-09 09:24:03'),(4,1,4,'2019-05-09 09:24:06','2019-05-09 09:24:06'),(5,1,6,'2019-05-09 10:21:56','2019-05-09 10:21:56');
/*!40000 ALTER TABLE `has_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participate`
--

DROP TABLE IF EXISTS `participate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participate` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(6) unsigned NOT NULL,
  `project_id` int(6) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `participate_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `participate_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participate`
--

LOCK TABLES `participate` WRITE;
/*!40000 ALTER TABLE `participate` DISABLE KEYS */;
INSERT INTO `participate` VALUES (1,1,18,'2019-05-13 06:08:26','2019-05-13 06:08:26'),(2,2,18,'2019-05-14 04:41:47','2019-05-14 04:41:47'),(3,1,18,'2019-05-14 04:41:47','2019-05-14 04:41:47'),(4,2,18,'2019-05-14 04:55:45','2019-05-14 04:55:45'),(5,1,18,'2019-05-14 04:55:45','2019-05-14 04:55:45'),(6,2,18,'2019-05-14 05:13:20','2019-05-14 05:13:20'),(7,1,18,'2019-05-14 05:13:20','2019-05-14 05:13:20'),(8,1,19,'2019-05-14 14:23:18','2019-05-14 14:23:18'),(9,1,19,'2019-05-14 14:24:50','2019-05-14 14:24:50'),(10,1,19,'2019-05-14 14:25:00','2019-05-14 14:25:00'),(11,1,19,'2019-05-14 14:41:12','2019-05-14 14:41:12'),(12,1,23,'2019-05-14 14:42:18','2019-05-14 14:42:18');
/*!40000 ALTER TABLE `participate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perangkat`
--

DROP TABLE IF EXISTS `perangkat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perangkat` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perangkat`
--

LOCK TABLES `perangkat` WRITE;
/*!40000 ALTER TABLE `perangkat` DISABLE KEYS */;
INSERT INTO `perangkat` VALUES (36,'Badan Kepegawaian Daerah'),(41,'Badan Penanggulangan Bencana Daerah'),(40,'Badan Pendapatan Daerah'),(38,'Badan Penelitian dan Pengembangan Daerah'),(39,'Badan Pengelolaan Keuangan dan Aset Daerah'),(37,'Badan Pengembangan Sumber Daya Manusia'),(35,'Badan Perencanaan Pembangunan Daerah'),(4,'Biro Badan Usaha Milik Daerah dan Investasi'),(8,'Biro Hubungan Masyarakat dan Protokol'),(2,'Biro Hukum dan Hak Asasi Manusia'),(7,'Biro Organisasi'),(3,'Biro Pelayanan dan Pengembangan Sosial'),(1,'Biro Pemerintahan dan Kerjasama'),(6,'Biro Pengadaan Barang/Jasa'),(5,'Biro Perekonomian'),(9,'Biro Umum'),(12,'Dinas Bina Marga dan Penataan Ruang'),(32,'Dinas Energi dan Sumber Daya Mineral'),(31,'Dinas Kehutanan'),(28,'Dinas Kelautan dan Perikanan'),(34,'Dinas Kependudukan dan Pencatatan Sipil'),(11,'Dinas Kesehatan'),(26,'Dinas Ketahanan Pangan dan Peternakan'),(20,'Dinas Komunikasi dan Informatika'),(21,'Dinas Koperasi dan Usaha Kecil'),(17,'Dinas Lingkungan Hidup'),(27,'Dinas Pariwisata dan Kebudayaan'),(18,'Dinas Pemberdayaan Masyarakat dan Desa'),(16,'Dinas Pemberdayaan Perempuan, Perlindungan Anak, dan Keluarga Berencana'),(23,'Dinas Pemuda dan Olahraga'),(22,'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu'),(10,'Dinas Pendidikan'),(19,'Dinas Perhubungan'),(33,'Dinas Perindustrian dan Perdagangan'),(30,'Dinas Perkebunan'),(24,'Dinas Perpustakaan dan Kearsipan Daerah'),(14,'Dinas Perumahan dan Pemukiman'),(15,'Dinas Sosial'),(13,'Dinas Sumber Daya Air'),(29,'Dinas Tanaman Pangan dan Holtikultura'),(25,'Dinas Tenaga Kerja dan Transmigrasi');
/*!40000 ALTER TABLE `perangkat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perangkat_involved`
--

DROP TABLE IF EXISTS `perangkat_involved`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perangkat_involved` (
  `project_id` int(6) unsigned NOT NULL,
  `perangkat_id` int(6) NOT NULL,
  PRIMARY KEY (`project_id`,`perangkat_id`),
  KEY `perangkat_id` (`perangkat_id`),
  CONSTRAINT `perangkat_involved_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `perangkat_involved_ibfk_2` FOREIGN KEY (`perangkat_id`) REFERENCES `perangkat` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perangkat_involved`
--

LOCK TABLES `perangkat_involved` WRITE;
/*!40000 ALTER TABLE `perangkat_involved` DISABLE KEYS */;
/*!40000 ALTER TABLE `perangkat_involved` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phase`
--

DROP TABLE IF EXISTS `phase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phase` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(6) unsigned NOT NULL,
  `stage_num` tinyint(4) NOT NULL,
  `due_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `phase_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phase`
--

LOCK TABLES `phase` WRITE;
/*!40000 ALTER TABLE `phase` DISABLE KEYS */;
INSERT INTO `phase` VALUES (5,18,1,'1998-03-20 17:00:00','2019-05-09 11:33:43','2019-05-14 05:25:08'),(6,18,2,'1998-03-21 17:00:00','2019-05-14 05:21:19','2019-05-14 05:25:08'),(7,18,3,'1998-03-22 17:00:00','2019-05-14 05:21:29','2019-05-14 05:25:08'),(8,18,4,'1998-03-23 17:00:00','2019-05-14 05:21:34','2019-05-14 05:25:08'),(9,18,5,'1998-03-24 17:00:00','2019-05-14 05:21:39','2019-05-14 05:25:08'),(10,18,6,'1998-03-25 17:00:00','2019-05-14 05:21:45','2019-05-14 05:25:08'),(11,18,7,'1998-03-26 17:00:00','2019-05-14 05:21:54','2019-05-14 05:25:08'),(12,19,1,'2020-02-29 17:00:00','2019-05-14 14:23:18','2019-05-14 14:23:18'),(13,19,2,'2020-02-29 17:00:00','2019-05-14 14:23:18','2019-05-14 14:23:18'),(14,19,3,'2020-02-29 17:00:00','2019-05-14 14:23:18','2019-05-14 14:23:18'),(15,19,4,'2020-02-29 17:00:00','2019-05-14 14:23:18','2019-05-14 14:23:18'),(16,19,5,'2020-02-29 17:00:00','2019-05-14 14:23:18','2019-05-14 14:23:18'),(17,19,6,'2020-02-29 17:00:00','2019-05-14 14:23:18','2019-05-14 14:23:18'),(18,19,7,'2020-02-29 17:00:00','2019-05-14 14:23:18','2019-05-14 14:23:18'),(40,23,1,'1998-03-20 17:00:00','2019-05-14 14:42:18','2019-05-14 14:43:49'),(41,23,2,'1998-03-21 17:00:00','2019-05-14 14:42:18','2019-05-14 14:43:49'),(42,23,3,'1998-03-22 17:00:00','2019-05-14 14:42:18','2019-05-14 14:43:49'),(43,23,4,'1998-03-23 17:00:00','2019-05-14 14:42:18','2019-05-14 14:43:49'),(44,23,5,'1998-03-24 17:00:00','2019-05-14 14:42:18','2019-05-14 14:43:49'),(45,23,6,'1998-03-25 17:00:00','2019-05-14 14:42:18','2019-05-14 14:43:49'),(46,23,7,'1998-03-26 17:00:00','2019-05-14 14:42:18','2019-05-14 14:43:49');
/*!40000 ALTER TABLE `phase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phase_identifier`
--

DROP TABLE IF EXISTS `phase_identifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phase_identifier` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `stage_num` int(6) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stage_num_UNIQUE` (`stage_num`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phase_identifier`
--

LOCK TABLES `phase_identifier` WRITE;
/*!40000 ALTER TABLE `phase_identifier` DISABLE KEYS */;
INSERT INTO `phase_identifier` VALUES (1,1,'Paparan','2019-05-09 08:43:30','2019-05-09 08:43:30'),(2,2,'Identifikasi Data/Potensi','2019-05-09 08:43:30','2019-05-09 08:43:30'),(3,3,'Penentuan Jenis/Model Kerjasama','2019-05-09 08:43:31','2019-05-09 08:43:31'),(4,4,'Penandatanganan MOU/PKS','2019-05-09 08:43:31','2019-05-09 08:43:31'),(5,5,'Implementasi Kegiatan','2019-05-09 08:43:31','2019-05-09 08:43:31'),(6,6,'Monitoring dan Laporan','2019-05-09 08:43:31','2019-05-09 08:43:31'),(7,7,'Ekspansi/Peningkatan','2019-05-09 08:43:31','2019-05-09 08:43:31');
/*!40000 ALTER TABLE `phase_identifier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photos` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `phase_id` int(6) unsigned NOT NULL,
  `project_id` int(6) unsigned NOT NULL,
  `photos` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `phase_id` (`phase_id`),
  CONSTRAINT `photos_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `photos_ibfk_2` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category` int(6) unsigned NOT NULL,
  `pic` varchar(255) NOT NULL,
  `curr_phase_stage` tinyint(4) NOT NULL DEFAULT '1',
  `company_name` varchar(255) DEFAULT NULL,
  `company_address` varchar(255) DEFAULT NULL,
  `cp_name` varchar(255) NOT NULL,
  `cp_phone` varchar(16) DEFAULT NULL,
  `cp_email` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `country` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name` (`name`),
  KEY `status` (`status`),
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`status`) REFERENCES `status_identifier` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (18,'Rehab IF',1,'1',11,'bla','bla','bla','bla','bla','bla','2019-05-09 11:33:43','2020-12-09 17:00:00',1,''),(19,'Test Project',1,'1',1,'Tirta','Bandung','Jon','0812','a@gmail.com','Testing Project','2019-05-14 14:23:18','2019-05-14 14:23:18',1,'Indonesia'),(23,'Editedproject',1,'1',1,'bla','bla','bla','bla','bla','bla','2019-05-14 14:42:18','2019-05-14 14:45:52',1,'Zimbabwe');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_identifier`
--

DROP TABLE IF EXISTS `role_identifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_identifier` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `role` int(6) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_UNIQUE` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_identifier`
--

LOCK TABLES `role_identifier` WRITE;
/*!40000 ALTER TABLE `role_identifier` DISABLE KEYS */;
INSERT INTO `role_identifier` VALUES (1,1,'Investasi','2019-05-09 08:43:30','2019-05-09 08:43:30'),(2,2,'Program Sosial','2019-05-09 08:43:30','2019-05-09 08:43:30'),(3,3,'Komoditas dan Perdagangan','2019-05-09 08:43:30','2019-05-09 08:43:30'),(4,4,'Pengembangan Pariwisata','2019-05-09 08:43:30','2019-05-09 08:43:30'),(5,5,'Admin','2019-05-09 08:43:30','2019-05-09 08:43:30'),(6,6,'Manager','2019-05-09 08:43:30','2019-05-09 08:43:30');
/*!40000 ALTER TABLE `role_identifier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_identifier`
--

DROP TABLE IF EXISTS `status_identifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_identifier` (
  `id` int(6) unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_identifier`
--

LOCK TABLES `status_identifier` WRITE;
/*!40000 ALTER TABLE `status_identifier` DISABLE KEYS */;
INSERT INTO `status_identifier` VALUES (1,1,'ongoing'),(2,2,'finished'),(3,3,'cancelled'),(4,4,'pending');
/*!40000 ALTER TABLE `status_identifier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `todo_list`
--

DROP TABLE IF EXISTS `todo_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `todo_list` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(45) NOT NULL,
  `is_checked` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `phase_id` int(6) unsigned NOT NULL,
  `project_id` int(6) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_phase_id` (`phase_id`),
  KEY `fk_project_id` (`project_id`),
  CONSTRAINT `fk_phase_id` FOREIGN KEY (`phase_id`) REFERENCES `phase` (`id`),
  CONSTRAINT `fk_project_id` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `todo_list`
--

LOCK TABLES `todo_list` WRITE;
/*!40000 ALTER TABLE `todo_list` DISABLE KEYS */;
INSERT INTO `todo_list` VALUES (1,'test',1,'2019-05-13 05:56:40','2019-05-13 06:08:34',5,18);
/*!40000 ALTER TABLE `todo_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(6) unsigned NOT NULL,
  `token` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `token_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
INSERT INTO `token` VALUES (2,1,'roMaZjE4lKyJR44czCMmp5qyiwOQPlwp','2019-05-09 09:17:00','2019-05-09 09:17:00');
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(40) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'test','test','test@gmail.com','test','2019-05-09 08:59:48','2019-05-13 14:36:49'),(2,'nico','nico','nico@nico.com','4444','2019-05-11 06:08:27','2019-05-11 06:08:27');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-14 23:12:03
