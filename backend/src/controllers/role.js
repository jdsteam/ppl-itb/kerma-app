const { Router: router } = require('express');
const { Role } = require('../models');
const { isAuthenticated, isAuthorized } = require('../middleware');
const { CATEGORIES } = require('../enum');

const app = router();

//!!!DEPRECATED!!!

//GET Role
app.get('/:uid', [isAuthenticated, isAuthorized([CATEGORIES.ADMIN])], function(req, res, next) {
    Role.getRole(req.params.uid, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end()
        // else next({status: 404});
    });
});

//POST Role
app.post('/', [isAuthenticated, isAuthorized([CATEGORIES.ADMIN])], function(req, res, next) {
    Role.addRole(req.body.uid, req.body.roles, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Role added'
        }).end();
    });
});

//DELETE Role
app.delete('/', [isAuthenticated, isAuthorized([CATEGORIES.ADMIN])], function(req, res, next) {
    Role.deleteRole(req.body.uid, req.body.roles, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Role deleted'
        }).end();
    });
});

module.exports = app;

