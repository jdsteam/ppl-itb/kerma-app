const { Router: router } = require('express');
const { Photo } = require('../models');
const { isAuthenticated, isAuthorized, isParticipant } = require('../middleware');
const { CATEGORIES } = require('../enum');

const app = router();

//GET Photos
/**
 * @api {get} /project/investasi/:pid/photos/:phase Investasi - Request Project Phase's Photos
 * @apiVersion 0.1.0
 * @apiName GetProjectPhotoInvestasi
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} photos                          List of photos
 * @apiSuccess {Number}     photos.id                        Photo ID
 * @apiSuccess {String}     photos.name                      Photo title name  
 * @apiSuccess {String}     photos.photos                    Photo link   
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 25,
 *              "name": "Onsite_observation.jpg",
 *              "document": "https://drive.google.com/file_path"
 *          }
 *     ]
 */
app.get('/investasi/:pid/photos/:phase', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI])], function(req, res, next) {
    Photo.getProjectPhotos(req.params.pid, req.params.phase, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {post} /project/investasi/photos Investasi - Add Photo to Project
 * @apiVersion 0.1.0
 * @apiName AddProjectPhotoInvestasi
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Current Stage Number
 * @apiParam {String}   name    Photo Name
 * @apiParam {String}   photo   Photo Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Photo added"
 *     }
 */
app.post('/investasi/photos', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isParticipant], function(req, res, next) {
    Photo.addNewPhoto(req.body.pid, req.body.phase, req.body.name, req.body.photo, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Photo added'
        }).end();
    })
});

/**
 * @api {delete} /project/investasi/photos Investasi - Delete Photo Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectPhotoInvestasi
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   photo_id  Photo ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Photo Deleted"
 *     }
 */
app.delete('/investasi/photos', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isParticipant], function(req, res, next) {
    Photo.deleteProjectPhoto(req.body.pid, req.body.photo_id, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next(err);
        else res.status(200).json({
            status: 200,
            message: 'Photo deleted'
        }).end();
    })
});

/**
 * @api {patch} /project/investasi/photos Investasi - Update Project Photo
 * @apiVersion 0.1.0
 * @apiName UpdateProjectPhotoInvestasi
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   photo_id   Photo ID
 * @apiParam {String}   name    New Photo Name
 * @apiParam {String}   photo   New Photo Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Photo updated"
 *     }
 */
app.patch('/investasi/photos', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isParticipant], function(req, res, next) {
    Photo.updateProjectPhoto(req.body.pid, req.body.photo_id, req.body.name, req.body.photo, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Photo updated'
        }).end();
    })
});

/**
 * @api {get} /project/sosial/:pid/photos/:phase Sosial - Request Project Phase's Photos
 * @apiVersion 0.1.0
 * @apiName GetProjectPhotoSosial
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} photos                          List of photos
 * @apiSuccess {Number}     photos.id                        Photo ID
 * @apiSuccess {String}     photos.name                      Photo title name  
 * @apiSuccess {String}     photos.photos                    Photo link   
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 25,
 *              "name": "Onsite_observation.jpg",
 *              "document": "https://drive.google.com/file_path"
 *          }
 *     ]
 */
app.get('/sosial/:pid/photos/:phase', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL])], function(req, res, next) {
    Photo.getProjectPhotos(req.params.pid, req.params.phase, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {post} /project/sosial/photos Investasi - Add Photo to Project
 * @apiVersion 0.1.0
 * @apiName AddProjectPhotoSosial
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Current Stage Number
 * @apiParam {String}   name    Photo Name
 * @apiParam {String}   photo   Photo Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Photo added"
 *     }
 */
app.post('/sosial/photos', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isParticipant], function(req, res, next) {
    Photo.addNewPhoto(req.body.pid, req.body.phase, req.body.name, req.body.photo, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Photo added'
        }).end();
    })
});

/**
 * @api {delete} /project/sosial/photos Sosial - Delete Photo Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectPhotoSosial
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   photo_id  Photo ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Photo Deleted"
 *     }
 */
app.delete('/sosial/photos', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isParticipant], function(req, res, next) {
    Photo.deleteProjectPhoto(req.body.pid, req.body.photo_id, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next(err);
        else res.status(200).json({
            status: 200,
            message: 'Photo deleted'
        }).end();
    })
});

/**
 * @api {patch} /project/sosial/photos Sosial - Update Project Photo
 * @apiVersion 0.1.0
 * @apiName UpdateProjectPhotoSosial
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   photo_id   Photo ID
 * @apiParam {String}   name    New Photo Name
 * @apiParam {String}   photo   New Photo Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Photo updated"
 *     }
 */
app.patch('/sosial/photos', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isParticipant], function(req, res, next) {
    Photo.updateProjectPhoto(req.body.pid, req.body.photo_id, req.body.name, req.body.photo, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Photo updated'
        }).end();
    })
});

/**
 * @api {get} /project/dagang/:pid/photos/:phase Dagang - Request Project Phase's Photos
 * @apiVersion 0.1.0
 * @apiName GetProjectPhotoDagang
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} photos                          List of photos
 * @apiSuccess {Number}     photos.id                        Photo ID
 * @apiSuccess {String}     photos.name                      Photo title name  
 * @apiSuccess {String}     photos.photos                    Photo link   
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 25,
 *              "name": "Onsite_observation.jpg",
 *              "document": "https://drive.google.com/file_path"
 *          }
 *     ]
 */
app.get('/dagang/:pid/photos/:phase', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG])], function(req, res, next) {
    Photo.getProjectPhotos(req.params.pid, req.params.phase, CATEGORIES.DAGANG, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {post} /project/dagang/photos Investasi - Add Photo to Project
 * @apiVersion 0.1.0
 * @apiName AddProjectPhotoDagang
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Current Stage Number
 * @apiParam {String}   name    Photo Name
 * @apiParam {String}   photo   Photo Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Photo added"
 *     }
 */
app.post('/dagang/photos', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG]), isParticipant], function(req, res, next) {
    Photo.addNewPhoto(req.body.pid, req.body.phase, req.body.name, req.body.photo, CATEGORIES.DAGANG, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Photo added'
        }).end();
    })
});

/**
 * @api {delete} /project/dagang/photos Dagang - Delete Photo Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectPhotoDagang
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   photo_id  Photo ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Photo Deleted"
 *     }
 */
app.delete('/dagang/photos', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG]), isParticipant], function(req, res, next) {
    Photo.deleteProjectPhoto(req.body.pid, req.body.photo_id, CATEGORIES.DAGANG, function(err, result) {
        if (err) next(err);
        else res.status(200).json({
            status: 200,
            message: 'Photo deleted'
        }).end();
    })
});

/**
 * @api {patch} /project/dagang/photos Dagang - Update Project Photo
 * @apiVersion 0.1.0
 * @apiName UpdateProjectPhotoDagang
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   photo_id   Photo ID
 * @apiParam {String}   name    New Photo Name
 * @apiParam {String}   photo   New Photo Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Photo updated"
 *     }
 */
app.patch('/dagang/photos', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG]), isParticipant], function(req, res, next) {
    Photo.updateProjectPhoto(req.body.pid, req.body.photo_id, req.body.name, req.body.photo, CATEGORIES.DAGANG, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Photo updated'
        }).end();
    })
});

/**
 * @api {get} /project/wisata/:pid/photos/:phase Wisata - Request Project Phase's Photos
 * @apiVersion 0.1.0
 * @apiName GetProjectPhotoWisata
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} photos                          List of photos
 * @apiSuccess {Number}     photos.id                        Photo ID
 * @apiSuccess {String}     photos.name                      Photo title name  
 * @apiSuccess {String}     photos.photos                    Photo link   
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 25,
 *              "name": "Onsite_observation.jpg",
 *              "document": "https://drive.google.com/file_path"
 *          }
 *     ]
 */
app.get('/wisata/:pid/photos/:phase', [isAuthenticated, isAuthorized([CATEGORIES.WISATA])], function(req, res, next) {
    Photo.getProjectPhotos(req.params.pid, req.params.phase, CATEGORIES.WISATA, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {post} /project/wisata/photos Investasi - Add Photo to Project
 * @apiVersion 0.1.0
 * @apiName AddProjectPhotoWisata
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Current Stage Number
 * @apiParam {String}   name    Photo Name
 * @apiParam {String}   photo   Photo Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Photo added"
 *     }
 */
app.post('/wisata/photos', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isParticipant], function(req, res, next) {
    Photo.addNewPhoto(req.body.pid, req.body.phase, req.body.name, req.body.photo, CATEGORIES.WISATA, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Photo added'
        }).end();
    })
});

/**
 * @api {delete} /project/wisata/photos Wisata - Delete Photo Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectPhotoWisata
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   photo_id  Photo ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Photo Deleted"
 *     }
 */
app.delete('/wisata/photos', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isParticipant], function(req, res, next) {
    Photo.deleteProjectPhoto(req.body.pid, req.body.photo_id, CATEGORIES.WISATA, function(err, result) {
        if (err) next(err);
        else res.status(200).json({
            status: 200,
            message: 'Photo deleted'
        }).end();
    })
});

/**
 * @api {patch} /project/wisata/photos Wisata - Update Project Photo
 * @apiVersion 0.1.0
 * @apiName UpdateProjectPhotoWisata
 * @apiGroup Project Phase Photo
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   photo_id   Photo ID
 * @apiParam {String}   name    New Photo Name
 * @apiParam {String}   photo   New Photo Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Photo updated"
 *     }
 */
app.patch('/wisata/photos', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isParticipant], function(req, res, next) {
    Photo.updateProjectPhoto(req.body.pid, req.body.photo_id, req.body.name, req.body.photo, CATEGORIES.WISATA, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Photo updated'
        }).end();
    })
});

module.exports = app;