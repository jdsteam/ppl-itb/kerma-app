const { Router: router } = require('express');
const jwt = require('jsonwebtoken');
const { User, Token } = require('../models');

const app = router();

/**
 * @api {post} /login Login
 * @apiVersion 0.1.0
 * @apiName Login
 * @apiGroup Login
 * 
 * @apiPermission none
 * @apiParam    (BodyParam)    {String}    username    Username
 * @apiParam    (BodyParam)    {String}    password    Password
 * 
 * @apiSuccess  {Number}    uid     User ID
 * @apiSuccess  {String}    token   Generated token for further authorization
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "uid": "5",
 *          "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywidXNlcm5hbWUiOiJyYW1hMSIsImlhdCI6MTU1NjcwNTU3OSwiZXhwIjoxNTU3MzEwMzc5fQ.0YB-1dTQHNDmDWq0P3HqLmRmoVRcI9iq3nkCqbzJi8Q"
 *     }
 * 
 * @apiError 401:Unauthorized False login attempt
 * @apiError (Error5) 500:InternalServerError Error in server part, please redo
 * 
 * @apiErrorExample Error-Response:
 *      HTTP/1.1 401 UNAUTHORIZED
 *      {
 *          code: 401,
 *          message: "UNAUTHORIZED"
 *      }
 */
app.post('/', function(req, res, next) {
    User.getUserLogin(req.body.username, req.body.password, function(err, result) {
        if (err) next(err)
        else if (result) {
            Token.addToken(result.id, function(err, resultToken) {
                if (err) next(err)
                else {
                    let payload = {id: result.id, username: req.body.username};
                    let token = jwt.sign(payload, resultToken, {expiresIn: 60 * 60 * 24 * 7});
                    res.status(200).json({
                        uid: result.id,
                        token: token
                    }).end();
                }
            });
        }
        else next({status: 401});
    });
});

module.exports = app;