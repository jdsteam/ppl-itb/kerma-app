const { isAuthenticated, isAuthorized } = require('../middleware');
const { Router: router } = require('express');
const { Notification } = require('../models');
const nodemailer = require('nodemailer');

const app = router();

app.post('/', [isAuthenticated], function(req, res, next) {
    Notification.sendNotification(req.body.emails, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

module.exports = app;