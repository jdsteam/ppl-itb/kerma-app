const { isAuthenticated} = require('../middleware');
const { Router: router } = require('express');
const { Data } = require('../models');

const app = router();

app.get('/countrycsv', function(req, res, next) {
    Data.generateCountryCSV(function(err, result) {
        if (err) next(err)
        else {
            res.download(result);
        }
    });
});

module.exports = app;