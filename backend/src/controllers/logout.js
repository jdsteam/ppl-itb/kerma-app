const { Router: router } = require('express');
const { Token } = require('../models');
const jwt = require('jsonwebtoken');

const app = router();

/**
 * @api {post} /logout Logout
 * @apiVersion 0.1.0
 * @apiName Logout
 * @apiGroup Logout
 * 
 * @apiPermission none
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Logout successful"
 *     }
 * 
 * @apiError (Error5) 500:InternalServerError Error in server part, please redo
 * 
 * @apiErrorExample Error-Response:
 *      HTTP/1.1 500 INTERNAL SERVER ERROR
 *      {
 *          code: 500,
 *          message: "INTERNAL SERVER ERROR"
 *      }
 */
app.post('/', function(req, res, next) {
    let token = req.headers['x-access-token'];
    if (req.body.id) {
        Token.deleteToken(req.body.id, function(err, resultToken) {
            if (err) next(err)
            else return res.status(200).json({
                status: 200,
                message: "Logout successful"
            });
        });
    }
    else if (token) {
        let userId = jwt.decode(token).id;
        Token.deleteToken(userId, function(err, resultToken) {
            if (err) next(err)
            else return res.status(200).json({
                status: 200,
                message: "Logout successful"
            });
        });
    }
});

module.exports = app;