const { Router: router } = require('express');
const { Todo } = require('../models');
const { isAuthenticated, isAuthorized, isParticipant } = require('../middleware');
const { CATEGORIES } = require('../enum');

const app = router();

//GET Todos
/**
 * @api {get} /project/investasi/:pid/todos/:phase Investasi - Request Project Phase's To-Dos
 * @apiVersion 0.1.0
 * @apiName GetProjectTodoInvestasi
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} todos                          List of todos
 * @apiSuccess {Number}     todos.id                        To-do ID
 * @apiSuccess {String}     todos.description               To-do task description 
 * @apiSuccess {Number}     todos.is_checked                To-do checkmark (1: checked, 0: not checked)   
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 20,
 *              "description": "Meeting # 1 @ Office",
 *              "is_checked": 1
 *          }
 *     ]
 */
app.get('/investasi/:pid/todos/:phase', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI])], function(req, res, next) {
    Todo.getProjectTodos(req.params.pid, req.params.phase, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {post} /project/investasi/todos Investasi - Add Todo to Project
 * @apiVersion 0.1.0
 * @apiName AddProjectTodoInvestasi
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Current Stage Number
 * @apiParam {String}   todo    Todo text
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo added"
 *     }
 */
app.post('/investasi/todos', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isParticipant], function(req, res, next) {
    Todo.addNewTodo(req.body.pid, req.body.phase, req.body.todo, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Todo added'
        }).end();
    });
});

/**
 * @api {delete} /project/investasi/todos Investasi - Delete Todo Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectTodoInvestasi
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   todo_id Todo ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo Deleted"
 *     }
 */
app.delete('/investasi/todos', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isParticipant], function(req, res, next) {
    Todo.deleteProjectTodo(req.body.pid, req.body.todo_id, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Todo deleted'
        }).end();
    });
});

/**
 * @api {patch} /project/investasi/todos Investasi - Update Project Todo
 * @apiVersion 0.1.0
 * @apiName UpdateProjectTodoInvestasi
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   todo_id Todo ID
 * @apiParam {String}   todo    Todo Text
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo updated"
 *     }
 */
app.patch('/investasi/todos', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isParticipant], function(req, res, next) {
    Todo.updateProjectTodo(req.body.pid, req.body.todo_id, req.body.todo, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Todo updated'
        }).end();
    })
});

/**
 * @api {patch} /project/investasi/todos/toggle Investasi - Toggle Todo Status
 * @apiVersion 0.1.0
 * @apiName UpdateProjectTodoInvestasiToggle
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   todo_id Todo ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo status toggled"
 *     }
 */
app.patch('/investasi/todos/toggle', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isParticipant], function(req, res, next) {
    Todo.toggleTodo(req.body.pid, req.body.todo_id, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Todo status toggled'
        }).end();
    })
});

/**
 * @api {get} /project/sosial/:pid/todos/:phase Sosial - Request Project Phase's To-Dos
 * @apiVersion 0.1.0
 * @apiName GetProjectTodoSosial
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} todos                          List of todos
 * @apiSuccess {Number}     todos.id                        To-do ID
 * @apiSuccess {String}     todos.description               To-do task description 
 * @apiSuccess {Number}     todos.is_checked                To-do checkmark (1: checked, 0: not checked)   
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 20,
 *              "description": "Meeting # 1 @ Office",
 *              "is_checked": 1
 *          }
 *     ]
 */
app.get('/sosial/:pid/todos/:phase', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL])], function(req, res, next) {
    Todo.getProjectTodos(req.params.pid, req.params.phase, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {post} /project/sosial/todos Sosial - Add Todo to Project
 * @apiVersion 0.1.0
 * @apiName AddProjectTodoSosial
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Current Stage Number
 * @apiParam {String}   todo    Todo text
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo added"
 *     }
 */
app.post('/sosial/todos', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isParticipant], function(req, res, next) {
    Todo.addNewTodo(req.body.pid, req.body.phase, req.body.todo, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Todo added'
        }).end();
    });
});

/**
 * @api {delete} /project/sosial/todos Sosial - Delete Todo Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectTodoSosial
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   todo_id Todo ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo Deleted"
 *     }
 */
app.delete('/sosial/todos', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isParticipant], function(req, res, next) {
    Todo.deleteProjectTodo(req.body.pid, req.body.todo_id, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Todo deleted'
        }).end();
    });
});

/**
 * @api {patch} /project/sosial/todos Sosial - Update Project Todo
 * @apiVersion 0.1.0
 * @apiName UpdateProjectTodoSosial
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   todo_id Todo ID
 * @apiParam {String}   todo    Todo Text
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo updated"
 *     }
 */
app.patch('/sosial/todos', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isParticipant], function(req, res, next) {
    Todo.updateProjectTodo(req.body.pid, req.body.todo_id, req.body.todo, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Todo updated'
        }).end();
    })
});

/**
 * @api {patch} /project/sosial/todos/toggle Sosial - Toggle Todo Status
 * @apiVersion 0.1.0
 * @apiName UpdateProjectTodoSosialToggle
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   todo_id Todo ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo status toggled"
 *     }
 */
app.patch('/sosial/todos/toggle', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isParticipant], function(req, res, next) {
    Todo.toggleTodo(req.body.pid, req.body.todo_id, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Todo status toggled'
        }).end();
    })
});

/**
 * @api {get} /project/dagang/:pid/todos/:phase Dagang - Request Project Phase's To-Dos
 * @apiVersion 0.1.0
 * @apiName GetProjectTodoDagang
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} todos                          List of todos
 * @apiSuccess {Number}     todos.id                        To-do ID
 * @apiSuccess {String}     todos.description               To-do task description 
 * @apiSuccess {Number}     todos.is_checked                To-do checkmark (1: checked, 0: not checked)   
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 20,
 *              "description": "Meeting # 1 @ Office",
 *              "is_checked": 1
 *          }
 *     ]
 */
app.get('/dagang/:pid/todos/:phase', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG])], function(req, res, next) {
    Todo.getProjectTodos(req.params.pid, req.params.phase, CATEGORIES.DAGANG, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {post} /project/dagang/todos Investasi - Add Todo to Project
 * @apiVersion 0.1.0
 * @apiName AddProjectTodoDagang
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Current Stage Number
 * @apiParam {String}   todo    Todo text
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo added"
 *     }
 */
app.post('/dagang/todos', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG]), isParticipant], function(req, res, next) {
    Todo.addNewTodo(req.body.pid, req.body.phase, req.body.todo, CATEGORIES.DAGANG, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Todo added'
        }).end();
    });
});

/**
 * @api {delete} /project/dagang/todos Dagang - Delete Todo Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectTodoDagang
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   todo_id Todo ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo Deleted"
 *     }
 */
app.delete('/dagang/todos', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG]), isParticipant], function(req, res, next) {
    Todo.deleteProjectTodo(req.body.pid, req.body.todo_id, CATEGORIES.DAGANG, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Todo deleted'
        }).end();
    });
});

/**
 * @api {patch} /project/dagang/todos Dagang - Update Project Todo
 * @apiVersion 0.1.0
 * @apiName UpdateProjectTodoDagang
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   todo_id Todo ID
 * @apiParam {String}   todo    Todo Text
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo updated"
 *     }
 */
app.patch('/dagang/todos', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG]), isParticipant], function(req, res, next) {
    Todo.updateProjectTodo(req.body.pid, req.body.todo_id, req.body.todo, CATEGORIES.DAGANG, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Todo updated'
        }).end();
    })
});

/**
 * @api {patch} /project/dagang/todos/toggle Dagang - Toggle Todo Status
 * @apiVersion 0.1.0
 * @apiName UpdateProjectTodoDagangToggle
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   todo_id Todo ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo status toggled"
 *     }
 */
app.patch('/dagang/todos/toggle', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG]), isParticipant], function(req, res, next) {
    Todo.toggleTodo(req.body.pid, req.body.todo_id, CATEGORIES.DAGANG, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Todo status toggled'
        }).end();
    })
});

/**
 * @api {get} /project/wisata/:pid/todos/:phase Wisata - Request Project Phase's To-Dos
 * @apiVersion 0.1.0
 * @apiName GetProjectTodoWisata
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} todos                          List of todos
 * @apiSuccess {Number}     todos.id                        To-do ID
 * @apiSuccess {String}     todos.description               To-do task description 
 * @apiSuccess {Number}     todos.is_checked                To-do checkmark (1: checked, 0: not checked)   
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 20,
 *              "description": "Meeting # 1 @ Office",
 *              "is_checked": 1
 *          }
 *     ]
 */
app.get('/wisata/:pid/todos/:phase', [isAuthenticated, isAuthorized([CATEGORIES.WISATA])], function(req, res, next) {
    Todo.getProjectTodos(req.params.pid, req.params.phase, CATEGORIES.WISATA, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {post} /project/wisata/todos Wisata - Add Todo to Project
 * @apiVersion 0.1.0
 * @apiName AddProjectTodoWisata
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Current Stage Number
 * @apiParam {String}   todo    Todo text
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo added"
 *     }
 */
app.post('/wisata/todos', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isParticipant], function(req, res, next) {
    Todo.addNewTodo(req.body.pid, req.body.phase, req.body.todo, CATEGORIES.WISATA, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Todo added'
        }).end();
    });
});

/**
 * @api {delete} /project/wisata/todos Wisata - Delete Todo Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectTodoWisata
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   todo_id Todo ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo Deleted"
 *     }
 */
app.delete('/wisata/todos', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isParticipant], function(req, res, next) {
    Todo.deleteProjectTodo(req.body.pid, req.body.todo_id, CATEGORIES.WISATA, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Todo deleted'
        }).end();
    });
});

/**
 * @api {patch} /project/wisata/todos Wisata - Update Project Todo
 * @apiVersion 0.1.0
 * @apiName UpdateProjectTodoWisata
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   todo_id Todo ID
 * @apiParam {String}   todo    Todo Text
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo updated"
 *     }
 */
app.patch('/wisata/todos', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isParticipant], function(req, res, next) {
    Todo.updateProjectTodo(req.body.pid, req.body.todo_id, req.body.todo, CATEGORIES.WISATA, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Todo updated'
        }).end();
    })
});

/**
 * @api {patch} /project/wisata/todos/toggle Wisata - Toggle Todo Status
 * @apiVersion 0.1.0
 * @apiName UpdateProjectTodoWisataToggle
 * @apiGroup Project Phase Todo
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   todo_id Todo ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Todo status toggled"
 *     }
 */
app.patch('/wisata/todos/toggle', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isParticipant], function(req, res, next) {
    Todo.toggleTodo(req.body.pid, req.body.todo_id, CATEGORIES.WISATA, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Todo status toggled'
        }).end();
    })
});

module.exports = app;