const loginController = require('./login');
const logoutController = require('./logout');
const projectController = require('./project');
const registerController = require('./register');
const roleController = require('./role');
const userController = require('./user');
const dashboardController = require('./dashboard');
const notificationController = require('./notification')
const dataController = require('./data');

/** 
 * @apiDefine BodyParam Body
 */

/**
 * @apiDefine QueryParam Query
 */

/**
 * @apiDefine Error5 Error 5xx
 */

/**
 * @apiDefine TokenHeader Header
 */

/**
 * @apiDefine HeaderTemplate
 * @apiHeader (TokenHeader) x-access-token User's token
 */

/**
 * @apiDefine ErrorTemplate
 * @apiError 401:Unauthorized API call not authorized, need correct authorization (see permission)
 * @apiError 403:Forbidden API call not authenticated, need to login
 * @apiError (Error5) 500:InternalServerError Error in server part, please redo
 * 
 * @apiErrorExample Error-Response:
 *      HTTP/1.1 401 UNAUTHORIZED
 *      {
 *          code: 401,
 *          message: "UNAUTHORIZED"
 *      }
 */

module.exports = { loginController, logoutController, projectController, registerController, roleController, userController, dashboardController, notificationController, dataController };