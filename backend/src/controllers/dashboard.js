const { Router: router } = require('express');
const { Dashboard }  = require('../models');
const { isAuthenticated, isAuthorized } = require('../middleware');
const { CATEGORIES } = require('../enum');

const app = router();

/**
 * @api {get} dashboard/investasi/monthly/phases Investasi - Get Ongoing Project Count Per Phase
 * @apiVersion 0.1.0
 * @apiName GetInvestasiOngoingProjectPerPhase
 * @apiGroup Dashboard
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 *
 * @apiSuccess {Numbers[]} project_count       List of project count per stage number    
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "project_count": [1,1,0,3,5,4,6]
 *          }
 *     ]
 */
app.get('/investasi/monthly/phases', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI])], function(req, res, next) {
    Dashboard.getOngoingProjectPerPhase(CATEGORIES.INVESTASI, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {get} dashboard/investasi/monthly/status Investasi - Get Project Count Per Status For Current Month
 * @apiVersion 0.1.0
 * @apiName GetInvestasiProjectCountStatusCurrentMonth
 * @apiGroup Dashboard
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 *
 * @apiSuccess {Numbers[]} project_count       List of project count per status for current month ([Finished, New Ongoing, Old Ongoing, Cancelled]) 
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "project_count": [1,1,0,3]
 *          }
 *     ]
 */
app.get('/investasi/monthly/status', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI])], function(req, res, next) {
    Dashboard.getCurrentMonthProjectPerStatus(CATEGORIES.INVESTASI, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/investasi/annually/status/completed', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI])], function(req, res, next) {
    Dashboard.getCompletedProjectAnnually(CATEGORIES.INVESTASI, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/investasi/annually/status/new', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI])], function(req, res, next) {
    Dashboard.getNewProjectAnnually(CATEGORIES.INVESTASI, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/investasi/annually/status/ongoing', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI])], function(req, res, next) {
    Dashboard.getOngoingProjectAnnually(CATEGORIES.INVESTASI, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/investasi/annually/status/cancelled', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI])], function(req, res, next) {
    Dashboard.getCancelledProjectAnnually(CATEGORIES.INVESTASI, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {get} dashboard/sosial/monthly/phases Sosial - Get Ongoing Project Count Per Phase
 * @apiVersion 0.1.0
 * @apiName GetSosialOngoingProjectPerPhase
 * @apiGroup Dashboard
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 *
 * @apiSuccess {Numbers[]} project_count       List of project count per stage number    
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "project_count": [1,1,0,3,5,4,6]
 *          }
 *     ]
 */
app.get('/sosial/monthly/phases', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL])], function(req, res, next)      {
    Dashboard.getOngoingProjectPerPhase(CATEGORIES.SOSIAL, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {get} dashboard/sosial/monthly/status Sosial - Get Project Count Per Status For Current Month
 * @apiVersion 0.1.0
 * @apiName GetSosialProjectCountStatusCurrentMonth
 * @apiGroup Dashboard
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 *
 * @apiSuccess {Numbers[]} project_count       List of project count per status for current month ([Finished, New Ongoing, Old Ongoing, Cancelled]) 
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "project_count": [1,1,0,3]
 *          }
 *     ]
 */
app.get('/sosial/monthly/status', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL])], function(req, res, next) {
    Dashboard.getCurrentMonthProjectPerStatus(CATEGORIES.SOSIAL, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/sosial/annually/status/completed', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL])], function(req, res, next) {
    Dashboard.getCompletedProjectAnnually(CATEGORIES.SOSIAL, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/sosial/annually/status/new', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL])], function(req, res, next) {
    Dashboard.getNewProjectAnnually(CATEGORIES.SOSIAL, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/sosial/annually/status/ongoing', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL])], function(req, res, next) {
    Dashboard.getOngoingProjectAnnually(CATEGORIES.SOSIAL, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/sosial/annually/status/cancelled', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL])], function(req, res, next) {
    Dashboard.getCancelledProjectAnnually(CATEGORIES.SOSIAL, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {get} dashboard/dagang/monthly/phases Dagang - Get Ongoing Project Count Per Phase
 * @apiVersion 0.1.0
 * @apiName GetDagangOngoingProjectPerPhase
 * @apiGroup Dashboard
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 *
 * @apiSuccess {Numbers[]} project_count       List of project count per stage number    
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "project_count": [1,1,0,3,5,4,6]
 *          }
 *     ]
 */
app.get('/dagang/monthly/phases', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG])], function(req, res, next)      {
    Dashboard.getOngoingProjectPerPhase(CATEGORIES.DAGANG, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {get} dashboard/dagang/monthly/status Dagang - Get Project Count Per Status For Current Month
 * @apiVersion 0.1.0
 * @apiName GetDagangProjectCountStatusCurrentMonth
 * @apiGroup Dashboard
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 *
 * @apiSuccess {Numbers[]} project_count       List of project count per status for current month ([Finished, New Ongoing, Old Ongoing, Cancelled]) 
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "project_count": [1,1,0,3]
 *          }
 *     ]
 */
app.get('/dagang/monthly/status', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG])], function(req, res, next) {
    Dashboard.getCurrentMonthProjectPerStatus(CATEGORIES.DAGANG, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/dagang/annually/status/completed', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG])], function(req, res, next) {
    Dashboard.getCompletedProjectAnnually(CATEGORIES.DAGANG, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/dagang/annually/status/new', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG])], function(req, res, next) {
    Dashboard.getNewProjectAnnually(CATEGORIES.DAGANG, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/dagang/annually/status/ongoing', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG])], function(req, res, next) {
    Dashboard.getOngoingProjectAnnually(CATEGORIES.DAGANG, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/dagang/annually/status/cancelled', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG])], function(req, res, next) {
    Dashboard.getCancelledProjectAnnually(CATEGORIES.DAGANG, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {get} dashboard/wisata/monthly/phases Wisata - Get Ongoing Project Count Per Phase
 * @apiVersion 0.1.0
 * @apiName GetWisataOngoingProjectPerPhase
 * @apiGroup Dashboard
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 *
 * @apiSuccess {Numbers[]} project_count       List of project count per stage number    
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "project_count": [1,1,0,3,5,4,6]
 *          }
 *     ]
 */
app.get('/wisata/monthly/phases', [isAuthenticated, isAuthorized([CATEGORIES.WISATA])], function(req, res, next)      {
    Dashboard.getOngoingProjectPerPhase(CATEGORIES.WISATA, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {get} dashboard/wisata/monthly/status Wisata - Get Project Count Per Status For Current Month
 * @apiVersion 0.1.0
 * @apiName GetWisataProjectCountStatusCurrentMonth
 * @apiGroup Dashboard
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 *
 * @apiSuccess {Numbers[]} project_count       List of project count per status for current month ([Finished, New Ongoing, Old Ongoing, Cancelled]) 
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "project_count": [1,1,0,3]
 *          }
 *     ]
 */
app.get('/wisata/monthly/status', [isAuthenticated, isAuthorized([CATEGORIES.WISATA])], function(req, res, next) {
    Dashboard.getCurrentMonthProjectPerStatus(CATEGORIES.WISATA, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/wisata/annually/status/completed', [isAuthenticated, isAuthorized([CATEGORIES.WISATA])], function(req, res, next) {
    Dashboard.getCompletedProjectAnnually(CATEGORIES.WISATA, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/wisata/annually/status/new', [isAuthenticated, isAuthorized([CATEGORIES.WISATA])], function(req, res, next) {
    Dashboard.getNewProjectAnnually(CATEGORIES.WISATA, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/wisata/annually/status/ongoing', [isAuthenticated, isAuthorized([CATEGORIES.WISATA])], function(req, res, next) {
    Dashboard.getOngoingProjectAnnually(CATEGORIES.WISATA, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

app.get('/wisata/annually/status/cancelled', [isAuthenticated, isAuthorized([CATEGORIES.WISATA])], function(req, res, next) {
    Dashboard.getCancelledProjectAnnually(CATEGORIES.WISATA, function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});


/**
 * @api {get} dashboard/observer Observer - Get Project Count For Ongoing and New Projects
 * @apiVersion 0.1.0
 * @apiName GetObserverProjectCount
 * @apiGroup Dashboard
 * 
 * @apiPermission None
 * 
 * @apiUse HeaderTemplate
 *
 * @apiSuccess {Object}   statistics              List of projects in each category
 * @apiSuccess {Number}     statistics.ongoing      Number of ongoing projects
 * @apiSuccess {Number}     statistics.new          Number of new projects (ongoing projects created after the start of this month)
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *      {
 *          "ongoing": 0,
 *          "new": 1
 *      }
 */
app.get('/observer', function(req, res, next) {
    Dashboard.getObserverStatistics(function(err, result) {
        if (err) next (err)
        else res.status(200).json(result).end();
    });
});

module.exports = app;