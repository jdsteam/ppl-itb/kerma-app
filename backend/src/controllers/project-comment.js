const { Router: router } = require('express');
const { Comment } = require('../models');
const { isAuthenticated, isAuthorized, isParticipant } = require('../middleware');
const { CATEGORIES } = require('../enum');

const app = router();


//GET Comments
/**
 * @api {get} /project/investasi/:pid/comments/:phase Investasi - Request Project Phase's Comments
 * @apiVersion 0.1.0
 * @apiName GetProjectCommentInvestasi
 * @apiGroup Project Phase Comment
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} comments                          List of comments
 * @apiSuccess {Number}     comments.id                        Comment ID
 * @apiSuccess {Number}     comments.comment                   Comment content     
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 30,
 *              "comment": "Wow what a nice project this is"
 *          },
 *          {
 *              "id": 29,
 *              "comment": "I think we need to do a meeting for this subject"
 *          }
 *     ]
 */
app.get('/investasi/:pid/comments/:phase', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI])], function(req, res, next) {
    Comment.getProjectComments(req.params.pid, req.params.phase, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});


/**
 * @api {post} /project/investasi/comments Investasi - Add New Comment to Project Phase
 * @apiVersion 0.1.0
 * @apiName AddProjectCommentInvestasi
 * @apiGroup Project Phase Comment
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 * @apiParam {String}   comment Comment Text
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Comment added"
 *     }
 */
app.post('/investasi/comments', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isParticipant], function(req, res, next) {
    Comment.addNewComment(req.body.pid, req.body.phase, req.body.comment, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Comment added'
        }).end();
    });
})

/**
 * @api {delete} /project/investasi/comments Investasi - Delete Comment Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectCommentInvestasi
 * @apiGroup Project Phase Comment
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   comment_id  Comment ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Comment Deleted"
 *     }
 */
app.delete('/investasi/comments', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isParticipant], function(req, res, next) {
    Comment.deleteProjectComment(req.body.pid, req.body.comment_id, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Comment deleted'
        }).end();
    })
})

/**
 * @api {get} /project/sosial/:pid/comments/:phase Sosial - Request Project Phase's Comments
 * @apiVersion 0.1.0
 * @apiName GetProjectCommentSosial
 * @apiGroup Project Phase Comment
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} comments                          List of comments
 * @apiSuccess {Number}     comments.id                        Comment ID
 * @apiSuccess {Number}     comments.comment                   Comment content     
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 30,
 *              "comment": "Wow what a nice project this is"
 *          },
 *          {
 *              "id": 29,
 *              "comment": "I think we need to do a meeting for this subject"
 *          }
 *     ]
 */
app.get('/sosial/:pid/comments/:phase', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL])], function(req, res, next) {
    Comment.getProjectComments(req.params.pid, req.params.phase, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {post} /project/sosial/comments Sosial - Add New Comment to Project Phase
 * @apiVersion 0.1.0
 * @apiName AddProjectCommentSosial
 * @apiGroup Project Phase Comment
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 * @apiParam {String}   comment Comment Text
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Comment added"
 *     }
 */
app.post('/sosial/comments', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isParticipant], function(req, res, next) {
    Comment.addNewComment(req.body.pid, req.body.phase, req.body.comment, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Comment added'
        }).end();
    });
})

/**
 * @api {delete} /project/sosial/comments Sosial - Delete Comment Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectCommentSosial
 * @apiGroup Project Phase Comment
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   comment_id  Comment ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Comment Deleted"
 *     }
 */
app.delete('/sosial/comments', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isParticipant], function(req, res, next) {
    Comment.deleteProjectComment(req.body.pid, req.body.comment_id, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Comment deleted'
        }).end();
    })
})

/**
 * @api {get} /project/dagang/:pid/comments/:phase Dagang - Request Project Phase's Comments
 * @apiVersion 0.1.0
 * @apiName GetProjectCommentDagang
 * @apiGroup Project Phase Comment
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} comments                          List of comments
 * @apiSuccess {Number}     comments.id                        Comment ID
 * @apiSuccess {Number}     comments.comment                   Comment content     
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 30,
 *              "comment": "Wow what a nice project this is"
 *          },
 *          {
 *              "id": 29,
 *              "comment": "I think we need to do a meeting for this subject"
 *          }
 *     ]
 */
app.get('/dagang/:pid/comments/:phase', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG])], function(req, res, next) {
    Comment.getProjectComments(req.params.pid, req.params.phase, CATEGORIES.DAGANG, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {post} /project/dagang/comments Dagang - Add New Comment to Project Phase
 * @apiVersion 0.1.0
 * @apiName AddProjectCommentDagang
 * @apiGroup Project Phase Comment
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 * @apiParam {String}   comment Comment Text
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Comment added"
 *     }
 */
app.post('/dagang/comments', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG]), isParticipant], function(req, res, next) {
    Comment.addNewComment(req.body.pid, req.body.phase, req.body.comment, CATEGORIES.DAGANG, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Comment added'
        }).end();
    });
})

/**
 * @api {delete} /project/dagang/comments Dagang - Delete Comment Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectCommentDagang
 * @apiGroup Project Phase Comment
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   comment_id  Comment ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Comment Deleted"
 *     }
 */
app.delete('/dagang/comments', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG])], isParticipant, function(req, res, next) {
    Comment.deleteProjectComment(req.body.pid, req.body.comment_id, CATEGORIES.DAGANG, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Comment deleted'
        }).end();
    })
})

/**
 * @api {get} /project/wisata/:pid/comments/:phase Wisata - Request Project Phase's Comments
 * @apiVersion 0.1.0
 * @apiName GetProjectCommentWisata
 * @apiGroup Project Phase Comment
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} comments                          List of comments
 * @apiSuccess {Number}     comments.id                        Comment ID
 * @apiSuccess {Number}     comments.comment                   Comment content     
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 30,
 *              "comment": "Wow what a nice project this is"
 *          },
 *          {
 *              "id": 29,
 *              "comment": "I think we need to do a meeting for this subject"
 *          }
 *     ]
 */
app.get('/wisata/:pid/comments/:phase', [isAuthenticated, isAuthorized([CATEGORIES.WISATA])], function(req, res, next) {
    Comment.getProjectComments(req.params.pid, req.params.phase, CATEGORIES.WISATA, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {post} /project/wisata/comments Wisata - Add New Comment to Project Phase
 * @apiVersion 0.1.0
 * @apiName AddProjectCommentWisata
 * @apiGroup Project Phase Comment
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 * @apiParam {String}   comment Comment Text
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Comment added"
 *     }
 */
app.post('/wisata/comments', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isParticipant], function(req, res, next) {
    Comment.addNewComment(req.body.pid, req.body.phase, req.body.comment, CATEGORIES.WISATA, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Comment added'
        }).end();
    });
});

/**
 * @api {delete} /project/wisata/comments Wisata - Delete Comment Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectCommentWisata
 * @apiGroup Project Phase Comment
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   comment_id  Comment ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Comment Deleted"
 *     }
 */
app.delete('/wisata/comments', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isParticipant], function(req, res, next) {
    Comment.deleteProjectComment(req.body.pid, req.body.comment_id, CATEGORIES.WISATA, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Comment deleted'
        }).end();
    });
});

module.exports = app;