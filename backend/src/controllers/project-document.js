const { Router: router } = require('express');
const { Document } = require('../models');
const { isAuthenticated, isAuthorized, isParticipant } = require('../middleware');
const { CATEGORIES } = require('../enum');

const app = router();


//GET Documents
/**
 * @api {get} /project/investasi/:pid/documents/:phase Investasi - Request Project Phase's Documents
 * @apiVersion 0.1.0
 * @apiName GetProjectDocumentInvestasi
 * @apiGroup Project Phase Document
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} documents                          List of documents
 * @apiSuccess {Number}     documents.id                        Document ID
 * @apiSuccess {String}     documents.name                      Document title name  
 * @apiSuccess {String}     documents.document                  Document link   
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 25,
 *              "name": "Report ver_1.pdf",
 *              "document": "https://drive.google.com/file_path"
 *          }
 *     ]
 */
app.get('/investasi/:pid/documents/:phase', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI])], function(req, res, next) {
    Document.getProjectDocuments(req.params.pid, req.params.phase, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {post} /project/investasi/documents Investasi - Add Document to Project
 * @apiVersion 0.1.0
 * @apiName AddProjectDocumentInvestasi
 * @apiGroup Project Phase Document
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Current Stage Number
 * @apiParam {String}   name    Document Name
 * @apiParam {String}   document    Document Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Document added"
 *     }
 */
app.post('/investasi/documents', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isParticipant], function(req, res, next) {
    Document.addNewDocument(req.body.pid, req.body.phase, req.body.name, req.body.document, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Document added'
        }).end();
    });
});

/**
 * @api {delete} /project/investasi/documents Investasi - Delete Document Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectDocumentInvestasi
 * @apiGroup Project Phase Document
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   document_id  Document ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Document Deleted"
 *     }
 */
app.delete('/investasi/documents', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isParticipant], function(req, res, next) {
    Document.deleteProjectDocument(req.body.pid, req.body.document_id, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Document deleted'
        }).end();
    });
});

/**
 * @api {patch} /project/investasi/documents Investasi - Update Project Document
 * @apiVersion 0.1.0
 * @apiName UpdateProjectDocumentInvestasi
 * @apiGroup Project Phase Document
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   document_id   Document ID
 * @apiParam {String}   name    New Document Name
 * @apiParam {String}   document    New Document Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Document updated"
 *     }
 */
app.patch('/investasi/documents', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isParticipant], function(req, res, next) {
    Document.updateProjectDocument(req.body.pid, req.body.document_id, req.body.name, req.body.document, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Document updated'
        }).end();
    })
});

/**
 * @api {get} /project/sosial/:pid/documents/:phase Sosial - Request Project Phase's Documents
 * @apiVersion 0.1.0
 * @apiName GetProjectDocumentSosial
 * @apiGroup Project Phase Document
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} documents                          List of documents
 * @apiSuccess {Number}     documents.id                        Document ID
 * @apiSuccess {String}     documents.name                      Document title name  
 * @apiSuccess {String}     documents.document                  Document link   
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 25,
 *              "name": "Report ver_1.pdf",
 *              "document": "https://drive.google.com/file_path"
 *          }
 *     ]
 */
app.get('/sosial/:pid/documents/:phase', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL])], function(req, res, next) {
    Document.getProjectDocuments(req.params.pid, req.params.phase, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end()
    });
});

/**
 * @api {post} /project/sosial/documents Sosial - Add Document to Project
 * @apiVersion 0.1.0
 * @apiName AddProjectDocumentSosial
 * @apiGroup Project Phase Document
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Current Stage Number
 * @apiParam {String}   name    Document Name
 * @apiParam {String}   document    Document Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Document added"
 *     }
 */
app.post('/sosial/documents', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isParticipant], function(req, res, next) {
    Document.addNewDocument(req.body.pid, req.body.phase, req.body.name, req.body.document, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Document added'
        }).end();
    });
});

/**
 * @api {delete} /project/sosial/documents Sosial - Delete Document Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectDocumentSosial
 * @apiGroup Project Phase Document
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   document_id  Document ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Document Deleted"
 *     }
 */
app.delete('/sosial/documents', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isParticipant], function(req, res, next) {
    Document.deleteProjectDocument(req.body.pid, req.body.document_id, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Document deleted'
        }).end();
    });
});

/**
 * @api {patch} /project/sosial/documents Sosial - Update Project Document
 * @apiVersion 0.1.0
 * @apiName UpdateProjectDocumentSosial
 * @apiGroup Project Phase Document
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   document_id   Document ID
 * @apiParam {String}   name    New Document Name
 * @apiParam {String}   document    New Document Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Document updated"
 *     }
 */
app.patch('/sosial/documents', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isParticipant], function(req, res, next) {
    Document.updateProjectDocument(req.body.pid, req.body.document_id, req.body.name, req.body.document, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Document updated'
        }).end();
    })
});

/**
 * @api {get} /project/dagang/:pid/documents/:phase Dagang - Request Project Phase's Documents
 * @apiVersion 0.1.0
 * @apiName GetProjectDocumentDagang
 * @apiGroup Project Phase Document
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} documents                          List of documents
 * @apiSuccess {Number}     documents.id                        Document ID
 * @apiSuccess {String}     documents.name                      Document title name  
 * @apiSuccess {String}     documents.document                  Document link   
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 25,
 *              "name": "Report ver_1.pdf",
 *              "document": "https://drive.google.com/file_path"
 *          }
 *     ]
 */
app.get('/dagang/:pid/documents/:phase', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG])], function(req, res, next) {
    Document.getProjectDocuments(req.params.pid, req.params.phase, CATEGORIES.DAGANG, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end()
    });
});

/**
 * @api {post} /project/dagang/documents Dagang - Add Document to Project
 * @apiVersion 0.1.0
 * @apiName AddProjectDocumentDagang
 * @apiGroup Project Phase Document
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Current Stage Number
 * @apiParam {String}   name    Document Name
 * @apiParam {String}   document    Document Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Document added"
 *     }
 */
app.post('/dagang/documents', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG]), isParticipant], function(req, res, next) {
    Document.addNewDocument(req.body.pid, req.body.phase, req.body.name, req.body.document, CATEGORIES.DAGANG, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Document added'
        }).end();
    });
});

/**
 * @api {delete} /project/dagang/documents Dagang - Delete Document Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectDocumentDagang
 * @apiGroup Project Phase Document
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   document_id  Document ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Document Deleted"
 *     }
 */
app.delete('/dagang/documents', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG]), isParticipant], function(req, res, next) {
    Document.deleteProjectDocument(req.body.pid, req.body.document_id, CATEGORIES.DAGANG, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Document deleted'
        }).end();
    });
});

/**
 * @api {patch} /project/dagang/documents Dagang - Update Project Document
 * @apiVersion 0.1.0
 * @apiName UpdateProjectDocumentDagang
 * @apiGroup Project Phase Document
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   document_id   Document ID
 * @apiParam {String}   name    New Document Name
 * @apiParam {String}   document    New Document Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Document updated"
 *     }
 */
app.patch('/dagang/documents', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG]), isParticipant], function(req, res, next) {
    Document.updateProjectDocument(req.body.pid, req.body.document_id, req.body.name, req.body.document, CATEGORIES.DAGANG, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Document updated'
        }).end();
    })
});

/**
 * @api {get} /project/wisata/:pid/documents/:phase Wisata - Request Project Phase's Documents
 * @apiVersion 0.1.0
 * @apiName GetProjectDocumentWisata
 * @apiGroup Project Phase Document
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Phase stage number
 *
 * @apiSuccess {Object[]} documents                          List of documents
 * @apiSuccess {Number}     documents.id                        Document ID
 * @apiSuccess {String}     documents.name                      Document title name  
 * @apiSuccess {String}     documents.document                  Document link   
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": 25,
 *              "name": "Report ver_1.pdf",
 *              "document": "https://drive.google.com/file_path"
 *          }
 *     ]
 */
app.get('/wisata/:pid/documents/:phase', [isAuthenticated, isAuthorized([CATEGORIES.WISATA])], function(req, res, next) {
    Document.getProjectDocuments(req.params.pid, req.params.phase, CATEGORIES.WISATA, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end()
    });
});

/**
 * @api {post} /project/wisata/documents Wisata - Add Document to Project
 * @apiVersion 0.1.0
 * @apiName AddProjectDocumentWisata
 * @apiGroup Project Phase Document
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   phase   Current Stage Number
 * @apiParam {String}   name    Document Name
 * @apiParam {String}   document    Document Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Document added"
 *     }
 */
app.post('/wisata/documents', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isParticipant], function(req, res, next) {
    Document.addNewDocument(req.body.pid, req.body.phase, req.body.name, req.body.document, CATEGORIES.WISATA, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Document added'
        }).end();
    });
});

/**
 * @api {delete} /project/wisata/documents Wisata - Delete Document Within a Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectDocumentWisata
 * @apiGroup Project Phase Document
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   document_id  Document ID
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Document Deleted"
 *     }
 */
app.delete('/wisata/documents', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isParticipant], function(req, res, next) {
    Document.deleteProjectDocument(req.body.pid, req.body.document_id, CATEGORIES.WISATA, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Document deleted'
        }).end();
    });
});

/**
 * @api {patch} /project/wisata/documents Wisata - Update Project Document
 * @apiVersion 0.1.0
 * @apiName UpdateProjectDocumentWisata
 * @apiGroup Project Phase Document
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   document_id   Document ID
 * @apiParam {String}   name    New Document Name
 * @apiParam {String}   document    New Document Link
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Document updated"
 *     }
 */
app.patch('/wisata/documents', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isParticipant], function(req, res, next) {
    Document.updateProjectDocument(req.body.pid, req.body.document_id, req.body.name, req.body.document, CATEGORIES.WISATA, function(err, result) {
        if (err) next (err)
        else res.status(200).json({
            status: 200,
            message: 'Document updated'
        }).end();
    })
});

module.exports = app;