const { Router: router } = require('express');
const { User } = require('../models');

const app = router();

app.post('/', function(req, res, next) {
    User.addUser(req.body.name, req.body.username, req.body.email, req.body.password, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Register successful'
        }).end();
    });
});

module.exports = app;