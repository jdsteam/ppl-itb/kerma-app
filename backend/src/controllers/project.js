const { Router: router } = require('express');
const { Project } = require('../models');
const { isAuthenticated, isAuthorized, isAuthorizedToEditProject, isParticipant } = require('../middleware');
const moment = require('moment');
const { CATEGORIES } = require('../enum');
const projectComment = require('./project-comment');
const projectDocument = require('./project-document');
const projectPhoto = require('./project-photo');
const projectTodo = require('./project-todo');

const app = router();


//GET Project
/**
 * @api {get} /project/investasi Investasi - Request List of Project
 * @apiVersion 0.1.0
 * @apiName GetAllProjectInvestasi
 * @apiGroup Project
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam (QueryParam) {Number}  status  Filter by project status (1: In progress, 2: Completed, 3: Cancelled)
 * @apiParam (QueryParam) {Number}  phase   Filter by project current phase stage number (0: No filter)
 * @apiParam (QueryParam) {Number}  sort    Sort user (1: Sort by newest created, 2: Sort by oldest updated, 3: Sort by phase number, 4: Sort by name)
 * @apiParam (QueryParam) {Number}  participate Filter by user participation in project (0: No filter, 1: Filter on)
 * 
 * @apiSuccess {Object[]} projects                          List of projects
 * @apiSuccess {Number}     projects.pid                    Project ID
 * @apiSuccess {String}     projects.name                   Name
 * @apiSuccess {String}     projects.pic                    Project's Person in Charge
 * @apiSuccess {Number}     projects.curr_phase_stage       Project's current phase stage number
 * @apiSuccess {Date}       projects.due_date               Project's current phase deadline
 * @apiSuccess {Date}       projects.updated_at             Project's last updated time        
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "pid": 1,
 *              "name": "Jabar Invest Proposition",
 *              "pic": "johndoe",
 *              "curr_phase_stage": 1,
 *              "due_date": "2019-05-09 00:00:00",
 *              "updated_at": "2019-05-09 13:14:25"
 *          }
 *     ]
 */
app.get('/investasi', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI])], function(req, res, next) {
    Project.getAllProject(CATEGORIES.INVESTASI, req.query.status, req.query.phase, req.query.sort, req.query.participate, res.locals.auth.userId, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {get} /project/investasi/:pid   Investasi - Request Project by Project ID
 * @apiVersion 0.1.0
 * @apiName GetProjectInvestasi
 * @apiGroup Project
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {Number}  pid  Project ID
 * 
 * @apiSuccess {String}     name                       Project Name  
 * @apiSuccess {Number}     curr_phase_stage           Current Phase Project   
 * @apiSuccess {String}     phase_deadline.stage_num   Due date of each stage num                
 * @apiSuccess {String}     status                     Current status of project (1 = ongoing, 2 = finished, 3 = cancelled)
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "name": "Rehab IF",
 *         "curr_phase_stage": 1,
 *         "phase_deadline": {
 *            "1": "2019-05-09 18:33:43"
 *         },
 *         "status": 1
 *     }
 */
app.get('/investasi/:pid', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI])], function(req, res, next) {
    Project.getProject(req.params.pid, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {get} /project/investasi/:pid/edit   Investasi - Request Complete Project Details
 * @apiVersion 0.1.0
 * @apiName GetProjectInvestasiEditDetails
 * @apiGroup Project
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {Number}  pid  Project ID
 * 
 * @apiSuccess {Number}     id                          Project ID
 * @apiSuccess {String}     name                        Project Name  
 * @apiSuccess {Number}     category                    Project Category
 * @apiSuccess {Number}     curr_phase_stage            Current Phase Project   
 * @apiSuccess {String}     company_name                Name of project partner
 * @apiSuccess {String}     company_address             Address of project partner
 * @apiSuccess {String}     cp_name                     Name of project partner's representative
 * @apiSuccess {String}     cp_phone                    Phone number of project partner
 * @apiSuccess {String}     cp_email                    Email of project partner
 * @apiSuccess {String}     description                 Brief description of the project
 * @apiSuccess {String}     created_at                  Project creation date
 * @apiSuccess {String}     updated_at                  Project last update's date
 * @apiSuccess {Number}     status                      Current project status
 * @apiSuccess {String}     country                     Project partner's country of origin
 * @apiSuccess {Object}     phase_deadline              Contains deadlines for each phase
 * @apiSuccess {String}     phase_deadline.1            Deadline for project phase 1
 * @apiSuccess {String}     phase_deadline.2            Deadline for project phase 2
 * @apiSuccess {String}     phase_deadline.3            Deadline for project phase 3
 * @apiSuccess {String}     phase_deadline.4            Deadline for project phase 4
 * @apiSuccess {String}     phase_deadline.5            Deadline for project phase 5
 * @apiSuccess {String}     phase_deadline.6            Deadline for project phase 6
 * @apiSuccess {String}     phase_deadline.7            Deadline for project phase 7
 * @apiSuccess {Number[]}   perangkat                   Array of indices of perangkat involved
 * 
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *           "id": 29,
 *           "name": "Test Project 4",
 *           "category": 1,
 *           "pic": "test",
 *           "curr_phase_stage": 1,
 *           "company_name": "Tirta",
 *           "company_address": "Bandung",
 *           "cp_name": "Jon",
 *           "cp_phone": "0812",
 *           "cp_email": "a@gmail.com",
 *           "description": "Testing Project",
 *           "created_at": "2019-05-15T13:52:24.000Z",
 *           "updated_at": "2019-05-15T13:52:24.000Z",
 *           "status": 1,
 *           "country": "Indonesia",
 *           "phase_deadline": {
 *               "1": "2020-03-01 00:00:00",
 *               "2": "2020-03-01 00:00:00",
 *               "3": "2020-03-01 00:00:00",
 *               "4": "2020-03-01 00:00:00",
 *               "5": "2020-03-01 00:00:00",
 *               "6": "2020-03-01 00:00:00",
 *               "7": "2020-03-01 00:00:00"
 *           },
 *           "perangkat": [
 *               1,
 *              10,
 *               13
 *           ],
 *           "list_perangkat": [
 *               "Biro Pemerintahan dan Kerjasama",
 *               "Biro Hukum dan Hak Asasi Manusia",
 *               "Biro Pelayanan dan Pengembangan Sosial",
 *               "Biro Badan Usaha Milik Daerah dan Investasi",
 *               "Biro Perekonomian",
 *               "Biro Pengadaan Barang/Jasa",
 *               "Biro Organisasi",
 *               "Biro Hubungan Masyarakat dan Protokol",
 *               "Biro Umum",
 *               "Dinas Pendidikan",
 *               "Dinas Kesehatan",
 *               "Dinas Bina Marga dan Penataan Ruang",
 *               "Dinas Sumber Daya Air",
 *               "Dinas Perumahan dan Pemukiman",
 *               "Dinas Sosial",
 *               "Dinas Pemberdayaan Perempuan, Perlindungan Anak, dan Keluarga Berencana",
 *               "Dinas Lingkungan Hidup",
 *               "Dinas Pemberdayaan Masyarakat dan Desa",
 *               "Dinas Perhubungan",
 *               "Dinas Komunikasi dan Informatika",
 *               "Dinas Koperasi dan Usaha Kecil",
 *               "Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu",
 *               "Dinas Pemuda dan Olahraga",
 *               "Dinas Perpustakaan dan Kearsipan Daerah",
 *               "Dinas Tenaga Kerja dan Transmigrasi",
 *               "Dinas Ketahanan Pangan dan Peternakan",
 *               "Dinas Pariwisata dan Kebudayaan",
 *               "Dinas Kelautan dan Perikanan",
 *               "Dinas Tanaman Pangan dan Holtikultura",
 *               "Dinas Perkebunan",
 *               "Dinas Kehutanan",
 *               "Dinas Energi dan Sumber Daya Mineral",
 *               "Dinas Perindustrian dan Perdagangan",
 *               "Dinas Kependudukan dan Pencatatan Sipil",
 *               "Badan Perencanaan Pembangunan Daerah",
 *               "Badan Kepegawaian Daerah",
 *               "Badan Pengembangan Sumber Daya Manusia",
 *               "Badan Penelitian dan Pengembangan Daerah",
 *               "Badan Pengelolaan Keuangan dan Aset Daerah",
 *               "Badan Pendapatan Daerah",
 *               "Badan Penanggulangan Bencana Daerah"
 *           ]
 *       }
 */
app.get('/investasi/:pid/edit', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI])], function(req, res, next) {
    Project.getProjectDetails(req.params.pid, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end()
    });
});

/**
 * @api {get} /project/investasi/:pid/participants   Investasi - Request Participants in a Project
 * @apiVersion 0.1.0
 * @apiName GetProjectParticipantsInvestasi
 * @apiGroup Project
 * 
 * @apiPermission investasi
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {Number}  pid  Project ID
 * 
 * @apiSuccess {Object[]}   participants               List of participants in a project
 * @apiSuccess {Number}     participants.uid           Current Phase Project   
 * @apiSuccess {String}     participants.name          Due date of each stage num                
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *      {
 *          "name": "test",
 *           "uid": 1
 *      },
 *       {
 *         "name": "nico",
 *           "uid": 2
 *      }
 *     ]
]
 */
app.get('/investasi/:pid/participants', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI])], function(req, res, next) {
    Project.getProjectParticipants(req.params.pid, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end()
    });
});

/**
 * @api {patch} /project/investasi/status Investasi - Update Project Status
 * @apiVersion 0.1.0
 * @apiName UpdateProjectStatusInvestasi
 * @apiGroup Project
 * 
 * @apiPermission investasi, pic
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   new_status  New Project Status
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Project status updated"
 *     }
 */
app.patch('/investasi/status', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isAuthorizedToEditProject], function(req, res, next) {
    Project.changeProjectStatus(req.body.pid, req.body.new_status, CATEGORIES.INVESTASI, function (err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Project status updated'
        }).end();
    })
})

/**
 * @api {patch} /project/investasi/phase Investasi - Change project phase
 * @apiVersion 0.1.0
 * @apiName UpdateProjectPhaseInvestasi
 * @apiGroup Project
 * 
 * @apiPermission investasi, pic
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   new_phase  New Project Phase
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Project phase updated"
 *     }
 */
app.patch('/investasi/phase', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isAuthorizedToEditProject], function(req, res, next) {
    Project.changeProjectPhase(req.body.pid, req.body.new_phase, CATEGORIES.INVESTASI, function (err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Project phase updated'
        }).end();
    })
});

/**
 * @api {patch} /project/investasi/edit   Investasi - Edit Project
 * @apiVersion 0.1.0
 * @apiName EditProjectInvestasi
 * @apiGroup Project
 * 
 * @apiPermission investasi, pic
 * 
 * @apiUse HeaderTemplate
 *
 * @apiparam  {Number}   pid                Project ID
 * @apiParam  {String}   name               Project Name
 * @apiParam  {Number}   pic_name           Project Manager username (current logged in user)
 * @apiParam  {String}   cp_name            Name of Contact Person
 * @apiParam  {String}   company_name       Name of partner company
 * @apiParam  {String}   company_address    Company's address
 * @apiParam  {String}   cp_phone           Contact person's phone
 * @apiParam  {String}   email              Contact person's email
 * @apiParam  {String}   Description        Project description
 * @apiParam  {String}   country            Project partner's country of origin
 * @apiParam  {String}   due_date1          Due date of phase 1
 * @apiParam  {String}   due_date2          Due date of phase 2
 * @apiParam  {String}   due_date3          Due date of phase 3
 * @apiParam  {String}   due_date4          Due date of phase 4
 * @apiParam  {String}   due_date5          Due date of phase 5
 * @apiParam  {String}   due_date6          Due date of phase 6
 * @apiParam  {String}   due_date7          Due date of phase 7
 * @apiParam  {Number[]} perangkat          List of perangkat by index
 * @apiParam  {String}   latest_update      Timestamp of latest update in format YYYY-MM-DD
 *               
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 200,
 *         "message": "Project edited",
 *     }
 */
app.patch('/investasi/edit', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isAuthorizedToEditProject], function(req, res, next) {
    if (req.body.latest_update && moment(req.body.latest_update).isAfter(moment())) {
        res.status(401).json({
            status: 401,
            message: 'DATE INVALID'
        }).end();
    }
    else {
        Project.editProject(req.body.pid, req.body.name, CATEGORIES.INVESTASI, req.body.pic_name, req.body.cp_name, req.body.company_name, req.body.company_address, req.body.cp_phone, req.body.cp_email, req.body.description,
            req.body.country,
            req.body.due_date1,
            req.body.due_date2,
            req.body.due_date3,
            req.body.due_date4,
            req.body.due_date5,
            req.body.due_date6,
            req.body.due_date7,
            req.body.perangkat,
            req.body.latest_update, function(err, result) {
            if (err) next(err)
            else res.status(200).json({
                status: 200,
                message: 'Project edited'
            }).end();
        });
    }
});

//POST Project

/**
 * @api {post} /project/investasi   Investasi - Create New Project
 * @apiVersion 0.1.0
 * @apiName AddProjectInvestasi
 * @apiGroup Project
 * 
 * @apiPermission investasi, manager
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {String}   name               Project Name
 * @apiParam  {Number}   pic_name           Project Manager username (current logged in user)
 * @apiParam  {String}   cp_name            Name of Contact Person
 * @apiParam  {String}   company_name       Name of partner company
 * @apiParam  {String}   company_address    Company's address
 * @apiParam  {String}   cp_phone           Contact person's phone
 * @apiParam  {String}   email              Contact person's email
 * @apiParam  {String}   Description        Project description
 * @apiParam  {String}   country            Project partner's country of origin
 * @apiParam  {String}   due_date1          Due date of phase 1
 * @apiParam  {String}   due_date2          Due date of phase 2
 * @apiParam  {String}   due_date3          Due date of phase 3
 * @apiParam  {String}   due_date4          Due date of phase 4
 * @apiParam  {String}   due_date5          Due date of phase 5
 * @apiParam  {String}   due_date6          Due date of phase 6
 * @apiParam  {String}   due_date7          Due date of phase 7
 * @apiParam  {Number[]} perangkat          List of perangkat by index
 *               
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 200,
 *         "message": "Project added",
 *     }
 */
app.post('/investasi', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.INVESTASI])], function(req, res, next) {
    Project.addProject(req.body.name, CATEGORIES.INVESTASI, req.body.pic_name, req.body.cp_name, req.body.company_name, req.body.company_address, req.body.cp_phone, req.body.cp_email, req.body.description,
        req.body.country,
        req.body.due_date1,
        req.body.due_date2,
        req.body.due_date3,
        req.body.due_date4,
        req.body.due_date5,
        req.body.due_date6,
        req.body.due_date7,
        req.body.perangkat, function(err, result) {
            if (err) next(err)
            else res.status(200).json({
                status: 200,
                message: 'Project added'
            }).end();
    });
});

//DELETE Project
/**
 * @api {delete} /project/investasi Investasi - Delete Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectInvestasi
 * @apiGroup Project
 * 
 * @apiPermission investasi, manager
 *
 * @apiUse HeaderTemplate
 * 
 * @apiParam    (BodyParam) {Number}    pid         Project ID   
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Project deleted"
 *     }
 * 
 * @apiUse ErrorTemplate
 */
app.delete('/investasi', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.INVESTASI])], function(req, res, next) {
    Project.deleteProject(req.body.pid, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Project deleted'
        }).end();
    });
});

/**
 * @api {post} /project/investasi/participant Investasi - Add participant
 * @apiVersion 0.1.0
 * @apiName AddParticipantInvestasi
 * @apiGroup Project
 * 
 * @apiPermission investasi, manager
 * 
 * @apiUse HeaderTemplate
 *
 * 
 * @apiParam {Number}     pid                        Project ID
 * @apiParam {String[]}   participants               List of add user to participate
 * 
 *        
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": 200,
 *        "message": "Participant added"
 *     }
 */
app.post('/investasi/participant', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.INVESTASI])], function(req, res, next) {
    Project.addParticipant(req.body.pid, req.body.participants, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Participant Added'
        }).end();
    });
});

/**
 * @api {delete} /project/investasi/participant Investasi - Delete Project Participant
 * @apiVersion 0.1.0
 * @apiName DeleteProjectParticipantInvestasi
 * @apiGroup Project
 * 
 * @apiPermission investasi, pic
 *
 * @apiUse HeaderTemplate
 * 
 * @apiParam    (BodyParam) {Number}    pid         Project ID   
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Participant deleted"
 *     }
 * 
 * @apiUse ErrorTemplate
 */
app.delete('/investasi/participant', [isAuthenticated, isAuthorized([CATEGORIES.INVESTASI]), isAuthorizedToEditProject], function(req, res, next) {
    Project.deleteProjectParticipant(req.body.pid, req.body.uid, CATEGORIES.INVESTASI, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Participant Deleted'
        }).end();
    });
});

/**
 * @api {patch} /project/investasi/last_update   Investasi - Change Last Update Date
 * @apiVersion 0.1.0
 * @apiName EditProjectUpdateDateInvestasi
 * @apiGroup Project
 * 
 * @apiPermission investasi, manager
 * 
 * @apiUse HeaderTemplate
 *
 * @apiparam  {Number}   pid                Project ID
 * @apiParam  {String}   new_date           New date in string format : YYYY-MM-DD
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 200,
 *         "message": "Project Last update date changed",
 *     }
 */
app.patch('/investasi/last_update', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.INVESTASI])], function(req, res, next) {
    if (req.body.new_date && moment(req.body.new_date).isAfter(moment())) {
        res.status(401).json({
            status: 401,
            message: 'DATE INVALID'
        }).end();
    }
    else {
        Project.changeLastUpdate(req.body.pid, req.body.new_date, CATEGORIES.INVESTASI, function(err, result) {
            if (err) next(err)
            else res.status(200).json({
                status: 200,
                message: 'Last update date changed'
            }).end();
        });
    }
});

/**
 * @api {get} /project/sosial Sosial - Request List of Project
 * @apiVersion 0.1.0
 * @apiName GetAllProjectSosial
 * @apiGroup Project
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam (QueryParam) {Number}  status  Filter by project status (1: In progress, 2: Completed, 3: Cancelled)
 * @apiParam (QueryParam) {Number}  phase   Filter by project current phase stage number (0: No filter)
 * @apiParam (QueryParam) {Number}  sort    Sort user (1: Sort by newest created, 2: Sort by oldest updated, 3: Sort by phase number, 4: Sort by name)
 * @apiParam (QueryParam) {Number}  participate Filter by user participation in project (0: No filter, 1: Filter on)
 *
 * @apiSuccess {Object[]} projects                          List of projects
 * @apiSuccess {Number}     projects.pid                    Project ID
 * @apiSuccess {String}     projects.name                   Name
 * @apiSuccess {String}     projects.pic                    Project's Person in Charge
 * @apiSuccess {Number}     projects.curr_phase_stage       Project's current phase stage number
 * @apiSuccess {Date}       projects.due_date               Project's current phase deadline
 * @apiSuccess {Date}       projects.updated_at             Project's last updated time        
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "pid": 1,
 *              "name": "Jabar Social Global Contribution",
 *              "pic": "johndoe",
 *              "curr_phase_stage": 1,
 *              "due_date": "2019-05-09 00:00:00",
 *              "updated_at": "2019-05-09 13:14:25"
 *          }
 *     ]
 */
app.get('/sosial', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL])], function(req, res, next) {
    Project.getAllProject(CATEGORIES.SOSIAL, req.query.status, req.query.phase, req.query.sort, req.query.participate, res.locals.auth.userId, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});


/**
 * @api {get} /project/sosial/:pid   Sosial - Request Project by Project ID
 * @apiVersion 0.1.0
 * @apiName GetProjectSosial
 * @apiGroup Project
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {Number}  pid  Project ID
 * 
 * @apiSuccess {String}     name                       Project Name  
 * @apiSuccess {Number}     curr_phase_stage           Current Phase Project   
 * @apiSuccess {String}     phase_deadline.stage_num   Due date of each stage num                
 * @apiSuccess {String}     status                     Current status of project (1 = ongoing, 2 = finished, 3 = cancelled)
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "name": "Rehab IF",
 *         "curr_phase_stage": 1,
 *         "phase_deadline": {
 *            "1": "2019-05-09 18:33:43"
 *         },
 *         "status": 1
 *     }
 */
app.get('/sosial/:pid', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL])], function(req, res, next) {
    Project.getProject(req.params.pid, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next(err)
        else if (result) res.status(200).json(result).end()
        else next({status: 404});
    });
});

/**
 * @api {get} /project/sosial/:pid/edit   Sosial - Request Complete Project Details
 * @apiVersion 0.1.0
 * @apiName GetProjectSosialEditDetails
 * @apiGroup Project
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {Number}  pid  Project ID
 * 
 * @apiSuccess {Number}     id                          Project ID
 * @apiSuccess {String}     name                        Project Name  
 * @apiSuccess {Number}     category                    Project Category
 * @apiSuccess {Number}     curr_phase_stage            Current Phase Project   
 * @apiSuccess {String}     company_name                Name of project partner
 * @apiSuccess {String}     company_address             Address of project partner
 * @apiSuccess {String}     cp_name                     Name of project partner's representative
 * @apiSuccess {String}     cp_phone                    Phone number of project partner
 * @apiSuccess {String}     cp_email                    Email of project partner
 * @apiSuccess {String}     description                 Brief description of the project
 * @apiSuccess {String}     created_at                  Project creation date
 * @apiSuccess {String}     updated_at                  Project last update's date
 * @apiSuccess {Number}     status                      Current project status
 * @apiSuccess {String}     country                     Project partner's country of origin
 * @apiSuccess {Object}     phase_deadline              Contains deadlines for each phase
 * @apiSuccess {String}     phase_deadline.1            Deadline for project phase 1
 * @apiSuccess {String}     phase_deadline.2            Deadline for project phase 2
 * @apiSuccess {String}     phase_deadline.3            Deadline for project phase 3
 * @apiSuccess {String}     phase_deadline.4            Deadline for project phase 4
 * @apiSuccess {String}     phase_deadline.5            Deadline for project phase 5
 * @apiSuccess {String}     phase_deadline.6            Deadline for project phase 6
 * @apiSuccess {String}     phase_deadline.7            Deadline for project phase 7
 * @apiSuccess {Number[]}   perangkat                   Array of indices of perangkat involved
 * 
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *           "id": 29,
 *           "name": "Test Project 4",
 *           "category": 1,
 *           "pic": "test",
 *           "curr_phase_stage": 1,
 *           "company_name": "Tirta",
 *           "company_address": "Bandung",
 *           "cp_name": "Jon",
 *           "cp_phone": "0812",
 *           "cp_email": "a@gmail.com",
 *           "description": "Testing Project",
 *           "created_at": "2019-05-15T13:52:24.000Z",
 *           "updated_at": "2019-05-15T13:52:24.000Z",
 *           "status": 1,
 *           "country": "Indonesia",
 *           "phase_deadline": {
 *               "1": "2020-03-01 00:00:00",
 *               "2": "2020-03-01 00:00:00",
 *               "3": "2020-03-01 00:00:00",
 *               "4": "2020-03-01 00:00:00",
 *               "5": "2020-03-01 00:00:00",
 *               "6": "2020-03-01 00:00:00",
 *               "7": "2020-03-01 00:00:00"
 *           },
 *           "perangkat": [
 *               1,
 *              10,
 *               13
 *           ],
 *           "list_perangkat": [
 *               "Biro Pemerintahan dan Kerjasama",
 *               "Biro Hukum dan Hak Asasi Manusia",
 *               "Biro Pelayanan dan Pengembangan Sosial",
 *               "Biro Badan Usaha Milik Daerah dan Investasi",
 *               "Biro Perekonomian",
 *               "Biro Pengadaan Barang/Jasa",
 *               "Biro Organisasi",
 *               "Biro Hubungan Masyarakat dan Protokol",
 *               "Biro Umum",
 *               "Dinas Pendidikan",
 *               "Dinas Kesehatan",
 *               "Dinas Bina Marga dan Penataan Ruang",
 *               "Dinas Sumber Daya Air",
 *               "Dinas Perumahan dan Pemukiman",
 *               "Dinas Sosial",
 *               "Dinas Pemberdayaan Perempuan, Perlindungan Anak, dan Keluarga Berencana",
 *               "Dinas Lingkungan Hidup",
 *               "Dinas Pemberdayaan Masyarakat dan Desa",
 *               "Dinas Perhubungan",
 *               "Dinas Komunikasi dan Informatika",
 *               "Dinas Koperasi dan Usaha Kecil",
 *               "Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu",
 *               "Dinas Pemuda dan Olahraga",
 *               "Dinas Perpustakaan dan Kearsipan Daerah",
 *               "Dinas Tenaga Kerja dan Transmigrasi",
 *               "Dinas Ketahanan Pangan dan Peternakan",
 *               "Dinas Pariwisata dan Kebudayaan",
 *               "Dinas Kelautan dan Perikanan",
 *               "Dinas Tanaman Pangan dan Holtikultura",
 *               "Dinas Perkebunan",
 *               "Dinas Kehutanan",
 *               "Dinas Energi dan Sumber Daya Mineral",
 *               "Dinas Perindustrian dan Perdagangan",
 *               "Dinas Kependudukan dan Pencatatan Sipil",
 *               "Badan Perencanaan Pembangunan Daerah",
 *               "Badan Kepegawaian Daerah",
 *               "Badan Pengembangan Sumber Daya Manusia",
 *               "Badan Penelitian dan Pengembangan Daerah",
 *               "Badan Pengelolaan Keuangan dan Aset Daerah",
 *               "Badan Pendapatan Daerah",
 *               "Badan Penanggulangan Bencana Daerah"
 *           ]
 *       }
 */
app.get('/sosial/:pid/edit', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL])], function(req, res, next) {
    Project.getProjectDetails(req.params.pid, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next(err)
        else if (result) res.status(200).json(result).end()
        else next({status: 404});
    });
});

/**
 * @api {get} /project/sosial/:pid/participants   SOsial - Request Participants in a Project
 * @apiVersion 0.1.0
 * @apiName GetProjectParticipantsSosial
 * @apiGroup Project
 * 
 * @apiPermission sosial
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {Number}  pid  Project ID
 * 
 * @apiSuccess {Object[]}   participants               List of participants in a project
 * @apiSuccess {Number}     participants.uid           Current Phase Project   
 * @apiSuccess {String}     participants.name          Due date of each stage num                
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *      {
 *          "name": "test",
 *           "uid": 1
 *      },
 *       {
 *         "name": "nico",
 *           "uid": 2
 *      }
 *     ]
]
 */
app.get('/sosial/:pid/participants', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL])], function(req, res, next) {
    Project.getProjectParticipants(req.params.pid, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end()
    });
});

/**
 * @api {patch} /project/sosial/status Sosial - Update Project Status
 * @apiVersion 0.1.0
 * @apiName UpdateProjectStatusSosial
 * @apiGroup Project
 * 
 * @apiPermission Sosial, pic
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   new_status  New Project Status
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Project status updated"
 *     }
 */
app.patch('/sosial/status', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isAuthorizedToEditProject], function(req, res, next) {
    Project.changeProjectStatus(req.body.pid, req.body.new_status, CATEGORIES.SOSIAL, function (err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Project status updated'
        }).end();
    })
})


/**
 * @api {post} /project/sosial   Sosial - Create New Project
 * @apiVersion 0.1.0
 * @apiName AddProjectSosial
 * @apiGroup Project
 * 
 * @apiPermission sosial, manager
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {String}   name               Project Name
 * @apiParam  {Number}   pic_name           Project Manager username (current logged in user)
 * @apiParam  {String}   cp_name            Name of Contact Person
 * @apiParam  {String}   company_name       Name of partner company
 * @apiParam  {String}   company_address    Company's address
 * @apiParam  {String}   cp_phone           Contact person's phone
 * @apiParam  {String}   email              Contact person's email
 * @apiParam  {String}   Description        Project description
 * @apiParam  {String}   country            Project partner's country of origin
 * @apiParam  {String}   due_date1          Due date of phase 1
 * @apiParam  {String}   due_date2          Due date of phase 2
 * @apiParam  {String}   due_date3          Due date of phase 3
 * @apiParam  {String}   due_date4          Due date of phase 4
 * @apiParam  {String}   due_date5          Due date of phase 5
 * @apiParam  {String}   due_date6          Due date of phase 6
 * @apiParam  {String}   due_date7          Due date of phase 7
 * @apiParam  {Number[]} perangkat          List of perangkat by index
 *               
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 200,
 *         "message": "Project added",
 *     }
 */
app.post('/sosial', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.SOSIAL])], function(req, res, next) {
    Project.addProject(req.body.name, CATEGORIES.SOSIAL, req.body.pic_name, req.body.cp_name, req.body.company_name, req.body.company_address, req.body.cp_phone, req.body.cp_email, req.body.description,
        req.body.country,
        req.body.due_date1,
        req.body.due_date2,
        req.body.due_date3,
        req.body.due_date4,
        req.body.due_date5,
        req.body.due_date6,
        req.body.due_date7,
        req.body.perangkat, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Project added'
        }).end();
    });
});

/**
 * @api {patch} /project/sosial/edit   Sosial - Edit Project
 * @apiVersion 0.1.0
 * @apiName EditProjectSosial
 * @apiGroup Project
 * 
 * @apiPermission sosial, pic
 * 
 * @apiUse HeaderTemplate
 *
 * @apiparam  {Number}   pid                Project ID
 * @apiParam  {String}   name               Project Name
 * @apiParam  {Number}   pic_name           Project Manager username (current logged in user)
 * @apiParam  {String}   cp_name            Name of Contact Person
 * @apiParam  {String}   company_name       Name of partner company
 * @apiParam  {String}   company_address    Company's address
 * @apiParam  {String}   cp_phone           Contact person's phone
 * @apiParam  {String}   email              Contact person's email
 * @apiParam  {String}   Description        Project description
 * @apiParam  {String}   country            Project partner's country of origin
 * @apiParam  {String}   due_date1          Due date of phase 1
 * @apiParam  {String}   due_date2          Due date of phase 2
 * @apiParam  {String}   due_date3          Due date of phase 3
 * @apiParam  {String}   due_date4          Due date of phase 4
 * @apiParam  {String}   due_date5          Due date of phase 5
 * @apiParam  {String}   due_date6          Due date of phase 6
 * @apiParam  {String}   due_date7          Due date of phase 7
 * @apiParam  {Number[]} perangkat          List of perangkat by index
 * @apiParam  {String}   latest_update      Timestamp of latest update in format YYYY-MM-DD
 *               
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 200,
 *         "message": "Project edited",
 *     }
 */
app.patch('/sosial/edit', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isAuthorizedToEditProject], function(req, res, next) {
    if (req.body.latest_update && moment(req.body.latest_update).isAfter(moment())) {
        res.status(401).json({
            status: 401,
            message: 'DATE INVALID'
        }).end();
    }
    else {
        Project.editProject(req.body.pid, req.body.name, CATEGORIES.SOSIAL, req.body.pic_name, req.body.cp_name, req.body.company_name, req.body.company_address, req.body.cp_phone, req.body.cp_email, req.body.description,
            req.body.country,
            req.body.due_date1,
            req.body.due_date2,
            req.body.due_date3,
            req.body.due_date4,
            req.body.due_date5,
            req.body.due_date6,
            req.body.due_date7, 
            req.body.perangkat,
            req.body.latest_update ,function(err, result) {
            if (err) next(err)
            else res.status(200).json({
                status: 200,
                message: 'Project edited'
            }).end();
        });
    }
});

/**
 * @api {delete} /project/sosial Sosial - Delete Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectSosial
 * @apiGroup Project
 * 
 * @apiPermission sosial, manager
 *
 * @apiUse HeaderTemplate
 * 
 * @apiParam    (BodyParam) {Number}    pid         Project ID   
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Project deleted"
 *     }
 * 
 * @apiUse ErrorTemplate
 */
app.delete('/sosial', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.SOSIAL])], function(req, res, next) {
    Project.deleteProject(req.body.pid, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Project deleted'
        }).end();
    });
});

/**
 * @api {post} /project/sosial/participant Sosial - Add participant
 * @apiVersion 0.1.0
 * @apiName AddParticipantSosial
 * @apiGroup Project
 * 
 * @apiPermission sosial, manager
 * 
 * @apiUse HeaderTemplate
 *
 * 
 * @apiParam {Number}     pid                        Project ID
 * @apiParam {String[]}   participants               List of add user to participate
 * 
 *        
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": 200,
 *        "message": "Participant added"
 *     }
 */
app.post('/sosial/participant', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.SOSIAL])], function(req, res, next) {
    Project.addParticipant(req.body.pid, req.body.participants, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Participant Added'
        }).end();
    });
});

/**
 * @api {delete} /project/sosial/participant Sosial - Delete Project Participant
 * @apiVersion 0.1.0
 * @apiName DeleteProjectParticipantSosial
 * @apiGroup Project
 * 
 * @apiPermission sosial, pic
 *
 * @apiUse HeaderTemplate
 * 
 * @apiParam    (BodyParam) {Number}    pid         Project ID   
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Participant deleted"
 *     }
 * 
 * @apiUse ErrorTemplate
 */
app.delete('/sosial/participant', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isAuthorizedToEditProject], function(req, res, next) {
    Project.deleteProjectParticipant(req.body.pid, req.body.uid, CATEGORIES.SOSIAL, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Participant Deleted'
        }).end();
    });
});

/**
 * @api {patch} /project/sosial/phase Sosial - Change project phase
 * @apiVersion 0.1.0
 * @apiName UpdateProjectPhaseSosial
 * @apiGroup Project
 * 
 * @apiPermission sosial, pic
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   new_phase  New Project Phase
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Project phase updated"
 *     }
 */
app.patch('/sosial/phase', [isAuthenticated, isAuthorized([CATEGORIES.SOSIAL]), isAuthorizedToEditProject], function(req, res, next) {
    Project.changeProjectPhase(req.body.pid, req.body.new_phase, CATEGORIES.SOSIAL, function (err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Project phase updated'
        }).end();
    })
});

/**
 * @api {patch} /project/sosial/last_update   Sosial - Change Last Update Date
 * @apiVersion 0.1.0
 * @apiName EditProjectUpdateDateSosial
 * @apiGroup Project
 * 
 * @apiPermission sosial, manager
 * 
 * @apiUse HeaderTemplate
 *
 * @apiparam  {Number}   pid                Project ID
 * @apiParam  {String}   new_date           New date in string format : YYYY-MM-DD
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 200,
 *         "message": "Project Last update date changed",
 *     }
 */
app.patch('/sosial/last_update', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.SOSIAL])], function(req, res, next) {
    if (req.body.new_date && moment(req.body.new_date).isAfter(moment())) {
        res.status(401).json({
            status: 401,
            message: 'DATE INVALID'
        }).end();
    }
    else {
        Project.changeLastUpdate(req.body.pid, req.body.new_date, CATEGORIES.SOSIAL, function(err, result) {
            if (err) next(err)
            else res.status(200).json({
                status: 200,
                message: 'Last update date changed'
            }).end();
        });
    }
});

/**
 * @api {get} /project/dagang Dagang - Request List of Project
 * @apiVersion 0.1.0
 * @apiName GetAllProjectDagang
 * @apiGroup Project
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam (QueryParam) {Number}  status  Filter by project status (1: In progress, 2: Completed, 3: Cancelled)
 * @apiParam (QueryParam) {Number}  phase   Filter by project current phase stage number (0: No filter)
 * @apiParam (QueryParam) {Number}  sort    Sort user (1: Sort by newest created, 2: Sort by oldest updated, 3: Sort by phase number, 4: Sort by name)
 * @apiParam (QueryParam) {Number}  participate Filter by user participation in project (0: No filter, 1: Filter on)
 *
 * @apiSuccess {Object[]} projects                          List of projects
 * @apiSuccess {Number}     projects.pid                    Project ID
 * @apiSuccess {String}     projects.name                   Name
 * @apiSuccess {String}     projects.pic                    Project's Person in Charge
 * @apiSuccess {Number}     projects.curr_phase_stage       Project's current phase stage number
 * @apiSuccess {Date}       projects.due_date               Project's current phase deadline
 * @apiSuccess {Date}       projects.updated_at             Project's last updated time        
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "pid": 1,
 *              "name": "Marketplace Reimagined",
 *              "pic": "johndoe",
 *              "curr_phase_stage": 1,
 *              "due_date": "2019-05-09 00:00:00",
 *              "updated_at": "2019-05-09 13:14:25"
 *          }
 *     ]
 */
app.get('/dagang', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG])], function(req, res, next) {
    Project.getAllProject(CATEGORIES.DAGANG, req.query.status, req.query.phase, req.query.sort, req.query.participate, res.locals.auth.userId, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});


/**
 * @api {get} /project/dagang/:pid   Dagang - Request Project by Project ID
 * @apiVersion 0.1.0
 * @apiName GetProjectDagang
 * @apiGroup Project
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {Number}  pid  Project ID
 * 
 * @apiSuccess {String}     name                       Project Name  
 * @apiSuccess {Number}     curr_phase_stage           Current Phase Project   
 * @apiSuccess {String}     phase_deadline.stage_num   Due date of each stage num
 * @apiSuccess {String}     status                     Current status of project (1 = ongoing, 2 = finished, 3 = cancelled)
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "name": "Rehab IF",
 *         "curr_phase_stage": 1,
 *         "phase_deadline": {
 *            "1": "2019-05-09 18:33:43"
 *         },
 *         "status": 1
 *     }
 */
app.get('/dagang/:pid', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG])], function(req, res, next) {
    Project.getProject(req.params.pid, CATEGORIES.DAGANG, function(err, result) {
        if (err) next(err)
        else if (result) res.status(200).json(result).end()
        else next({status: 404});
    });
});

/**
 * @api {get} /project/dagang/:pid/edit   Dagang - Request Complete Project Details
 * @apiVersion 0.1.0
 * @apiName GetProjectDagangEditDetails
 * @apiGroup Project
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {Number}  pid  Project ID
 * 
 * @apiSuccess {Number}     id                          Project ID
 * @apiSuccess {String}     name                        Project Name  
 * @apiSuccess {Number}     category                    Project Category
 * @apiSuccess {Number}     curr_phase_stage            Current Phase Project   
 * @apiSuccess {String}     company_name                Name of project partner
 * @apiSuccess {String}     company_address             Address of project partner
 * @apiSuccess {String}     cp_name                     Name of project partner's representative
 * @apiSuccess {String}     cp_phone                    Phone number of project partner
 * @apiSuccess {String}     cp_email                    Email of project partner
 * @apiSuccess {String}     description                 Brief description of the project
 * @apiSuccess {String}     created_at                  Project creation date
 * @apiSuccess {String}     updated_at                  Project last update's date
 * @apiSuccess {Number}     status                      Current project status
 * @apiSuccess {String}     country                     Project partner's country of origin
 * @apiSuccess {Object}     phase_deadline              Contains deadlines for each phase
 * @apiSuccess {String}     phase_deadline.1            Deadline for project phase 1
 * @apiSuccess {String}     phase_deadline.2            Deadline for project phase 2
 * @apiSuccess {String}     phase_deadline.3            Deadline for project phase 3
 * @apiSuccess {String}     phase_deadline.4            Deadline for project phase 4
 * @apiSuccess {String}     phase_deadline.5            Deadline for project phase 5
 * @apiSuccess {String}     phase_deadline.6            Deadline for project phase 6
 * @apiSuccess {String}     phase_deadline.7            Deadline for project phase 7
 * @apiSuccess {Number[]}   perangkat                   Array of indices of perangkat involved
 * @apiSuccess {String[]}   list_perangkat              Array of perangkat name ordered by id in DB
 * 
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *           "id": 29,
 *           "name": "Test Project 4",
 *           "category": 1,
 *           "pic": "test",
 *           "curr_phase_stage": 1,
 *           "company_name": "Tirta",
 *           "company_address": "Bandung",
 *           "cp_name": "Jon",
 *           "cp_phone": "0812",
 *           "cp_email": "a@gmail.com",
 *           "description": "Testing Project",
 *           "created_at": "2019-05-15T13:52:24.000Z",
 *           "updated_at": "2019-05-15T13:52:24.000Z",
 *           "status": 1,
 *           "country": "Indonesia",
 *           "phase_deadline": {
 *               "1": "2020-03-01 00:00:00",
 *               "2": "2020-03-01 00:00:00",
 *               "3": "2020-03-01 00:00:00",
 *               "4": "2020-03-01 00:00:00",
 *               "5": "2020-03-01 00:00:00",
 *               "6": "2020-03-01 00:00:00",
 *               "7": "2020-03-01 00:00:00"
 *           },
 *           "perangkat": [
 *               1,
 *              10,
 *               13
 *           ],
 *           "list_perangkat": [
 *               "Biro Pemerintahan dan Kerjasama",
 *               "Biro Hukum dan Hak Asasi Manusia",
 *               "Biro Pelayanan dan Pengembangan Sosial",
 *               "Biro Badan Usaha Milik Daerah dan Investasi",
 *               "Biro Perekonomian",
 *               "Biro Pengadaan Barang/Jasa",
 *               "Biro Organisasi",
 *               "Biro Hubungan Masyarakat dan Protokol",
 *               "Biro Umum",
 *               "Dinas Pendidikan",
 *               "Dinas Kesehatan",
 *               "Dinas Bina Marga dan Penataan Ruang",
 *               "Dinas Sumber Daya Air",
 *               "Dinas Perumahan dan Pemukiman",
 *               "Dinas Sosial",
 *               "Dinas Pemberdayaan Perempuan, Perlindungan Anak, dan Keluarga Berencana",
 *               "Dinas Lingkungan Hidup",
 *               "Dinas Pemberdayaan Masyarakat dan Desa",
 *               "Dinas Perhubungan",
 *               "Dinas Komunikasi dan Informatika",
 *               "Dinas Koperasi dan Usaha Kecil",
 *               "Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu",
 *               "Dinas Pemuda dan Olahraga",
 *               "Dinas Perpustakaan dan Kearsipan Daerah",
 *               "Dinas Tenaga Kerja dan Transmigrasi",
 *               "Dinas Ketahanan Pangan dan Peternakan",
 *               "Dinas Pariwisata dan Kebudayaan",
 *               "Dinas Kelautan dan Perikanan",
 *               "Dinas Tanaman Pangan dan Holtikultura",
 *               "Dinas Perkebunan",
 *               "Dinas Kehutanan",
 *               "Dinas Energi dan Sumber Daya Mineral",
 *               "Dinas Perindustrian dan Perdagangan",
 *               "Dinas Kependudukan dan Pencatatan Sipil",
 *               "Badan Perencanaan Pembangunan Daerah",
 *               "Badan Kepegawaian Daerah",
 *               "Badan Pengembangan Sumber Daya Manusia",
 *               "Badan Penelitian dan Pengembangan Daerah",
 *               "Badan Pengelolaan Keuangan dan Aset Daerah",
 *               "Badan Pendapatan Daerah",
 *               "Badan Penanggulangan Bencana Daerah"
 *           ]
 *       }
 */
app.get('/dagang/:pid/edit', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG])], function(req, res, next) {
    Project.getProjectDetails(req.params.pid, CATEGORIES.DAGANG, function(err, result) {
        if (err) next(err)
        else if (result) res.status(200).json(result).end()
        else next({status: 404});
    });
});

/**
 * @api {get} /project/dagang/:pid/participants   Dagang - Request Participants in a Project
 * @apiVersion 0.1.0
 * @apiName GetProjectParticipantsDagang
 * @apiGroup Project
 * 
 * @apiPermission dagang
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {Number}  pid  Project ID
 * 
 * @apiSuccess {Object[]}   participants               List of participants in a project
 * @apiSuccess {Number}     participants.uid           Current Phase Project   
 * @apiSuccess {String}     participants.name          Due date of each stage num                
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *      {
 *          "name": "test",
 *           "uid": 1
 *      },
 *       {
 *         "name": "nico",
 *           "uid": 2
 *      }
 *     ]
]
 */
app.get('/dagang/:pid/participants', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG])], function(req, res, next) {
    Project.getProjectParticipants(req.params.pid, CATEGORIES.DAGANG, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end()
    });
});

/**
 * @api {patch} /project/dagang/status Dagang - Update Project Status
 * @apiVersion 0.1.0
 * @apiName UpdateProjectStatusDagang
 * @apiGroup Project
 * 
 * @apiPermission Dagang, pic
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   new_status  New Project Status
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Project status updated"
 *     }
 */
app.patch('/dagang/status', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG]), isAuthorizedToEditProject], function(req, res, next) {
    Project.changeProjectStatus(req.body.pid, req.body.new_status, CATEGORIES.DAGANG, function (err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Project status updated'
        }).end();
    })
})

/**
 * @api {post} /project/dagang   Dagang - Create New Project
 * @apiVersion 0.1.0
 * @apiName AddProjectDagang
 * @apiGroup Project
 * 
 * @apiPermission dagang, manager
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {String}   name               Project Name
 * @apiParam  {Number}   pic_name           Project Manager username (current logged in user)
 * @apiParam  {String}   cp_name            Name of Contact Person
 * @apiParam  {String}   company_name       Name of partner company
 * @apiParam  {String}   company_address    Company's address
 * @apiParam  {String}   cp_phone           Contact person's phone
 * @apiParam  {String}   email              Contact person's email
 * @apiParam  {String}   Description        Project description
 * @apiParam  {String}   country            Project partner's country of origin
 * @apiParam  {String}   due_date1          Due date of phase 1
 * @apiParam  {String}   due_date2          Due date of phase 2
 * @apiParam  {String}   due_date3          Due date of phase 3
 * @apiParam  {String}   due_date4          Due date of phase 4
 * @apiParam  {String}   due_date5          Due date of phase 5
 * @apiParam  {String}   due_date6          Due date of phase 6
 * @apiParam  {String}   due_date7          Due date of phase 7
 * @apiParam  {Number[]} perangkat          List of perangkat by index
 *               
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 200,
 *         "message": "Project added",
 *     }
 */
app.post('/dagang', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.DAGANG])], function(req, res, next) {
    Project.addProject(req.body.name, CATEGORIES.DAGANG, req.body.pic_name, req.body.cp_name, req.body.company_name, req.body.company_address, req.body.cp_phone, req.body.cp_email, req.body.description,
        req.body.country,
        req.body.due_date1,
        req.body.due_date2,
        req.body.due_date3,
        req.body.due_date4,
        req.body.due_date5,
        req.body.due_date6,
        req.body.due_date7,
        req.body.perangkat, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Project added'
        }).end();
    });
});

/**
 * @api {patch} /project/dagang/edit   dagang - Edit Project
 * @apiVersion 0.1.0
 * @apiName EditProjectDagang
 * @apiGroup Project
 * 
 * @apiPermission dagang, pic
 * 
 * @apiUse HeaderTemplate
 *
 * @apiparam  {Number}   pid                Project ID
 * @apiParam  {String}   name               Project Name
 * @apiParam  {Number}   pic_name           Project Manager username (current logged in user)
 * @apiParam  {String}   cp_name            Name of Contact Person
 * @apiParam  {String}   company_name       Name of partner company
 * @apiParam  {String}   company_address    Company's address
 * @apiParam  {String}   cp_phone           Contact person's phone
 * @apiParam  {String}   email              Contact person's email
 * @apiParam  {String}   Description        Project description
 * @apiParam  {String}   country            Project partner's country of origin
 * @apiParam  {String}   due_date1          Due date of phase 1
 * @apiParam  {String}   due_date2          Due date of phase 2
 * @apiParam  {String}   due_date3          Due date of phase 3
 * @apiParam  {String}   due_date4          Due date of phase 4
 * @apiParam  {String}   due_date5          Due date of phase 5
 * @apiParam  {String}   due_date6          Due date of phase 6
 * @apiParam  {String}   due_date7          Due date of phase 7
 * @apiParam  {Number[]} perangkat          List of perangkat by index
 * @apiParam  {String}   latest_update      Timestamp of latest update in format YYYY-MM-DD
 *               
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 200,
 *         "message": "Project edited",
 *     }
 */
app.patch('/dagang/edit', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG]), isAuthorizedToEditProject], function(req, res, next) {
    if (req.body.latest_update && moment(req.body.latest_update).isAfter(moment())) {
        res.status(401).json({
            status: 401,
            message: 'DATE INVALID'
        }).end();
    }
    else {
        Project.editProject(req.body.pid, req.body.name, CATEGORIES.DAGANG, req.body.pic_name, req.body.cp_name, req.body.company_name, req.body.company_address, req.body.cp_phone, req.body.cp_email, req.body.description,
            req.body.country,
            req.body.due_date1,
            req.body.due_date2,
            req.body.due_date3,
            req.body.due_date4,
            req.body.due_date5,
            req.body.due_date6,
            req.body.due_date7, 
            req.body.perangkat,
            req.body.latest_update, function(err, result) {
            if (err) next(err)
            else res.status(200).json({
                status: 200,
                message: 'Project edited'
            }).end();
        });
    }
});

/**
 * @api {delete} /project/dagang Dagang - Delete Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectDagang
 * @apiGroup Project
 * 
 * @apiPermission dagang, manager
 *
 * @apiUse HeaderTemplate
 * 
 * @apiParam    (BodyParam) {Number}    pid         Project ID   
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Project deleted"
 *     }
 * 
 * @apiUse ErrorTemplate
 */
app.delete('/dagang', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.DAGANG])], function(req, res, next) {
    Project.deleteProject(req.body.pid, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Project deleted'
        }).end();
    });
});

/**
 * @api {post} /project/dagang/participant Dagang - Add participant
 * @apiVersion 0.1.0
 * @apiName AddParticipantDagang
 * @apiGroup Project
 * 
 * @apiPermission dagang, manager
 * 
 * @apiUse HeaderTemplate
 *
 * 
 * @apiParam {Number}     pid                        Project ID
 * @apiParam {String[]}   participants               List of add user to participate
 * 
 *        
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": 200,
 *        "message": "Participant added"
 *     }
 */
app.post('/dagang/participant', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.DAGANG])], function(req, res, next) {
    Project.addParticipant(req.body.pid, req.body.participants, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Participant Added'
        }).end();
    });
});

/**
 * @api {delete} /project/dagang/participant Dagang - Delete Project Participant
 * @apiVersion 0.1.0
 * @apiName DeleteProjectParticipantDagang
 * @apiGroup Project
 * 
 * @apiPermission dagang, pic
 *
 * @apiUse HeaderTemplate
 * 
 * @apiParam    (BodyParam) {Number}    pid         Project ID   
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Participant deleted"
 *     }
 * 
 * @apiUse ErrorTemplate
 */
app.delete('/dagang/participant', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG]), isAuthorizedToEditProject], function(req, res, next) {
    Project.deleteProjectParticipant(req.body.pid, req.body.uid, CATEGORIES.DAGANG, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Participant Deleted'
        }).end();
    });
});

/**
 * @api {patch} /project/dagang/phase Dagang - Change project phase
 * @apiVersion 0.1.0
 * @apiName UpdateProjectPhaseDagang
 * @apiGroup Project
 * 
 * @apiPermission dagang, pic
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   new_phase  New Project Phase
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Project phase updated"
 *     }
 */
app.patch('/dagang/phase', [isAuthenticated, isAuthorized([CATEGORIES.DAGANG]), isAuthorizedToEditProject], function(req, res, next) {
    Project.changeProjectPhase(req.body.pid, req.body.new_phase, CATEGORIES.DAGANG, function (err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Project phase updated'
        }).end();
    })
});

/**
 * @api {patch} /project/dagang/last_update   Dagang - Change Last Update Date
 * @apiVersion 0.1.0
 * @apiName EditProjectUpdateDateDagang
 * @apiGroup Project
 * 
 * @apiPermission dagang, manager
 * 
 * @apiUse HeaderTemplate
 *
 * @apiparam  {Number}   pid                Project ID
 * @apiParam  {String}   new_date           New date in string format : YYYY-MM-DD
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 200,
 *         "message": "Project Last update date changed",
 *     }
 */
app.patch('/dagang/last_update', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.DAGANG])], function(req, res, next) {
    if (req.body.new_date && moment(req.body.new_date).isAfter(moment())) {
        res.status(401).json({
            status: 401,
            message: 'DATE INVALID'
        }).end();
    }
    else {
        Project.changeLastUpdate(req.body.pid, req.body.new_date, CATEGORIES.DAGANG, function(err, result) {
            if (err) next(err)
            else res.status(200).json({
                status: 200,
                message: 'Last update date changed'
            }).end();
        });
    }
});

/**
 * @api {get} /project/wisata Wisata - Request List of Project
 * @apiVersion 0.1.0
 * @apiName GetAllProjectWisata
 * @apiGroup Project
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam (QueryParam) {Number}  status  Filter by project status (1: In progress, 2: Completed, 3: Cancelled)
 * @apiParam (QueryParam) {Number}  phase   Filter by project current phase stage number (0: No filter)
 * @apiParam (QueryParam) {Number}  sort    Sort user (1: Sort by newest created, 2: Sort by oldest updated, 3: Sort by phase number, 4: Sort by name)
 * @apiParam (QueryParam) {Number}  participate Filter by user participation in project (0: No filter, 1: Filter on)
 *
 * @apiSuccess {Object[]} projects                          List of projects
 * @apiSuccess {Number}     projects.pid                    Project ID
 * @apiSuccess {String}     projects.name                   Name
 * @apiSuccess {String}     projects.pic                    Project's Person in Charge
 * @apiSuccess {Number}     projects.curr_phase_stage       Project's current phase stage number
 * @apiSuccess {Date}       projects.due_date               Project's current phase deadline
 * @apiSuccess {Date}       projects.updated_at             Project's last updated time        
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "pid": 1,
 *              "name": "Improvement of Tangkuban Parahu",
 *              "pic": "johndoe",
 *              "curr_phase_stage": 1,
 *              "due_date": "2019-05-09 00:00:00",
 *              "updated_at": "2019-05-09 13:14:25"
 *          }
 *     ]
 */
app.get('/wisata', [isAuthenticated, isAuthorized([CATEGORIES.WISATA])], function(req, res, next) {
    Project.getAllProject(CATEGORIES.WISATA, req.query.status, req.query.phase, req.query.sort, req.query.participate, res.locals.auth.userId, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});


/**
 * @api {get} /project/wisata/:pid   Wisata - Request Project by Project ID
 * @apiVersion 0.1.0
 * @apiName GetProjectWisata
 * @apiGroup Project
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {Number}  pid  Project ID
 * 
 * @apiSuccess {String}     name                       Project Name  
 * @apiSuccess {Number}     curr_phase_stage           Current Phase Project   
 * @apiSuccess {String}     phase_deadline.stage_num   Due date of each stage num                
 * @apiSuccess {String}     status                     Current status of project (1 = ongoing, 2 = finished, 3 = cancelled)
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "name": "Rehab IF",
 *         "curr_phase_stage": 1,
 *         "phase_deadline": {
 *            "1": "2019-05-09 18:33:43"
 *         },
 *         "status": 1
 *     }
 */
app.get('/wisata/:pid', [isAuthenticated, isAuthorized([CATEGORIES.WISATA])], function(req, res, next) {
    Project.getProject(req.params.pid, CATEGORIES.WISATA, function(err, result) {
        if (err) next(err)
        else if (result) res.status(200).json(result).end()
        else next({status: 404});
    });
});

/**
 * @api {get} /project/wisata/:pid/edit   Wisata - Request Complete Project Details
 * @apiVersion 0.1.0
 * @apiName GetProjectWisataEditDetails
 * @apiGroup Project
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {Number}  pid  Project ID
 * 
 * @apiSuccess {Number}     id                          Project ID
 * @apiSuccess {String}     name                        Project Name  
 * @apiSuccess {Number}     category                    Project Category
 * @apiSuccess {Number}     curr_phase_stage            Current Phase Project   
 * @apiSuccess {String}     company_name                Name of project partner
 * @apiSuccess {String}     company_address             Address of project partner
 * @apiSuccess {String}     cp_name                     Name of project partner's representative
 * @apiSuccess {String}     cp_phone                    Phone number of project partner
 * @apiSuccess {String}     cp_email                    Email of project partner
 * @apiSuccess {String}     description                 Brief description of the project
 * @apiSuccess {String}     created_at                  Project creation date
 * @apiSuccess {String}     updated_at                  Project last update's date
 * @apiSuccess {Number}     status                      Current project status
 * @apiSuccess {String}     country                     Project partner's country of origin
 * @apiSuccess {Object}     phase_deadline              Contains deadlines for each phase
 * @apiSuccess {String}     phase_deadline.1            Deadline for project phase 1
 * @apiSuccess {String}     phase_deadline.2            Deadline for project phase 2
 * @apiSuccess {String}     phase_deadline.3            Deadline for project phase 3
 * @apiSuccess {String}     phase_deadline.4            Deadline for project phase 4
 * @apiSuccess {String}     phase_deadline.5            Deadline for project phase 5
 * @apiSuccess {String}     phase_deadline.6            Deadline for project phase 6
 * @apiSuccess {String}     phase_deadline.7            Deadline for project phase 7
 * @apiSuccess {Number[]}   perangkat                   Array of indices of perangkat involved
 * 
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *           "id": 29,
 *           "name": "Test Project 4",
 *           "category": 1,
 *           "pic": "test",
 *           "curr_phase_stage": 1,
 *           "company_name": "Tirta",
 *           "company_address": "Bandung",
 *           "cp_name": "Jon",
 *           "cp_phone": "0812",
 *           "cp_email": "a@gmail.com",
 *           "description": "Testing Project",
 *           "created_at": "2019-05-15T13:52:24.000Z",
 *           "updated_at": "2019-05-15T13:52:24.000Z",
 *           "status": 1,
 *           "country": "Indonesia",
 *           "phase_deadline": {
 *               "1": "2020-03-01 00:00:00",
 *               "2": "2020-03-01 00:00:00",
 *               "3": "2020-03-01 00:00:00",
 *               "4": "2020-03-01 00:00:00",
 *               "5": "2020-03-01 00:00:00",
 *               "6": "2020-03-01 00:00:00",
 *               "7": "2020-03-01 00:00:00"
 *           },
 *           "perangkat": [
 *               1,
 *              10,
 *               13
 *           ],
 *           "list_perangkat": [
 *               "Biro Pemerintahan dan Kerjasama",
 *               "Biro Hukum dan Hak Asasi Manusia",
 *               "Biro Pelayanan dan Pengembangan Sosial",
 *               "Biro Badan Usaha Milik Daerah dan Investasi",
 *               "Biro Perekonomian",
 *               "Biro Pengadaan Barang/Jasa",
 *               "Biro Organisasi",
 *               "Biro Hubungan Masyarakat dan Protokol",
 *               "Biro Umum",
 *               "Dinas Pendidikan",
 *               "Dinas Kesehatan",
 *               "Dinas Bina Marga dan Penataan Ruang",
 *               "Dinas Sumber Daya Air",
 *               "Dinas Perumahan dan Pemukiman",
 *               "Dinas Sosial",
 *               "Dinas Pemberdayaan Perempuan, Perlindungan Anak, dan Keluarga Berencana",
 *               "Dinas Lingkungan Hidup",
 *               "Dinas Pemberdayaan Masyarakat dan Desa",
 *               "Dinas Perhubungan",
 *               "Dinas Komunikasi dan Informatika",
 *               "Dinas Koperasi dan Usaha Kecil",
 *               "Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu",
 *               "Dinas Pemuda dan Olahraga",
 *               "Dinas Perpustakaan dan Kearsipan Daerah",
 *               "Dinas Tenaga Kerja dan Transmigrasi",
 *               "Dinas Ketahanan Pangan dan Peternakan",
 *               "Dinas Pariwisata dan Kebudayaan",
 *               "Dinas Kelautan dan Perikanan",
 *               "Dinas Tanaman Pangan dan Holtikultura",
 *               "Dinas Perkebunan",
 *               "Dinas Kehutanan",
 *               "Dinas Energi dan Sumber Daya Mineral",
 *               "Dinas Perindustrian dan Perdagangan",
 *               "Dinas Kependudukan dan Pencatatan Sipil",
 *               "Badan Perencanaan Pembangunan Daerah",
 *               "Badan Kepegawaian Daerah",
 *               "Badan Pengembangan Sumber Daya Manusia",
 *               "Badan Penelitian dan Pengembangan Daerah",
 *               "Badan Pengelolaan Keuangan dan Aset Daerah",
 *               "Badan Pendapatan Daerah",
 *               "Badan Penanggulangan Bencana Daerah"
 *           ]
 *       }
 */
app.get('/wisata/:pid/edit', [isAuthenticated, isAuthorized([CATEGORIES.WISATA])], function(req, res, next) {
    Project.getProjectDetails(req.params.pid, CATEGORIES.WISATA, function(err, result) {
        if (err) next(err)
        else if (result) res.status(200).json(result).end()
        else next({status: 404});
    });
});

/**
 * @api {get} /project/wisata/:pid/participants   Wisata - Request Participants in a Project
 * @apiVersion 0.1.0
 * @apiName GetProjectParticipantsWisata
 * @apiGroup Project
 * 
 * @apiPermission wisata
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {Number}  pid  Project ID
 * 
 * @apiSuccess {Object[]}   participants               List of participants in a project
 * @apiSuccess {Number}     participants.uid           Current Phase Project   
 * @apiSuccess {String}     participants.name          Due date of each stage num                
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *      {
 *          "name": "test",
 *           "uid": 1
 *      },
 *       {
 *         "name": "nico",
 *           "uid": 2
 *      }
 *     ]
]
 */
app.get('/wisata/:pid/participants', [isAuthenticated, isAuthorized([CATEGORIES.WISATA])], function(req, res, next) {
    Project.getProjectParticipants(req.params.pid, CATEGORIES.WISATA, function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end()
    });
});

/**
 * @api {post} /project/wisata   Wisata - Create New Project
 * @apiVersion 0.1.0
 * @apiName AddProjectWisata
 * @apiGroup Project
 * 
 * @apiPermission wisata manager
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam  {String}   name               Project Name
 * @apiParam  {Number}   pic_name           Project Manager username (current logged in user)
 * @apiParam  {String}   cp_name            Name of Contact Person
 * @apiParam  {String}   company_name       Name of partner company
 * @apiParam  {String}   company_address    Company's address
 * @apiParam  {String}   cp_phone           Contact person's phone
 * @apiParam  {String}   email              Contact person's email
 * @apiParam  {String}   Description        Project description
 * @apiParam  {String}   country            Project partner's country of origin
 * @apiParam  {String}   due_date1          Due date of phase 1
 * @apiParam  {String}   due_date2          Due date of phase 2
 * @apiParam  {String}   due_date3          Due date of phase 3
 * @apiParam  {String}   due_date4          Due date of phase 4
 * @apiParam  {String}   due_date5          Due date of phase 5
 * @apiParam  {String}   due_date6          Due date of phase 6
 * @apiParam  {String}   due_date7          Due date of phase 7
 * @apiParam  {Number[]} perangkat          List of perangkat by index
 *               
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 200,
 *         "message": "Project added",
 *     }
 */
app.post('/wisata', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.WISATA])], function(req, res, next) {
    Project.addProject(req.body.name, CATEGORIES.WISATA, req.body.pic_name, req.body.cp_name, req.body.company_name, req.body.company_address, req.body.cp_phone, req.body.cp_email, req.body.description,
        req.body.country,
        req.body.due_date1,
        req.body.due_date2,
        req.body.due_date3,
        req.body.due_date4,
        req.body.due_date5,
        req.body.due_date6,
        req.body.due_date7,
        req.body.perangkat, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Project added'
        }).end();
    });
});

/**
 * @api {patch} /project/wisata/edit   Wisata - Edit Project
 * @apiVersion 0.1.0
 * @apiName EditProjectWisata
 * @apiGroup Project
 * 
 * @apiPermission wisata, pic
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiparam  {Number}   pid                Project ID
 * @apiParam  {String}   name               Project Name
 * @apiParam  {Number}   pic_name           Project Manager username (current logged in user)
 * @apiParam  {String}   cp_name            Name of Contact Person
 * @apiParam  {String}   company_name       Name of partner company
 * @apiParam  {String}   company_address    Company's address
 * @apiParam  {String}   cp_phone           Contact person's phone
 * @apiParam  {String}   email              Contact person's email
 * @apiParam  {String}   Description        Project description
 * @apiParam  {String}   country            Project partner's country of origin
 * @apiParam  {String}   due_date1          Due date of phase 1
 * @apiParam  {String}   due_date2          Due date of phase 2
 * @apiParam  {String}   due_date3          Due date of phase 3
 * @apiParam  {String}   due_date4          Due date of phase 4
 * @apiParam  {String}   due_date5          Due date of phase 5
 * @apiParam  {String}   due_date6          Due date of phase 6
 * @apiParam  {String}   due_date7          Due date of phase 7
 * @apiParam  {Number[]} perangkat          List of perangkat by index
 * @apiParam  {String}   latest_update      Timestamp of latest update in format YYYY-MM-DD
 *               
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 200,
 *         "message": "Project edited",
 *     }
 */
app.patch('/wisata/edit', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isAuthorizedToEditProject], function(req, res, next) {
    if (req.body.latest_update && moment(req.body.latest_update).isAfter(moment())) {
        res.status(401).json({
            status: 401,
            message: 'DATE INVALID'
        }).end();
    }
    else {
        Project.editProject(req.body.pid, req.body.name, CATEGORIES.WISATA, req.body.pic_name, req.body.cp_name, req.body.company_name, req.body.company_address, req.body.cp_phone, req.body.cp_email, req.body.description,
            req.body.country,
            req.body.due_date1,
            req.body.due_date2,
            req.body.due_date3,
            req.body.due_date4,
            req.body.due_date5,
            req.body.due_date6,
            req.body.due_date7, 
            req.body.perangkat,
            req.body.latest_update, function(err, result) {
            if (err) next(err)
            else res.status(200).json({
                status: 200,
                message: 'Project edited'
            }).end();
        });
    }
});

/**
 * @api {delete} /project/wisata Wisata - Delete Project
 * @apiVersion 0.1.0
 * @apiName DeleteProjectWisata
 * @apiGroup Project
 * 
 * @apiPermission wisata, manager
 *
 * @apiUse HeaderTemplate
 * 
 * @apiParam    (BodyParam) {Number}    pid         Project ID   
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Project deleted"
 *     }
 * 
 * @apiUse ErrorTemplate
 */
app.delete('/wisata', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.WISATA])], function(req, res, next) {
    Project.deleteProject(req.body.pid, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Project deleted'
        }).end();
    });
});

/**
 * @api {patch} /project/wisata status Wisata - Update Project Status
 * @apiVersion 0.1.0
 * @apiName UpdateProjectStatusWisata
 * @apiGroup Project
 * 
 * @apiPermission Wisata, pic
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   new_status  New Project Status
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Project status updated"
 *     }
 */
app.patch('/wisata/status', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isAuthorizedToEditProject], function(req, res, next) {
    Project.changeProjectStatus(req.body.pid, req.body.new_status, CATEGORIES.WISATA, function (err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Project status updated'
        }).end();
    })
})

/**
 * @api {post} /project/wisata/participant Wisata - Add participant
 * @apiVersion 0.1.0
 * @apiName AddParticipantWisata
 * @apiGroup Project
 * 
 * @apiPermission wisata, manager
 * 
 * @apiUse HeaderTemplate
 *
 * 
 * @apiParam {Number}     pid                        Project ID
 * @apiParam {String[]}   participants               List of add user to participate
 * 
 *        
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        "status": 200,
 *        "message": "Participant added"
 *     }
 */
app.post('/wisata/participant', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.WISATA])], function(req, res, next) {
    Project.addParticipant(req.body.pid, req.body.participants, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Participant Added'
        }).end();
    });
});

/**
 * @api {delete} /project/wisata/participant Wisata - Delete Project Participant
 * @apiVersion 0.1.0
 * @apiName DeleteProjectParticipantWisata
 * @apiGroup Project
 * 
 * @apiPermission wisata, pic
 *
 * @apiUse HeaderTemplate
 * 
 * @apiParam    (BodyParam) {Number}    pid         Project ID   
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Participant deleted"
 *     }
 * 
 * @apiUse ErrorTemplate
 */
app.delete('/wisata/participant', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isAuthorizedToEditProject], function(req, res, next) {
    Project.deleteProjectParticipant(req.body.pid, req.body.uid, CATEGORIES.WISATA, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Participant Deleted'
        }).end();
    });
});

/**
 * @api {patch} /project/wisata/phase Wisata - Change project phase
 * @apiVersion 0.1.0
 * @apiName UpdateProjectPhaseWisata
 * @apiGroup Project
 * 
 * @apiPermission wisata, pic
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam {Number}   pid     Project ID
 * @apiParam {Number}   new_phase  New Project Phase
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "Project phase updated"
 *     }
 */
app.patch('/wisata/phase', [isAuthenticated, isAuthorized([CATEGORIES.WISATA]), isAuthorizedToEditProject], function(req, res, next) {
    Project.changeProjectPhase(req.body.pid, req.body.new_phase, CATEGORIES.WISATA, function (err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'Project phase updated'
        }).end();
    })
});

/**
 * @api {patch} /project/wisata/last_update   Wisata - Change Last Update Date
 * @apiVersion 0.1.0
 * @apiName EditProjectUpdateDateWisata
 * @apiGroup Project
 * 
 * @apiPermission wisata, manager
 * 
 * @apiUse HeaderTemplate
 *
 * @apiparam  {Number}   pid                Project ID
 * @apiParam  {String}   new_date           New date in string format : YYYY-MM-DD
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "status": 200,
 *         "message": "Project Last update date changed",
 *     }
 */
app.patch('/wisata/last_update', [isAuthenticated, isAuthorized([CATEGORIES.MANAGER, CATEGORIES.WISATA])], function(req, res, next) {
    if (req.body.new_date && moment(req.body.new_date).isAfter(moment())) {
        res.status(401).json({
            status: 401,
            message: 'DATE INVALID'
        }).end();
    }
    else {
        Project.changeLastUpdate(req.body.pid, req.body.new_date, CATEGORIES.WISATA, function(err, result) {
            if (err) next(err)
            else res.status(200).json({
                status: 200,
                message: 'Last update date changed'
            }).end();
        });
    }
});

//Other Project components (using same /project path)
app.use(projectComment);
app.use(projectDocument);
app.use(projectPhoto);
app.use(projectTodo);

module.exports = app;

