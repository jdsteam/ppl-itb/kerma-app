const { Router: router } = require('express');
const { User } = require('../models');
const { isAuthenticated, isAuthorized } = require('../middleware');
const { CATEGORIES } = require('../enum');

const app = router();

//GET User
/**
 * @api {get} /user/search Request User Id Based on Username
 * @apiVersion 0.1.0
 * @apiName GetUserIdByUsername
 * @apiGroup User
 * 
 * @apiPermission Login
 * 
 * @apiUse HeaderTemplate
 * 
 * @apiParam   (QueryParam)  {String}  username     Username of the searched user
 *
 * @apiSuccess {Number}   id       User ID     
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *        {
 *           "id": 2
 *        }
 *     ]
 */

app.get('/search', isAuthenticated, function(req, res, next) {
    User.getUserIdByUsername(req.query.username, function(err, results){
        if (err) next(err)
        else res.status(200).json(results).end();
    });
});

/**
 * @api {get} /user Request All User Information
 * @apiVersion 0.1.0
 * @apiName GetAllUser
 * @apiGroup User
 * 
 * @apiPermission admin
 * 
 * @apiUse HeaderTemplate
 *
 * @apiSuccess {Object[]} users       List of users
 * @apiSuccess {Number}     users.uid          User ID
 * @apiSuccess {String}     users.name        Name
 * @apiSuccess {String}     users.username    Username
 * @apiSuccess {String}     users.email       Email
 * @apiSuccess {Number[]}   users.roles       User Roles in enumeration representation        
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *          {
 *              "id": "5",
 *              "name": "John Doe",
 *              "username": "johndoe",
 *              "email": "johndoe@email.com",
 *              "roles": [
 *                  1,
 *                  2
 *              ]
 *          }
 *     ]
 */
app.get('/', [isAuthenticated, isAuthorized([CATEGORIES.ADMIN])], function(req, res, next) {
    console.log('masuk sini');
    User.getAllUser(function(err, result) {
        if (err) next(err)
        else res.status(200).json(result).end();
    });
});

/**
 * @api {get} /user/:uid Request Specific User Information
 * @apiVersion 0.1.0
 * @apiName GetUser
 * @apiGroup User
 * 
 * @apiPermission admin
 * 
 * @apiUse HeaderTemplate
 *
 * @apiParam {Number}   uid     User ID
 * 
 * @apiSuccess {String}     users.name        Name
 * @apiSuccess {String}     users.username    Username
 * @apiSuccess {String}     users.email       Email
 * @apiSuccess {Number[]}   users.roles       User Roles in ernumeration representation        
 * 
 * @apiUse ErrorTemplate
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          "name": "John Doe",
 *          "username": "johndoe",
 *          "email": "johndoe@email.com",
 *          "roles": [
 *              1,
 *              2
 *          ]
 *     }
 */
app.get('/:uid', [isAuthenticated, isAuthorized([CATEGORIES.ADMIN], true)], function(req, res, next) {
    User.getUser(req.params.uid, function(err, result) {
        if (err) next(err)
        else if (result) res.status(200).json(result).end()
        else next({status: 404});
    });
});

//POST User
/**
 * @api {post} /user Add New User
 * @apiVersion 0.1.0
 * @apiName AddUser
 * @apiGroup User
 * 
 * @apiPermission admin
 *
 * @apiUse HeaderTemplate
 * 
 * @apiParam    (BodyParam)  {String}    name        Name
 * @apiParam    (BodyParam)  {String}    username    Username
 * @apiParam    (BodyParam)  {String}    email       Email
 * @apiParam    (BodyParam)  {String}    password    Password
 * @apiParam    (BodyParam)  {Number[]}  roles       User Roles in ernumeration representation        
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "User added"
 *     }
 * 
 * @apiUse ErrorTemplate
 */
app.post('/', [isAuthenticated, isAuthorized([CATEGORIES.ADMIN])], function(req, res, next) {
    User.addUser(req.body.name, req.body.username, req.body.email, req.body.password, req.body.roles, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'User added'
        }).end();
    });
});

//PATCH User
/**
 * @api {patch} /user Edit User
 * @apiVersion 0.1.0
 * @apiName EditUser
 * @apiGroup User
 * 
 * @apiPermission admin
 *
 * @apiUse HeaderTemplate
 * 
 * @apiParam    (BodyParam)    {Number}    uid         User ID
 * @apiParam    (BodyParam)    {String}    name        Name
 * @apiParam    (BodyParam)    {String}    username    Username
 * @apiParam    (BodyParam)    {String}    email       Email
 * @apiParam    (BodyParam)    {String}    password    Password
 * @apiParam    (BodyParam)    {Number[]}  roles       User Roles in ernumeration representation        
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "User edited"
 *     }
 * 
 * @apiUse ErrorTemplate
 */
app.patch('/', [isAuthenticated, isAuthorized([CATEGORIES.ADMIN])], function(req, res, next) {
    let password = (req.body.password) ? req.body.password : ""
    User.editUser(req.body.uid, req.body.name, req.body.username, req.body.email, password, req.body.roles, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'User edited'
        }).end();
    });
});

//DELETE User
/**
 * @api {delete} /user Delete User
 * @apiVersion 0.1.0
 * @apiName DeleteUser
 * @apiGroup User
 * 
 * @apiPermission admin
 *
 * @apiUse HeaderTemplate
 * 
 * @apiParam    (BodyParam) {Number}    uid         User ID   
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *          status: "200",
 *          message: "User deleted"
 *     }
 * 
 * @apiUse ErrorTemplate
 */
app.delete('/', [isAuthenticated, isAuthorized([CATEGORIES.ADMIN])], function(req, res, next) {
    User.deleteUser(req.body.uid, function(err, result) {
        if (err) next(err)
        else res.status(200).json({
            status: 200,
            message: 'User deleted'
        }).end();
    });
});


module.exports = app;

