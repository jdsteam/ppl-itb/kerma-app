const express = require('express');
const bodyParser = require('body-parser');

const config = require('./config');
const api = require('./src/routes');
const { notFoundHandler } = require('./src/middleware');
const { MysqlManager } = require('./src/mysql');

const { DailyScheduler, MonthlyScheduler } = require('./src/service');

const app = express();

//connect db
const mysql = new MysqlManager(config.mysql);
MysqlManager.connect();

app.get('/', function(req, res, next) {
  res.send("Hello World");
});

// Parse URL-encoded bodies (as sent by HTML forms)
app.use(bodyParser.urlencoded({ extended: true }));

// Parse JSON bodies (as sent by API clients)
app.use(bodyParser.json());

app.use(function (req, res, next) {
  res.set('Access-Control-Allow-Origin', '*');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token");
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST, PATCH'); 
  next();
});

// api routes v1
app.use('/api/v1', api(config));

//Not found
app.use('/', notFoundHandler);

module.exports = app;