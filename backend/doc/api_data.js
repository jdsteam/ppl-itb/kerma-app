define({ "api": [
  {
    "type": "get",
    "url": "dashboard/dagang/monthly/phases",
    "title": "Dagang - Get Ongoing Project Count Per Phase",
    "version": "0.1.0",
    "name": "GetDagangOngoingProjectPerPhase",
    "group": "Dashboard",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Numbers[]",
            "optional": false,
            "field": "project_count",
            "description": "<p>List of project count per stage number</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"project_count\": [1,1,0,3,5,4,6]\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/dashboard.js",
    "groupTitle": "Dashboard",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "dashboard/dagang/monthly/status",
    "title": "Dagang - Get Project Count Per Status For Current Month",
    "version": "0.1.0",
    "name": "GetDagangProjectCountStatusCurrentMonth",
    "group": "Dashboard",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Numbers[]",
            "optional": false,
            "field": "project_count",
            "description": "<p>List of project count per status for current month ([Finished, New Ongoing, Old Ongoing, Cancelled])</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"project_count\": [1,1,0,3]\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/dashboard.js",
    "groupTitle": "Dashboard",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "dashboard/investasi/monthly/phases",
    "title": "Investasi - Get Ongoing Project Count Per Phase",
    "version": "0.1.0",
    "name": "GetInvestasiOngoingProjectPerPhase",
    "group": "Dashboard",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Numbers[]",
            "optional": false,
            "field": "project_count",
            "description": "<p>List of project count per stage number</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"project_count\": [1,1,0,3,5,4,6]\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/dashboard.js",
    "groupTitle": "Dashboard",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "dashboard/investasi/monthly/status",
    "title": "Investasi - Get Project Count Per Status For Current Month",
    "version": "0.1.0",
    "name": "GetInvestasiProjectCountStatusCurrentMonth",
    "group": "Dashboard",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Numbers[]",
            "optional": false,
            "field": "project_count",
            "description": "<p>List of project count per status for current month ([Finished, New Ongoing, Old Ongoing, Cancelled])</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"project_count\": [1,1,0,3]\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/dashboard.js",
    "groupTitle": "Dashboard",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "dashboard/observer",
    "title": "Observer - Get Project Count For Ongoing and New Projects",
    "version": "0.1.0",
    "name": "GetObserverProjectCount",
    "group": "Dashboard",
    "permission": [
      {
        "name": "None"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "statistics",
            "description": "<p>List of projects in each category</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "statistics.ongoing",
            "description": "<p>Number of ongoing projects</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "statistics.new",
            "description": "<p>Number of new projects (ongoing projects created after the start of this month)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n {\n     \"ongoing\": 0,\n     \"new\": 1\n }",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/dashboard.js",
    "groupTitle": "Dashboard",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "dashboard/sosial/monthly/phases",
    "title": "Sosial - Get Ongoing Project Count Per Phase",
    "version": "0.1.0",
    "name": "GetSosialOngoingProjectPerPhase",
    "group": "Dashboard",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Numbers[]",
            "optional": false,
            "field": "project_count",
            "description": "<p>List of project count per stage number</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"project_count\": [1,1,0,3,5,4,6]\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/dashboard.js",
    "groupTitle": "Dashboard",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "dashboard/sosial/monthly/status",
    "title": "Sosial - Get Project Count Per Status For Current Month",
    "version": "0.1.0",
    "name": "GetSosialProjectCountStatusCurrentMonth",
    "group": "Dashboard",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Numbers[]",
            "optional": false,
            "field": "project_count",
            "description": "<p>List of project count per status for current month ([Finished, New Ongoing, Old Ongoing, Cancelled])</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"project_count\": [1,1,0,3]\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/dashboard.js",
    "groupTitle": "Dashboard",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "dashboard/wisata/monthly/phases",
    "title": "Wisata - Get Ongoing Project Count Per Phase",
    "version": "0.1.0",
    "name": "GetWisataOngoingProjectPerPhase",
    "group": "Dashboard",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Numbers[]",
            "optional": false,
            "field": "project_count",
            "description": "<p>List of project count per stage number</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"project_count\": [1,1,0,3,5,4,6]\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/dashboard.js",
    "groupTitle": "Dashboard",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "dashboard/wisata/monthly/status",
    "title": "Wisata - Get Project Count Per Status For Current Month",
    "version": "0.1.0",
    "name": "GetWisataProjectCountStatusCurrentMonth",
    "group": "Dashboard",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Numbers[]",
            "optional": false,
            "field": "project_count",
            "description": "<p>List of project count per status for current month ([Finished, New Ongoing, Old Ongoing, Cancelled])</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"project_count\": [1,1,0,3]\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/dashboard.js",
    "groupTitle": "Dashboard",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/login",
    "title": "Login",
    "version": "0.1.0",
    "name": "Login",
    "group": "Login",
    "permission": [
      {
        "name": "none"
      }
    ],
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "BodyParam",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username</p>"
          },
          {
            "group": "BodyParam",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "uid",
            "description": "<p>User ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Generated token for further authorization</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     \"uid\": \"5\",\n     \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywidXNlcm5hbWUiOiJyYW1hMSIsImlhdCI6MTU1NjcwNTU3OSwiZXhwIjoxNTU3MzEwMzc5fQ.0YB-1dTQHNDmDWq0P3HqLmRmoVRcI9iq3nkCqbzJi8Q\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>False login attempt</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/login.js",
    "groupTitle": "Login"
  },
  {
    "type": "post",
    "url": "/logout",
    "title": "Logout",
    "version": "0.1.0",
    "name": "Logout",
    "group": "Logout",
    "permission": [
      {
        "name": "none"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Logout successful\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 INTERNAL SERVER ERROR\n{\n    code: 500,\n    message: \"INTERNAL SERVER ERROR\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/logout.js",
    "groupTitle": "Logout",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    }
  },
  {
    "type": "post",
    "url": "/project/dagang/participant",
    "title": "Dagang - Add participant",
    "version": "0.1.0",
    "name": "AddParticipantDagang",
    "group": "Project",
    "permission": [
      {
        "name": "dagang, manager"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "participants",
            "description": "<p>List of add user to participate</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": 200,\n   \"message\": \"Participant added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/investasi/participant",
    "title": "Investasi - Add participant",
    "version": "0.1.0",
    "name": "AddParticipantInvestasi",
    "group": "Project",
    "permission": [
      {
        "name": "investasi, manager"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "participants",
            "description": "<p>List of add user to participate</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": 200,\n   \"message\": \"Participant added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/sosial/participant",
    "title": "Sosial - Add participant",
    "version": "0.1.0",
    "name": "AddParticipantSosial",
    "group": "Project",
    "permission": [
      {
        "name": "sosial, manager"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "participants",
            "description": "<p>List of add user to participate</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": 200,\n   \"message\": \"Participant added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/wisata/participant",
    "title": "Wisata - Add participant",
    "version": "0.1.0",
    "name": "AddParticipantWisata",
    "group": "Project",
    "permission": [
      {
        "name": "wisata, manager"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "participants",
            "description": "<p>List of add user to participate</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"status\": 200,\n   \"message\": \"Participant added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/dagang",
    "title": "Dagang - Create New Project",
    "version": "0.1.0",
    "name": "AddProjectDagang",
    "group": "Project",
    "permission": [
      {
        "name": "dagang, manager"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pic_name",
            "description": "<p>Project Manager username (current logged in user)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_name",
            "description": "<p>Name of Contact Person</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": "<p>Name of partner company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_address",
            "description": "<p>Company's address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_phone",
            "description": "<p>Contact person's phone</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Contact person's email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Description",
            "description": "<p>Project description</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Project partner's country of origin</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date1",
            "description": "<p>Due date of phase 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date2",
            "description": "<p>Due date of phase 2</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date3",
            "description": "<p>Due date of phase 3</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date4",
            "description": "<p>Due date of phase 4</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date5",
            "description": "<p>Due date of phase 5</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date6",
            "description": "<p>Due date of phase 6</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date7",
            "description": "<p>Due date of phase 7</p>"
          },
          {
            "group": "Parameter",
            "type": "Number[]",
            "optional": false,
            "field": "perangkat",
            "description": "<p>List of perangkat by index</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": 200,\n    \"message\": \"Project added\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/investasi",
    "title": "Investasi - Create New Project",
    "version": "0.1.0",
    "name": "AddProjectInvestasi",
    "group": "Project",
    "permission": [
      {
        "name": "investasi, manager"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pic_name",
            "description": "<p>Project Manager username (current logged in user)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_name",
            "description": "<p>Name of Contact Person</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": "<p>Name of partner company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_address",
            "description": "<p>Company's address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_phone",
            "description": "<p>Contact person's phone</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Contact person's email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Description",
            "description": "<p>Project description</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Project partner's country of origin</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date1",
            "description": "<p>Due date of phase 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date2",
            "description": "<p>Due date of phase 2</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date3",
            "description": "<p>Due date of phase 3</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date4",
            "description": "<p>Due date of phase 4</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date5",
            "description": "<p>Due date of phase 5</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date6",
            "description": "<p>Due date of phase 6</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date7",
            "description": "<p>Due date of phase 7</p>"
          },
          {
            "group": "Parameter",
            "type": "Number[]",
            "optional": false,
            "field": "perangkat",
            "description": "<p>List of perangkat by index</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": 200,\n    \"message\": \"Project added\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/sosial",
    "title": "Sosial - Create New Project",
    "version": "0.1.0",
    "name": "AddProjectSosial",
    "group": "Project",
    "permission": [
      {
        "name": "sosial, manager"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pic_name",
            "description": "<p>Project Manager username (current logged in user)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_name",
            "description": "<p>Name of Contact Person</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": "<p>Name of partner company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_address",
            "description": "<p>Company's address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_phone",
            "description": "<p>Contact person's phone</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Contact person's email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Description",
            "description": "<p>Project description</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Project partner's country of origin</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date1",
            "description": "<p>Due date of phase 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date2",
            "description": "<p>Due date of phase 2</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date3",
            "description": "<p>Due date of phase 3</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date4",
            "description": "<p>Due date of phase 4</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date5",
            "description": "<p>Due date of phase 5</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date6",
            "description": "<p>Due date of phase 6</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date7",
            "description": "<p>Due date of phase 7</p>"
          },
          {
            "group": "Parameter",
            "type": "Number[]",
            "optional": false,
            "field": "perangkat",
            "description": "<p>List of perangkat by index</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": 200,\n    \"message\": \"Project added\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/wisata",
    "title": "Wisata - Create New Project",
    "version": "0.1.0",
    "name": "AddProjectWisata",
    "group": "Project",
    "permission": [
      {
        "name": "wisata manager"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pic_name",
            "description": "<p>Project Manager username (current logged in user)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_name",
            "description": "<p>Name of Contact Person</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": "<p>Name of partner company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_address",
            "description": "<p>Company's address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_phone",
            "description": "<p>Contact person's phone</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Contact person's email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Description",
            "description": "<p>Project description</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Project partner's country of origin</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date1",
            "description": "<p>Due date of phase 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date2",
            "description": "<p>Due date of phase 2</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date3",
            "description": "<p>Due date of phase 3</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date4",
            "description": "<p>Due date of phase 4</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date5",
            "description": "<p>Due date of phase 5</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date6",
            "description": "<p>Due date of phase 6</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date7",
            "description": "<p>Due date of phase 7</p>"
          },
          {
            "group": "Parameter",
            "type": "Number[]",
            "optional": false,
            "field": "perangkat",
            "description": "<p>List of perangkat by index</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": 200,\n    \"message\": \"Project added\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/dagang",
    "title": "Dagang - Delete Project",
    "version": "0.1.0",
    "name": "DeleteProjectDagang",
    "group": "Project",
    "permission": [
      {
        "name": "dagang, manager"
      }
    ],
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "BodyParam",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Project deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/investasi",
    "title": "Investasi - Delete Project",
    "version": "0.1.0",
    "name": "DeleteProjectInvestasi",
    "group": "Project",
    "permission": [
      {
        "name": "investasi, manager"
      }
    ],
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "BodyParam",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Project deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/dagang/participant",
    "title": "Dagang - Delete Project Participant",
    "version": "0.1.0",
    "name": "DeleteProjectParticipantDagang",
    "group": "Project",
    "permission": [
      {
        "name": "dagang, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "BodyParam",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Participant deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/investasi/participant",
    "title": "Investasi - Delete Project Participant",
    "version": "0.1.0",
    "name": "DeleteProjectParticipantInvestasi",
    "group": "Project",
    "permission": [
      {
        "name": "investasi, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "BodyParam",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Participant deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/sosial/participant",
    "title": "Sosial - Delete Project Participant",
    "version": "0.1.0",
    "name": "DeleteProjectParticipantSosial",
    "group": "Project",
    "permission": [
      {
        "name": "sosial, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "BodyParam",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Participant deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/wisata/participant",
    "title": "Wisata - Delete Project Participant",
    "version": "0.1.0",
    "name": "DeleteProjectParticipantWisata",
    "group": "Project",
    "permission": [
      {
        "name": "wisata, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "BodyParam",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Participant deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/sosial",
    "title": "Sosial - Delete Project",
    "version": "0.1.0",
    "name": "DeleteProjectSosial",
    "group": "Project",
    "permission": [
      {
        "name": "sosial, manager"
      }
    ],
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "BodyParam",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Project deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/wisata",
    "title": "Wisata - Delete Project",
    "version": "0.1.0",
    "name": "DeleteProjectWisata",
    "group": "Project",
    "permission": [
      {
        "name": "wisata, manager"
      }
    ],
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "BodyParam",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Project deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/dagang/edit",
    "title": "dagang - Edit Project",
    "version": "0.1.0",
    "name": "EditProjectDagang",
    "group": "Project",
    "permission": [
      {
        "name": "dagang, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pic_name",
            "description": "<p>Project Manager username (current logged in user)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_name",
            "description": "<p>Name of Contact Person</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": "<p>Name of partner company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_address",
            "description": "<p>Company's address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_phone",
            "description": "<p>Contact person's phone</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Contact person's email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Description",
            "description": "<p>Project description</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Project partner's country of origin</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date1",
            "description": "<p>Due date of phase 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date2",
            "description": "<p>Due date of phase 2</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date3",
            "description": "<p>Due date of phase 3</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date4",
            "description": "<p>Due date of phase 4</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date5",
            "description": "<p>Due date of phase 5</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date6",
            "description": "<p>Due date of phase 6</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date7",
            "description": "<p>Due date of phase 7</p>"
          },
          {
            "group": "Parameter",
            "type": "Number[]",
            "optional": false,
            "field": "perangkat",
            "description": "<p>List of perangkat by index</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "latest_update",
            "description": "<p>Timestamp of latest update in format YYYY-MM-DD</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": 200,\n    \"message\": \"Project edited\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/investasi/edit",
    "title": "Investasi - Edit Project",
    "version": "0.1.0",
    "name": "EditProjectInvestasi",
    "group": "Project",
    "permission": [
      {
        "name": "investasi, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pic_name",
            "description": "<p>Project Manager username (current logged in user)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_name",
            "description": "<p>Name of Contact Person</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": "<p>Name of partner company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_address",
            "description": "<p>Company's address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_phone",
            "description": "<p>Contact person's phone</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Contact person's email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Description",
            "description": "<p>Project description</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Project partner's country of origin</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date1",
            "description": "<p>Due date of phase 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date2",
            "description": "<p>Due date of phase 2</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date3",
            "description": "<p>Due date of phase 3</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date4",
            "description": "<p>Due date of phase 4</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date5",
            "description": "<p>Due date of phase 5</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date6",
            "description": "<p>Due date of phase 6</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date7",
            "description": "<p>Due date of phase 7</p>"
          },
          {
            "group": "Parameter",
            "type": "Number[]",
            "optional": false,
            "field": "perangkat",
            "description": "<p>List of perangkat by index</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "latest_update",
            "description": "<p>Timestamp of latest update in format YYYY-MM-DD</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": 200,\n    \"message\": \"Project edited\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/sosial/edit",
    "title": "Sosial - Edit Project",
    "version": "0.1.0",
    "name": "EditProjectSosial",
    "group": "Project",
    "permission": [
      {
        "name": "sosial, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pic_name",
            "description": "<p>Project Manager username (current logged in user)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_name",
            "description": "<p>Name of Contact Person</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": "<p>Name of partner company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_address",
            "description": "<p>Company's address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_phone",
            "description": "<p>Contact person's phone</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Contact person's email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Description",
            "description": "<p>Project description</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Project partner's country of origin</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date1",
            "description": "<p>Due date of phase 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date2",
            "description": "<p>Due date of phase 2</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date3",
            "description": "<p>Due date of phase 3</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date4",
            "description": "<p>Due date of phase 4</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date5",
            "description": "<p>Due date of phase 5</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date6",
            "description": "<p>Due date of phase 6</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date7",
            "description": "<p>Due date of phase 7</p>"
          },
          {
            "group": "Parameter",
            "type": "Number[]",
            "optional": false,
            "field": "perangkat",
            "description": "<p>List of perangkat by index</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "latest_update",
            "description": "<p>Timestamp of latest update in format YYYY-MM-DD</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": 200,\n    \"message\": \"Project edited\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/dagang/last_update",
    "title": "Dagang - Change Last Update Date",
    "version": "0.1.0",
    "name": "EditProjectUpdateDateDagang",
    "group": "Project",
    "permission": [
      {
        "name": "dagang, manager"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "new_date",
            "description": "<p>New date in string format : YYYY-MM-DD</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": 200,\n    \"message\": \"Project Last update date changed\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/investasi/last_update",
    "title": "Investasi - Change Last Update Date",
    "version": "0.1.0",
    "name": "EditProjectUpdateDateInvestasi",
    "group": "Project",
    "permission": [
      {
        "name": "investasi, manager"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "new_date",
            "description": "<p>New date in string format : YYYY-MM-DD</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": 200,\n    \"message\": \"Project Last update date changed\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/sosial/last_update",
    "title": "Sosial - Change Last Update Date",
    "version": "0.1.0",
    "name": "EditProjectUpdateDateSosial",
    "group": "Project",
    "permission": [
      {
        "name": "sosial, manager"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "new_date",
            "description": "<p>New date in string format : YYYY-MM-DD</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": 200,\n    \"message\": \"Project Last update date changed\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/wisata/last_update",
    "title": "Wisata - Change Last Update Date",
    "version": "0.1.0",
    "name": "EditProjectUpdateDateWisata",
    "group": "Project",
    "permission": [
      {
        "name": "wisata, manager"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "new_date",
            "description": "<p>New date in string format : YYYY-MM-DD</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": 200,\n    \"message\": \"Project Last update date changed\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/wisata/edit",
    "title": "Wisata - Edit Project",
    "version": "0.1.0",
    "name": "EditProjectWisata",
    "group": "Project",
    "permission": [
      {
        "name": "wisata, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pic_name",
            "description": "<p>Project Manager username (current logged in user)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_name",
            "description": "<p>Name of Contact Person</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": "<p>Name of partner company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "company_address",
            "description": "<p>Company's address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cp_phone",
            "description": "<p>Contact person's phone</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Contact person's email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Description",
            "description": "<p>Project description</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Project partner's country of origin</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date1",
            "description": "<p>Due date of phase 1</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date2",
            "description": "<p>Due date of phase 2</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date3",
            "description": "<p>Due date of phase 3</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date4",
            "description": "<p>Due date of phase 4</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date5",
            "description": "<p>Due date of phase 5</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date6",
            "description": "<p>Due date of phase 6</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "due_date7",
            "description": "<p>Due date of phase 7</p>"
          },
          {
            "group": "Parameter",
            "type": "Number[]",
            "optional": false,
            "field": "perangkat",
            "description": "<p>List of perangkat by index</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "latest_update",
            "description": "<p>Timestamp of latest update in format YYYY-MM-DD</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"status\": 200,\n    \"message\": \"Project edited\",\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/dagang",
    "title": "Dagang - Request List of Project",
    "version": "0.1.0",
    "name": "GetAllProjectDagang",
    "group": "Project",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Query": [
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Filter by project status (1: In progress, 2: Completed, 3: Cancelled)</p>"
          },
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Filter by project current phase stage number (0: No filter)</p>"
          },
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>Sort user (1: Sort by newest created, 2: Sort by oldest updated, 3: Sort by phase number, 4: Sort by name)</p>"
          },
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "participate",
            "description": "<p>Filter by user participation in project (0: No filter, 1: Filter on)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "projects",
            "description": "<p>List of projects</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "projects.pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "projects.name",
            "description": "<p>Name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "projects.pic",
            "description": "<p>Project's Person in Charge</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "projects.curr_phase_stage",
            "description": "<p>Project's current phase stage number</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "projects.due_date",
            "description": "<p>Project's current phase deadline</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "projects.updated_at",
            "description": "<p>Project's last updated time</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"pid\": 1,\n         \"name\": \"Marketplace Reimagined\",\n         \"pic\": \"johndoe\",\n         \"curr_phase_stage\": 1,\n         \"due_date\": \"2019-05-09 00:00:00\",\n         \"updated_at\": \"2019-05-09 13:14:25\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/investasi",
    "title": "Investasi - Request List of Project",
    "version": "0.1.0",
    "name": "GetAllProjectInvestasi",
    "group": "Project",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Query": [
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Filter by project status (1: In progress, 2: Completed, 3: Cancelled)</p>"
          },
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Filter by project current phase stage number (0: No filter)</p>"
          },
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>Sort user (1: Sort by newest created, 2: Sort by oldest updated, 3: Sort by phase number, 4: Sort by name)</p>"
          },
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "participate",
            "description": "<p>Filter by user participation in project (0: No filter, 1: Filter on)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "projects",
            "description": "<p>List of projects</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "projects.pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "projects.name",
            "description": "<p>Name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "projects.pic",
            "description": "<p>Project's Person in Charge</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "projects.curr_phase_stage",
            "description": "<p>Project's current phase stage number</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "projects.due_date",
            "description": "<p>Project's current phase deadline</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "projects.updated_at",
            "description": "<p>Project's last updated time</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"pid\": 1,\n         \"name\": \"Jabar Invest Proposition\",\n         \"pic\": \"johndoe\",\n         \"curr_phase_stage\": 1,\n         \"due_date\": \"2019-05-09 00:00:00\",\n         \"updated_at\": \"2019-05-09 13:14:25\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/sosial",
    "title": "Sosial - Request List of Project",
    "version": "0.1.0",
    "name": "GetAllProjectSosial",
    "group": "Project",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Query": [
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Filter by project status (1: In progress, 2: Completed, 3: Cancelled)</p>"
          },
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Filter by project current phase stage number (0: No filter)</p>"
          },
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>Sort user (1: Sort by newest created, 2: Sort by oldest updated, 3: Sort by phase number, 4: Sort by name)</p>"
          },
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "participate",
            "description": "<p>Filter by user participation in project (0: No filter, 1: Filter on)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "projects",
            "description": "<p>List of projects</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "projects.pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "projects.name",
            "description": "<p>Name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "projects.pic",
            "description": "<p>Project's Person in Charge</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "projects.curr_phase_stage",
            "description": "<p>Project's current phase stage number</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "projects.due_date",
            "description": "<p>Project's current phase deadline</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "projects.updated_at",
            "description": "<p>Project's last updated time</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"pid\": 1,\n         \"name\": \"Jabar Social Global Contribution\",\n         \"pic\": \"johndoe\",\n         \"curr_phase_stage\": 1,\n         \"due_date\": \"2019-05-09 00:00:00\",\n         \"updated_at\": \"2019-05-09 13:14:25\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/wisata",
    "title": "Wisata - Request List of Project",
    "version": "0.1.0",
    "name": "GetAllProjectWisata",
    "group": "Project",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Query": [
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Filter by project status (1: In progress, 2: Completed, 3: Cancelled)</p>"
          },
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Filter by project current phase stage number (0: No filter)</p>"
          },
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "sort",
            "description": "<p>Sort user (1: Sort by newest created, 2: Sort by oldest updated, 3: Sort by phase number, 4: Sort by name)</p>"
          },
          {
            "group": "QueryParam",
            "type": "Number",
            "optional": false,
            "field": "participate",
            "description": "<p>Filter by user participation in project (0: No filter, 1: Filter on)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "projects",
            "description": "<p>List of projects</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "projects.pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "projects.name",
            "description": "<p>Name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "projects.pic",
            "description": "<p>Project's Person in Charge</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "projects.curr_phase_stage",
            "description": "<p>Project's current phase stage number</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "projects.due_date",
            "description": "<p>Project's current phase deadline</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "projects.updated_at",
            "description": "<p>Project's last updated time</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"pid\": 1,\n         \"name\": \"Improvement of Tangkuban Parahu\",\n         \"pic\": \"johndoe\",\n         \"curr_phase_stage\": 1,\n         \"due_date\": \"2019-05-09 00:00:00\",\n         \"updated_at\": \"2019-05-09 13:14:25\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/dagang/:pid",
    "title": "Dagang - Request Project by Project ID",
    "version": "0.1.0",
    "name": "GetProjectDagang",
    "group": "Project",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "curr_phase_stage",
            "description": "<p>Current Phase Project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.stage_num",
            "description": "<p>Due date of each stage num</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Current status of project (1 = ongoing, 2 = finished, 3 = cancelled)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"name\": \"Rehab IF\",\n    \"curr_phase_stage\": 1,\n    \"phase_deadline\": {\n       \"1\": \"2019-05-09 18:33:43\"\n    },\n    \"status\": 1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/dagang/:pid/edit",
    "title": "Dagang - Request Complete Project Details",
    "version": "0.1.0",
    "name": "GetProjectDagangEditDetails",
    "group": "Project",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category",
            "description": "<p>Project Category</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "curr_phase_stage",
            "description": "<p>Current Phase Project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": "<p>Name of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "company_address",
            "description": "<p>Address of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cp_name",
            "description": "<p>Name of project partner's representative</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cp_phone",
            "description": "<p>Phone number of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cp_email",
            "description": "<p>Email of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Brief description of the project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "created_at",
            "description": "<p>Project creation date</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Project last update's date</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Current project status</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Project partner's country of origin</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "phase_deadline",
            "description": "<p>Contains deadlines for each phase</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.1",
            "description": "<p>Deadline for project phase 1</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.2",
            "description": "<p>Deadline for project phase 2</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.3",
            "description": "<p>Deadline for project phase 3</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.4",
            "description": "<p>Deadline for project phase 4</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.5",
            "description": "<p>Deadline for project phase 5</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.6",
            "description": "<p>Deadline for project phase 6</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.7",
            "description": "<p>Deadline for project phase 7</p>"
          },
          {
            "group": "Success 200",
            "type": "Number[]",
            "optional": false,
            "field": "perangkat",
            "description": "<p>Array of indices of perangkat involved</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "list_perangkat",
            "description": "<p>Array of perangkat name ordered by id in DB</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n      \"id\": 29,\n      \"name\": \"Test Project 4\",\n      \"category\": 1,\n      \"pic\": \"test\",\n      \"curr_phase_stage\": 1,\n      \"company_name\": \"Tirta\",\n      \"company_address\": \"Bandung\",\n      \"cp_name\": \"Jon\",\n      \"cp_phone\": \"0812\",\n      \"cp_email\": \"a@gmail.com\",\n      \"description\": \"Testing Project\",\n      \"created_at\": \"2019-05-15T13:52:24.000Z\",\n      \"updated_at\": \"2019-05-15T13:52:24.000Z\",\n      \"status\": 1,\n      \"country\": \"Indonesia\",\n      \"phase_deadline\": {\n          \"1\": \"2020-03-01 00:00:00\",\n          \"2\": \"2020-03-01 00:00:00\",\n          \"3\": \"2020-03-01 00:00:00\",\n          \"4\": \"2020-03-01 00:00:00\",\n          \"5\": \"2020-03-01 00:00:00\",\n          \"6\": \"2020-03-01 00:00:00\",\n          \"7\": \"2020-03-01 00:00:00\"\n      },\n      \"perangkat\": [\n          1,\n         10,\n          13\n      ],\n      \"list_perangkat\": [\n          \"Biro Pemerintahan dan Kerjasama\",\n          \"Biro Hukum dan Hak Asasi Manusia\",\n          \"Biro Pelayanan dan Pengembangan Sosial\",\n          \"Biro Badan Usaha Milik Daerah dan Investasi\",\n          \"Biro Perekonomian\",\n          \"Biro Pengadaan Barang/Jasa\",\n          \"Biro Organisasi\",\n          \"Biro Hubungan Masyarakat dan Protokol\",\n          \"Biro Umum\",\n          \"Dinas Pendidikan\",\n          \"Dinas Kesehatan\",\n          \"Dinas Bina Marga dan Penataan Ruang\",\n          \"Dinas Sumber Daya Air\",\n          \"Dinas Perumahan dan Pemukiman\",\n          \"Dinas Sosial\",\n          \"Dinas Pemberdayaan Perempuan, Perlindungan Anak, dan Keluarga Berencana\",\n          \"Dinas Lingkungan Hidup\",\n          \"Dinas Pemberdayaan Masyarakat dan Desa\",\n          \"Dinas Perhubungan\",\n          \"Dinas Komunikasi dan Informatika\",\n          \"Dinas Koperasi dan Usaha Kecil\",\n          \"Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu\",\n          \"Dinas Pemuda dan Olahraga\",\n          \"Dinas Perpustakaan dan Kearsipan Daerah\",\n          \"Dinas Tenaga Kerja dan Transmigrasi\",\n          \"Dinas Ketahanan Pangan dan Peternakan\",\n          \"Dinas Pariwisata dan Kebudayaan\",\n          \"Dinas Kelautan dan Perikanan\",\n          \"Dinas Tanaman Pangan dan Holtikultura\",\n          \"Dinas Perkebunan\",\n          \"Dinas Kehutanan\",\n          \"Dinas Energi dan Sumber Daya Mineral\",\n          \"Dinas Perindustrian dan Perdagangan\",\n          \"Dinas Kependudukan dan Pencatatan Sipil\",\n          \"Badan Perencanaan Pembangunan Daerah\",\n          \"Badan Kepegawaian Daerah\",\n          \"Badan Pengembangan Sumber Daya Manusia\",\n          \"Badan Penelitian dan Pengembangan Daerah\",\n          \"Badan Pengelolaan Keuangan dan Aset Daerah\",\n          \"Badan Pendapatan Daerah\",\n          \"Badan Penanggulangan Bencana Daerah\"\n      ]\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/investasi/:pid",
    "title": "Investasi - Request Project by Project ID",
    "version": "0.1.0",
    "name": "GetProjectInvestasi",
    "group": "Project",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "curr_phase_stage",
            "description": "<p>Current Phase Project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.stage_num",
            "description": "<p>Due date of each stage num</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Current status of project (1 = ongoing, 2 = finished, 3 = cancelled)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"name\": \"Rehab IF\",\n    \"curr_phase_stage\": 1,\n    \"phase_deadline\": {\n       \"1\": \"2019-05-09 18:33:43\"\n    },\n    \"status\": 1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/investasi/:pid/edit",
    "title": "Investasi - Request Complete Project Details",
    "version": "0.1.0",
    "name": "GetProjectInvestasiEditDetails",
    "group": "Project",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category",
            "description": "<p>Project Category</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "curr_phase_stage",
            "description": "<p>Current Phase Project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": "<p>Name of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "company_address",
            "description": "<p>Address of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cp_name",
            "description": "<p>Name of project partner's representative</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cp_phone",
            "description": "<p>Phone number of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cp_email",
            "description": "<p>Email of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Brief description of the project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "created_at",
            "description": "<p>Project creation date</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Project last update's date</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Current project status</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Project partner's country of origin</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "phase_deadline",
            "description": "<p>Contains deadlines for each phase</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.1",
            "description": "<p>Deadline for project phase 1</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.2",
            "description": "<p>Deadline for project phase 2</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.3",
            "description": "<p>Deadline for project phase 3</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.4",
            "description": "<p>Deadline for project phase 4</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.5",
            "description": "<p>Deadline for project phase 5</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.6",
            "description": "<p>Deadline for project phase 6</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.7",
            "description": "<p>Deadline for project phase 7</p>"
          },
          {
            "group": "Success 200",
            "type": "Number[]",
            "optional": false,
            "field": "perangkat",
            "description": "<p>Array of indices of perangkat involved</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n      \"id\": 29,\n      \"name\": \"Test Project 4\",\n      \"category\": 1,\n      \"pic\": \"test\",\n      \"curr_phase_stage\": 1,\n      \"company_name\": \"Tirta\",\n      \"company_address\": \"Bandung\",\n      \"cp_name\": \"Jon\",\n      \"cp_phone\": \"0812\",\n      \"cp_email\": \"a@gmail.com\",\n      \"description\": \"Testing Project\",\n      \"created_at\": \"2019-05-15T13:52:24.000Z\",\n      \"updated_at\": \"2019-05-15T13:52:24.000Z\",\n      \"status\": 1,\n      \"country\": \"Indonesia\",\n      \"phase_deadline\": {\n          \"1\": \"2020-03-01 00:00:00\",\n          \"2\": \"2020-03-01 00:00:00\",\n          \"3\": \"2020-03-01 00:00:00\",\n          \"4\": \"2020-03-01 00:00:00\",\n          \"5\": \"2020-03-01 00:00:00\",\n          \"6\": \"2020-03-01 00:00:00\",\n          \"7\": \"2020-03-01 00:00:00\"\n      },\n      \"perangkat\": [\n          1,\n         10,\n          13\n      ],\n      \"list_perangkat\": [\n          \"Biro Pemerintahan dan Kerjasama\",\n          \"Biro Hukum dan Hak Asasi Manusia\",\n          \"Biro Pelayanan dan Pengembangan Sosial\",\n          \"Biro Badan Usaha Milik Daerah dan Investasi\",\n          \"Biro Perekonomian\",\n          \"Biro Pengadaan Barang/Jasa\",\n          \"Biro Organisasi\",\n          \"Biro Hubungan Masyarakat dan Protokol\",\n          \"Biro Umum\",\n          \"Dinas Pendidikan\",\n          \"Dinas Kesehatan\",\n          \"Dinas Bina Marga dan Penataan Ruang\",\n          \"Dinas Sumber Daya Air\",\n          \"Dinas Perumahan dan Pemukiman\",\n          \"Dinas Sosial\",\n          \"Dinas Pemberdayaan Perempuan, Perlindungan Anak, dan Keluarga Berencana\",\n          \"Dinas Lingkungan Hidup\",\n          \"Dinas Pemberdayaan Masyarakat dan Desa\",\n          \"Dinas Perhubungan\",\n          \"Dinas Komunikasi dan Informatika\",\n          \"Dinas Koperasi dan Usaha Kecil\",\n          \"Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu\",\n          \"Dinas Pemuda dan Olahraga\",\n          \"Dinas Perpustakaan dan Kearsipan Daerah\",\n          \"Dinas Tenaga Kerja dan Transmigrasi\",\n          \"Dinas Ketahanan Pangan dan Peternakan\",\n          \"Dinas Pariwisata dan Kebudayaan\",\n          \"Dinas Kelautan dan Perikanan\",\n          \"Dinas Tanaman Pangan dan Holtikultura\",\n          \"Dinas Perkebunan\",\n          \"Dinas Kehutanan\",\n          \"Dinas Energi dan Sumber Daya Mineral\",\n          \"Dinas Perindustrian dan Perdagangan\",\n          \"Dinas Kependudukan dan Pencatatan Sipil\",\n          \"Badan Perencanaan Pembangunan Daerah\",\n          \"Badan Kepegawaian Daerah\",\n          \"Badan Pengembangan Sumber Daya Manusia\",\n          \"Badan Penelitian dan Pengembangan Daerah\",\n          \"Badan Pengelolaan Keuangan dan Aset Daerah\",\n          \"Badan Pendapatan Daerah\",\n          \"Badan Penanggulangan Bencana Daerah\"\n      ]\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/dagang/:pid/participants",
    "title": "Dagang - Request Participants in a Project",
    "version": "0.1.0",
    "name": "GetProjectParticipantsDagang",
    "group": "Project",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "participants",
            "description": "<p>List of participants in a project</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "participants.uid",
            "description": "<p>Current Phase Project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "participants.name",
            "description": "<p>Due date of each stage num</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    [\n     {\n         \"name\": \"test\",\n          \"uid\": 1\n     },\n      {\n        \"name\": \"nico\",\n          \"uid\": 2\n     }\n    ]\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/investasi/:pid/participants",
    "title": "Investasi - Request Participants in a Project",
    "version": "0.1.0",
    "name": "GetProjectParticipantsInvestasi",
    "group": "Project",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "participants",
            "description": "<p>List of participants in a project</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "participants.uid",
            "description": "<p>Current Phase Project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "participants.name",
            "description": "<p>Due date of each stage num</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    [\n     {\n         \"name\": \"test\",\n          \"uid\": 1\n     },\n      {\n        \"name\": \"nico\",\n          \"uid\": 2\n     }\n    ]\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/sosial/:pid/participants",
    "title": "SOsial - Request Participants in a Project",
    "version": "0.1.0",
    "name": "GetProjectParticipantsSosial",
    "group": "Project",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "participants",
            "description": "<p>List of participants in a project</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "participants.uid",
            "description": "<p>Current Phase Project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "participants.name",
            "description": "<p>Due date of each stage num</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    [\n     {\n         \"name\": \"test\",\n          \"uid\": 1\n     },\n      {\n        \"name\": \"nico\",\n          \"uid\": 2\n     }\n    ]\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/wisata/:pid/participants",
    "title": "Wisata - Request Participants in a Project",
    "version": "0.1.0",
    "name": "GetProjectParticipantsWisata",
    "group": "Project",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "participants",
            "description": "<p>List of participants in a project</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "participants.uid",
            "description": "<p>Current Phase Project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "participants.name",
            "description": "<p>Due date of each stage num</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    [\n     {\n         \"name\": \"test\",\n          \"uid\": 1\n     },\n      {\n        \"name\": \"nico\",\n          \"uid\": 2\n     }\n    ]\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/sosial/:pid",
    "title": "Sosial - Request Project by Project ID",
    "version": "0.1.0",
    "name": "GetProjectSosial",
    "group": "Project",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "curr_phase_stage",
            "description": "<p>Current Phase Project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.stage_num",
            "description": "<p>Due date of each stage num</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Current status of project (1 = ongoing, 2 = finished, 3 = cancelled)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"name\": \"Rehab IF\",\n    \"curr_phase_stage\": 1,\n    \"phase_deadline\": {\n       \"1\": \"2019-05-09 18:33:43\"\n    },\n    \"status\": 1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/sosial/:pid/edit",
    "title": "Sosial - Request Complete Project Details",
    "version": "0.1.0",
    "name": "GetProjectSosialEditDetails",
    "group": "Project",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category",
            "description": "<p>Project Category</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "curr_phase_stage",
            "description": "<p>Current Phase Project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": "<p>Name of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "company_address",
            "description": "<p>Address of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cp_name",
            "description": "<p>Name of project partner's representative</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cp_phone",
            "description": "<p>Phone number of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cp_email",
            "description": "<p>Email of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Brief description of the project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "created_at",
            "description": "<p>Project creation date</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Project last update's date</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Current project status</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Project partner's country of origin</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "phase_deadline",
            "description": "<p>Contains deadlines for each phase</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.1",
            "description": "<p>Deadline for project phase 1</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.2",
            "description": "<p>Deadline for project phase 2</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.3",
            "description": "<p>Deadline for project phase 3</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.4",
            "description": "<p>Deadline for project phase 4</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.5",
            "description": "<p>Deadline for project phase 5</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.6",
            "description": "<p>Deadline for project phase 6</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.7",
            "description": "<p>Deadline for project phase 7</p>"
          },
          {
            "group": "Success 200",
            "type": "Number[]",
            "optional": false,
            "field": "perangkat",
            "description": "<p>Array of indices of perangkat involved</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n      \"id\": 29,\n      \"name\": \"Test Project 4\",\n      \"category\": 1,\n      \"pic\": \"test\",\n      \"curr_phase_stage\": 1,\n      \"company_name\": \"Tirta\",\n      \"company_address\": \"Bandung\",\n      \"cp_name\": \"Jon\",\n      \"cp_phone\": \"0812\",\n      \"cp_email\": \"a@gmail.com\",\n      \"description\": \"Testing Project\",\n      \"created_at\": \"2019-05-15T13:52:24.000Z\",\n      \"updated_at\": \"2019-05-15T13:52:24.000Z\",\n      \"status\": 1,\n      \"country\": \"Indonesia\",\n      \"phase_deadline\": {\n          \"1\": \"2020-03-01 00:00:00\",\n          \"2\": \"2020-03-01 00:00:00\",\n          \"3\": \"2020-03-01 00:00:00\",\n          \"4\": \"2020-03-01 00:00:00\",\n          \"5\": \"2020-03-01 00:00:00\",\n          \"6\": \"2020-03-01 00:00:00\",\n          \"7\": \"2020-03-01 00:00:00\"\n      },\n      \"perangkat\": [\n          1,\n         10,\n          13\n      ],\n      \"list_perangkat\": [\n          \"Biro Pemerintahan dan Kerjasama\",\n          \"Biro Hukum dan Hak Asasi Manusia\",\n          \"Biro Pelayanan dan Pengembangan Sosial\",\n          \"Biro Badan Usaha Milik Daerah dan Investasi\",\n          \"Biro Perekonomian\",\n          \"Biro Pengadaan Barang/Jasa\",\n          \"Biro Organisasi\",\n          \"Biro Hubungan Masyarakat dan Protokol\",\n          \"Biro Umum\",\n          \"Dinas Pendidikan\",\n          \"Dinas Kesehatan\",\n          \"Dinas Bina Marga dan Penataan Ruang\",\n          \"Dinas Sumber Daya Air\",\n          \"Dinas Perumahan dan Pemukiman\",\n          \"Dinas Sosial\",\n          \"Dinas Pemberdayaan Perempuan, Perlindungan Anak, dan Keluarga Berencana\",\n          \"Dinas Lingkungan Hidup\",\n          \"Dinas Pemberdayaan Masyarakat dan Desa\",\n          \"Dinas Perhubungan\",\n          \"Dinas Komunikasi dan Informatika\",\n          \"Dinas Koperasi dan Usaha Kecil\",\n          \"Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu\",\n          \"Dinas Pemuda dan Olahraga\",\n          \"Dinas Perpustakaan dan Kearsipan Daerah\",\n          \"Dinas Tenaga Kerja dan Transmigrasi\",\n          \"Dinas Ketahanan Pangan dan Peternakan\",\n          \"Dinas Pariwisata dan Kebudayaan\",\n          \"Dinas Kelautan dan Perikanan\",\n          \"Dinas Tanaman Pangan dan Holtikultura\",\n          \"Dinas Perkebunan\",\n          \"Dinas Kehutanan\",\n          \"Dinas Energi dan Sumber Daya Mineral\",\n          \"Dinas Perindustrian dan Perdagangan\",\n          \"Dinas Kependudukan dan Pencatatan Sipil\",\n          \"Badan Perencanaan Pembangunan Daerah\",\n          \"Badan Kepegawaian Daerah\",\n          \"Badan Pengembangan Sumber Daya Manusia\",\n          \"Badan Penelitian dan Pengembangan Daerah\",\n          \"Badan Pengelolaan Keuangan dan Aset Daerah\",\n          \"Badan Pendapatan Daerah\",\n          \"Badan Penanggulangan Bencana Daerah\"\n      ]\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/wisata/:pid",
    "title": "Wisata - Request Project by Project ID",
    "version": "0.1.0",
    "name": "GetProjectWisata",
    "group": "Project",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "curr_phase_stage",
            "description": "<p>Current Phase Project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.stage_num",
            "description": "<p>Due date of each stage num</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Current status of project (1 = ongoing, 2 = finished, 3 = cancelled)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"name\": \"Rehab IF\",\n    \"curr_phase_stage\": 1,\n    \"phase_deadline\": {\n       \"1\": \"2019-05-09 18:33:43\"\n    },\n    \"status\": 1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/wisata/:pid/edit",
    "title": "Wisata - Request Complete Project Details",
    "version": "0.1.0",
    "name": "GetProjectWisataEditDetails",
    "group": "Project",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Project Name</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category",
            "description": "<p>Project Category</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "curr_phase_stage",
            "description": "<p>Current Phase Project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "company_name",
            "description": "<p>Name of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "company_address",
            "description": "<p>Address of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cp_name",
            "description": "<p>Name of project partner's representative</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cp_phone",
            "description": "<p>Phone number of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cp_email",
            "description": "<p>Email of project partner</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Brief description of the project</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "created_at",
            "description": "<p>Project creation date</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Project last update's date</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Current project status</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Project partner's country of origin</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "phase_deadline",
            "description": "<p>Contains deadlines for each phase</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.1",
            "description": "<p>Deadline for project phase 1</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.2",
            "description": "<p>Deadline for project phase 2</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.3",
            "description": "<p>Deadline for project phase 3</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.4",
            "description": "<p>Deadline for project phase 4</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.5",
            "description": "<p>Deadline for project phase 5</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.6",
            "description": "<p>Deadline for project phase 6</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phase_deadline.7",
            "description": "<p>Deadline for project phase 7</p>"
          },
          {
            "group": "Success 200",
            "type": "Number[]",
            "optional": false,
            "field": "perangkat",
            "description": "<p>Array of indices of perangkat involved</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n      \"id\": 29,\n      \"name\": \"Test Project 4\",\n      \"category\": 1,\n      \"pic\": \"test\",\n      \"curr_phase_stage\": 1,\n      \"company_name\": \"Tirta\",\n      \"company_address\": \"Bandung\",\n      \"cp_name\": \"Jon\",\n      \"cp_phone\": \"0812\",\n      \"cp_email\": \"a@gmail.com\",\n      \"description\": \"Testing Project\",\n      \"created_at\": \"2019-05-15T13:52:24.000Z\",\n      \"updated_at\": \"2019-05-15T13:52:24.000Z\",\n      \"status\": 1,\n      \"country\": \"Indonesia\",\n      \"phase_deadline\": {\n          \"1\": \"2020-03-01 00:00:00\",\n          \"2\": \"2020-03-01 00:00:00\",\n          \"3\": \"2020-03-01 00:00:00\",\n          \"4\": \"2020-03-01 00:00:00\",\n          \"5\": \"2020-03-01 00:00:00\",\n          \"6\": \"2020-03-01 00:00:00\",\n          \"7\": \"2020-03-01 00:00:00\"\n      },\n      \"perangkat\": [\n          1,\n         10,\n          13\n      ],\n      \"list_perangkat\": [\n          \"Biro Pemerintahan dan Kerjasama\",\n          \"Biro Hukum dan Hak Asasi Manusia\",\n          \"Biro Pelayanan dan Pengembangan Sosial\",\n          \"Biro Badan Usaha Milik Daerah dan Investasi\",\n          \"Biro Perekonomian\",\n          \"Biro Pengadaan Barang/Jasa\",\n          \"Biro Organisasi\",\n          \"Biro Hubungan Masyarakat dan Protokol\",\n          \"Biro Umum\",\n          \"Dinas Pendidikan\",\n          \"Dinas Kesehatan\",\n          \"Dinas Bina Marga dan Penataan Ruang\",\n          \"Dinas Sumber Daya Air\",\n          \"Dinas Perumahan dan Pemukiman\",\n          \"Dinas Sosial\",\n          \"Dinas Pemberdayaan Perempuan, Perlindungan Anak, dan Keluarga Berencana\",\n          \"Dinas Lingkungan Hidup\",\n          \"Dinas Pemberdayaan Masyarakat dan Desa\",\n          \"Dinas Perhubungan\",\n          \"Dinas Komunikasi dan Informatika\",\n          \"Dinas Koperasi dan Usaha Kecil\",\n          \"Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu\",\n          \"Dinas Pemuda dan Olahraga\",\n          \"Dinas Perpustakaan dan Kearsipan Daerah\",\n          \"Dinas Tenaga Kerja dan Transmigrasi\",\n          \"Dinas Ketahanan Pangan dan Peternakan\",\n          \"Dinas Pariwisata dan Kebudayaan\",\n          \"Dinas Kelautan dan Perikanan\",\n          \"Dinas Tanaman Pangan dan Holtikultura\",\n          \"Dinas Perkebunan\",\n          \"Dinas Kehutanan\",\n          \"Dinas Energi dan Sumber Daya Mineral\",\n          \"Dinas Perindustrian dan Perdagangan\",\n          \"Dinas Kependudukan dan Pencatatan Sipil\",\n          \"Badan Perencanaan Pembangunan Daerah\",\n          \"Badan Kepegawaian Daerah\",\n          \"Badan Pengembangan Sumber Daya Manusia\",\n          \"Badan Penelitian dan Pengembangan Daerah\",\n          \"Badan Pengelolaan Keuangan dan Aset Daerah\",\n          \"Badan Pendapatan Daerah\",\n          \"Badan Penanggulangan Bencana Daerah\"\n      ]\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/dagang/phase",
    "title": "Dagang - Change project phase",
    "version": "0.1.0",
    "name": "UpdateProjectPhaseDagang",
    "group": "Project",
    "permission": [
      {
        "name": "dagang, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "new_phase",
            "description": "<p>New Project Phase</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Project phase updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/investasi/phase",
    "title": "Investasi - Change project phase",
    "version": "0.1.0",
    "name": "UpdateProjectPhaseInvestasi",
    "group": "Project",
    "permission": [
      {
        "name": "investasi, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "new_phase",
            "description": "<p>New Project Phase</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Project phase updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/sosial/phase",
    "title": "Sosial - Change project phase",
    "version": "0.1.0",
    "name": "UpdateProjectPhaseSosial",
    "group": "Project",
    "permission": [
      {
        "name": "sosial, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "new_phase",
            "description": "<p>New Project Phase</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Project phase updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/wisata/phase",
    "title": "Wisata - Change project phase",
    "version": "0.1.0",
    "name": "UpdateProjectPhaseWisata",
    "group": "Project",
    "permission": [
      {
        "name": "wisata, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "new_phase",
            "description": "<p>New Project Phase</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Project phase updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/dagang/status",
    "title": "Dagang - Update Project Status",
    "version": "0.1.0",
    "name": "UpdateProjectStatusDagang",
    "group": "Project",
    "permission": [
      {
        "name": "Dagang, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "new_status",
            "description": "<p>New Project Status</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Project status updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/investasi/status",
    "title": "Investasi - Update Project Status",
    "version": "0.1.0",
    "name": "UpdateProjectStatusInvestasi",
    "group": "Project",
    "permission": [
      {
        "name": "investasi, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "new_status",
            "description": "<p>New Project Status</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Project status updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/sosial/status",
    "title": "Sosial - Update Project Status",
    "version": "0.1.0",
    "name": "UpdateProjectStatusSosial",
    "group": "Project",
    "permission": [
      {
        "name": "Sosial, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "new_status",
            "description": "<p>New Project Status</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Project status updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/wisata",
    "title": "status Wisata - Update Project Status",
    "version": "0.1.0",
    "name": "UpdateProjectStatusWisata",
    "group": "Project",
    "permission": [
      {
        "name": "Wisata, pic"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "new_status",
            "description": "<p>New Project Status</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Project status updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project.js",
    "groupTitle": "Project",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/dagang/comments",
    "title": "Dagang - Add New Comment to Project Phase",
    "version": "0.1.0",
    "name": "AddProjectCommentDagang",
    "group": "Project_Phase_Comment",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment Text</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Comment added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-comment.js",
    "groupTitle": "Project_Phase_Comment",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/investasi/comments",
    "title": "Investasi - Add New Comment to Project Phase",
    "version": "0.1.0",
    "name": "AddProjectCommentInvestasi",
    "group": "Project_Phase_Comment",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment Text</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Comment added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-comment.js",
    "groupTitle": "Project_Phase_Comment",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/sosial/comments",
    "title": "Sosial - Add New Comment to Project Phase",
    "version": "0.1.0",
    "name": "AddProjectCommentSosial",
    "group": "Project_Phase_Comment",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment Text</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Comment added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-comment.js",
    "groupTitle": "Project_Phase_Comment",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/wisata/comments",
    "title": "Wisata - Add New Comment to Project Phase",
    "version": "0.1.0",
    "name": "AddProjectCommentWisata",
    "group": "Project_Phase_Comment",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment Text</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Comment added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-comment.js",
    "groupTitle": "Project_Phase_Comment",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/dagang/comments",
    "title": "Dagang - Delete Comment Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectCommentDagang",
    "group": "Project_Phase_Comment",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "comment_id",
            "description": "<p>Comment ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Comment Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-comment.js",
    "groupTitle": "Project_Phase_Comment",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/investasi/comments",
    "title": "Investasi - Delete Comment Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectCommentInvestasi",
    "group": "Project_Phase_Comment",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "comment_id",
            "description": "<p>Comment ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Comment Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-comment.js",
    "groupTitle": "Project_Phase_Comment",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/sosial/comments",
    "title": "Sosial - Delete Comment Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectCommentSosial",
    "group": "Project_Phase_Comment",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "comment_id",
            "description": "<p>Comment ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Comment Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-comment.js",
    "groupTitle": "Project_Phase_Comment",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/wisata/comments",
    "title": "Wisata - Delete Comment Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectCommentWisata",
    "group": "Project_Phase_Comment",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "comment_id",
            "description": "<p>Comment ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Comment Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-comment.js",
    "groupTitle": "Project_Phase_Comment",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/dagang/:pid/comments/:phase",
    "title": "Dagang - Request Project Phase's Comments",
    "version": "0.1.0",
    "name": "GetProjectCommentDagang",
    "group": "Project_Phase_Comment",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "comments",
            "description": "<p>List of comments</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "comments.id",
            "description": "<p>Comment ID</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "comments.comment",
            "description": "<p>Comment content</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 30,\n         \"comment\": \"Wow what a nice project this is\"\n     },\n     {\n         \"id\": 29,\n         \"comment\": \"I think we need to do a meeting for this subject\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-comment.js",
    "groupTitle": "Project_Phase_Comment",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/investasi/:pid/comments/:phase",
    "title": "Investasi - Request Project Phase's Comments",
    "version": "0.1.0",
    "name": "GetProjectCommentInvestasi",
    "group": "Project_Phase_Comment",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "comments",
            "description": "<p>List of comments</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "comments.id",
            "description": "<p>Comment ID</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "comments.comment",
            "description": "<p>Comment content</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 30,\n         \"comment\": \"Wow what a nice project this is\"\n     },\n     {\n         \"id\": 29,\n         \"comment\": \"I think we need to do a meeting for this subject\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-comment.js",
    "groupTitle": "Project_Phase_Comment",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/sosial/:pid/comments/:phase",
    "title": "Sosial - Request Project Phase's Comments",
    "version": "0.1.0",
    "name": "GetProjectCommentSosial",
    "group": "Project_Phase_Comment",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "comments",
            "description": "<p>List of comments</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "comments.id",
            "description": "<p>Comment ID</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "comments.comment",
            "description": "<p>Comment content</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 30,\n         \"comment\": \"Wow what a nice project this is\"\n     },\n     {\n         \"id\": 29,\n         \"comment\": \"I think we need to do a meeting for this subject\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-comment.js",
    "groupTitle": "Project_Phase_Comment",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/wisata/:pid/comments/:phase",
    "title": "Wisata - Request Project Phase's Comments",
    "version": "0.1.0",
    "name": "GetProjectCommentWisata",
    "group": "Project_Phase_Comment",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "comments",
            "description": "<p>List of comments</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "comments.id",
            "description": "<p>Comment ID</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "comments.comment",
            "description": "<p>Comment content</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 30,\n         \"comment\": \"Wow what a nice project this is\"\n     },\n     {\n         \"id\": 29,\n         \"comment\": \"I think we need to do a meeting for this subject\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-comment.js",
    "groupTitle": "Project_Phase_Comment",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/dagang/documents",
    "title": "Dagang - Add Document to Project",
    "version": "0.1.0",
    "name": "AddProjectDocumentDagang",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Current Stage Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Document Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "document",
            "description": "<p>Document Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Document added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/investasi/documents",
    "title": "Investasi - Add Document to Project",
    "version": "0.1.0",
    "name": "AddProjectDocumentInvestasi",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Current Stage Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Document Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "document",
            "description": "<p>Document Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Document added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/sosial/documents",
    "title": "Sosial - Add Document to Project",
    "version": "0.1.0",
    "name": "AddProjectDocumentSosial",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Current Stage Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Document Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "document",
            "description": "<p>Document Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Document added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/wisata/documents",
    "title": "Wisata - Add Document to Project",
    "version": "0.1.0",
    "name": "AddProjectDocumentWisata",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Current Stage Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Document Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "document",
            "description": "<p>Document Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Document added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/dagang/documents",
    "title": "Dagang - Delete Document Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectDocumentDagang",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "document_id",
            "description": "<p>Document ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Document Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/investasi/documents",
    "title": "Investasi - Delete Document Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectDocumentInvestasi",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "document_id",
            "description": "<p>Document ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Document Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/sosial/documents",
    "title": "Sosial - Delete Document Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectDocumentSosial",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "document_id",
            "description": "<p>Document ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Document Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/wisata/documents",
    "title": "Wisata - Delete Document Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectDocumentWisata",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "document_id",
            "description": "<p>Document ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Document Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/dagang/:pid/documents/:phase",
    "title": "Dagang - Request Project Phase's Documents",
    "version": "0.1.0",
    "name": "GetProjectDocumentDagang",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "documents",
            "description": "<p>List of documents</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "documents.id",
            "description": "<p>Document ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "documents.name",
            "description": "<p>Document title name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "documents.document",
            "description": "<p>Document link</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 25,\n         \"name\": \"Report ver_1.pdf\",\n         \"document\": \"https://drive.google.com/file_path\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/investasi/:pid/documents/:phase",
    "title": "Investasi - Request Project Phase's Documents",
    "version": "0.1.0",
    "name": "GetProjectDocumentInvestasi",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "documents",
            "description": "<p>List of documents</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "documents.id",
            "description": "<p>Document ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "documents.name",
            "description": "<p>Document title name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "documents.document",
            "description": "<p>Document link</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 25,\n         \"name\": \"Report ver_1.pdf\",\n         \"document\": \"https://drive.google.com/file_path\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/sosial/:pid/documents/:phase",
    "title": "Sosial - Request Project Phase's Documents",
    "version": "0.1.0",
    "name": "GetProjectDocumentSosial",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "documents",
            "description": "<p>List of documents</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "documents.id",
            "description": "<p>Document ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "documents.name",
            "description": "<p>Document title name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "documents.document",
            "description": "<p>Document link</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 25,\n         \"name\": \"Report ver_1.pdf\",\n         \"document\": \"https://drive.google.com/file_path\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/wisata/:pid/documents/:phase",
    "title": "Wisata - Request Project Phase's Documents",
    "version": "0.1.0",
    "name": "GetProjectDocumentWisata",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "documents",
            "description": "<p>List of documents</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "documents.id",
            "description": "<p>Document ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "documents.name",
            "description": "<p>Document title name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "documents.document",
            "description": "<p>Document link</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 25,\n         \"name\": \"Report ver_1.pdf\",\n         \"document\": \"https://drive.google.com/file_path\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/dagang/documents",
    "title": "Dagang - Update Project Document",
    "version": "0.1.0",
    "name": "UpdateProjectDocumentDagang",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "document_id",
            "description": "<p>Document ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>New Document Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "document",
            "description": "<p>New Document Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Document updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/investasi/documents",
    "title": "Investasi - Update Project Document",
    "version": "0.1.0",
    "name": "UpdateProjectDocumentInvestasi",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "document_id",
            "description": "<p>Document ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>New Document Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "document",
            "description": "<p>New Document Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Document updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/sosial/documents",
    "title": "Sosial - Update Project Document",
    "version": "0.1.0",
    "name": "UpdateProjectDocumentSosial",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "document_id",
            "description": "<p>Document ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>New Document Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "document",
            "description": "<p>New Document Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Document updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/wisata/documents",
    "title": "Wisata - Update Project Document",
    "version": "0.1.0",
    "name": "UpdateProjectDocumentWisata",
    "group": "Project_Phase_Document",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "document_id",
            "description": "<p>Document ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>New Document Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "document",
            "description": "<p>New Document Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Document updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-document.js",
    "groupTitle": "Project_Phase_Document",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/dagang/photos",
    "title": "Investasi - Add Photo to Project",
    "version": "0.1.0",
    "name": "AddProjectPhotoDagang",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Current Stage Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Photo Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "photo",
            "description": "<p>Photo Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Photo added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/investasi/photos",
    "title": "Investasi - Add Photo to Project",
    "version": "0.1.0",
    "name": "AddProjectPhotoInvestasi",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Current Stage Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Photo Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "photo",
            "description": "<p>Photo Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Photo added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/sosial/photos",
    "title": "Investasi - Add Photo to Project",
    "version": "0.1.0",
    "name": "AddProjectPhotoSosial",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Current Stage Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Photo Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "photo",
            "description": "<p>Photo Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Photo added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/wisata/photos",
    "title": "Investasi - Add Photo to Project",
    "version": "0.1.0",
    "name": "AddProjectPhotoWisata",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Current Stage Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Photo Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "photo",
            "description": "<p>Photo Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Photo added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/dagang/photos",
    "title": "Dagang - Delete Photo Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectPhotoDagang",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "photo_id",
            "description": "<p>Photo ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Photo Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/investasi/photos",
    "title": "Investasi - Delete Photo Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectPhotoInvestasi",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "photo_id",
            "description": "<p>Photo ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Photo Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/sosial/photos",
    "title": "Sosial - Delete Photo Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectPhotoSosial",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "photo_id",
            "description": "<p>Photo ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Photo Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/wisata/photos",
    "title": "Wisata - Delete Photo Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectPhotoWisata",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "photo_id",
            "description": "<p>Photo ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Photo Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/dagang/:pid/photos/:phase",
    "title": "Dagang - Request Project Phase's Photos",
    "version": "0.1.0",
    "name": "GetProjectPhotoDagang",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "photos",
            "description": "<p>List of photos</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "photos.id",
            "description": "<p>Photo ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "photos.name",
            "description": "<p>Photo title name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "photos.photos",
            "description": "<p>Photo link</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 25,\n         \"name\": \"Onsite_observation.jpg\",\n         \"document\": \"https://drive.google.com/file_path\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/investasi/:pid/photos/:phase",
    "title": "Investasi - Request Project Phase's Photos",
    "version": "0.1.0",
    "name": "GetProjectPhotoInvestasi",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "photos",
            "description": "<p>List of photos</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "photos.id",
            "description": "<p>Photo ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "photos.name",
            "description": "<p>Photo title name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "photos.photos",
            "description": "<p>Photo link</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 25,\n         \"name\": \"Onsite_observation.jpg\",\n         \"document\": \"https://drive.google.com/file_path\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/sosial/:pid/photos/:phase",
    "title": "Sosial - Request Project Phase's Photos",
    "version": "0.1.0",
    "name": "GetProjectPhotoSosial",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "photos",
            "description": "<p>List of photos</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "photos.id",
            "description": "<p>Photo ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "photos.name",
            "description": "<p>Photo title name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "photos.photos",
            "description": "<p>Photo link</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 25,\n         \"name\": \"Onsite_observation.jpg\",\n         \"document\": \"https://drive.google.com/file_path\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/wisata/:pid/photos/:phase",
    "title": "Wisata - Request Project Phase's Photos",
    "version": "0.1.0",
    "name": "GetProjectPhotoWisata",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "photos",
            "description": "<p>List of photos</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "photos.id",
            "description": "<p>Photo ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "photos.name",
            "description": "<p>Photo title name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "photos.photos",
            "description": "<p>Photo link</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 25,\n         \"name\": \"Onsite_observation.jpg\",\n         \"document\": \"https://drive.google.com/file_path\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/dagang/photos",
    "title": "Dagang - Update Project Photo",
    "version": "0.1.0",
    "name": "UpdateProjectPhotoDagang",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "photo_id",
            "description": "<p>Photo ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>New Photo Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "photo",
            "description": "<p>New Photo Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Photo updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/investasi/photos",
    "title": "Investasi - Update Project Photo",
    "version": "0.1.0",
    "name": "UpdateProjectPhotoInvestasi",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "photo_id",
            "description": "<p>Photo ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>New Photo Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "photo",
            "description": "<p>New Photo Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Photo updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/sosial/photos",
    "title": "Sosial - Update Project Photo",
    "version": "0.1.0",
    "name": "UpdateProjectPhotoSosial",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "photo_id",
            "description": "<p>Photo ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>New Photo Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "photo",
            "description": "<p>New Photo Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Photo updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/wisata/photos",
    "title": "Wisata - Update Project Photo",
    "version": "0.1.0",
    "name": "UpdateProjectPhotoWisata",
    "group": "Project_Phase_Photo",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "photo_id",
            "description": "<p>Photo ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>New Photo Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "photo",
            "description": "<p>New Photo Link</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Photo updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-photo.js",
    "groupTitle": "Project_Phase_Photo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/dagang/todos",
    "title": "Investasi - Add Todo to Project",
    "version": "0.1.0",
    "name": "AddProjectTodoDagang",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Current Stage Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "todo",
            "description": "<p>Todo text</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/investasi/todos",
    "title": "Investasi - Add Todo to Project",
    "version": "0.1.0",
    "name": "AddProjectTodoInvestasi",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Current Stage Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "todo",
            "description": "<p>Todo text</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/sosial/todos",
    "title": "Sosial - Add Todo to Project",
    "version": "0.1.0",
    "name": "AddProjectTodoSosial",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Current Stage Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "todo",
            "description": "<p>Todo text</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/project/wisata/todos",
    "title": "Wisata - Add Todo to Project",
    "version": "0.1.0",
    "name": "AddProjectTodoWisata",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Current Stage Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "todo",
            "description": "<p>Todo text</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/dagang/todos",
    "title": "Dagang - Delete Todo Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectTodoDagang",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Todo ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/investasi/todos",
    "title": "Investasi - Delete Todo Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectTodoInvestasi",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Todo ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/sosial/todos",
    "title": "Sosial - Delete Todo Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectTodoSosial",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Todo ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/project/wisata/todos",
    "title": "Wisata - Delete Todo Within a Project",
    "version": "0.1.0",
    "name": "DeleteProjectTodoWisata",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Todo ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo Deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/dagang/:pid/todos/:phase",
    "title": "Dagang - Request Project Phase's To-Dos",
    "version": "0.1.0",
    "name": "GetProjectTodoDagang",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "todos",
            "description": "<p>List of todos</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "todos.id",
            "description": "<p>To-do ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "todos.description",
            "description": "<p>To-do task description</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "todos.is_checked",
            "description": "<p>To-do checkmark (1: checked, 0: not checked)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 20,\n         \"description\": \"Meeting # 1 @ Office\",\n         \"is_checked\": 1\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/investasi/:pid/todos/:phase",
    "title": "Investasi - Request Project Phase's To-Dos",
    "version": "0.1.0",
    "name": "GetProjectTodoInvestasi",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "todos",
            "description": "<p>List of todos</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "todos.id",
            "description": "<p>To-do ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "todos.description",
            "description": "<p>To-do task description</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "todos.is_checked",
            "description": "<p>To-do checkmark (1: checked, 0: not checked)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 20,\n         \"description\": \"Meeting # 1 @ Office\",\n         \"is_checked\": 1\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/sosial/:pid/todos/:phase",
    "title": "Sosial - Request Project Phase's To-Dos",
    "version": "0.1.0",
    "name": "GetProjectTodoSosial",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "todos",
            "description": "<p>List of todos</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "todos.id",
            "description": "<p>To-do ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "todos.description",
            "description": "<p>To-do task description</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "todos.is_checked",
            "description": "<p>To-do checkmark (1: checked, 0: not checked)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 20,\n         \"description\": \"Meeting # 1 @ Office\",\n         \"is_checked\": 1\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/project/wisata/:pid/todos/:phase",
    "title": "Wisata - Request Project Phase's To-Dos",
    "version": "0.1.0",
    "name": "GetProjectTodoWisata",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "phase",
            "description": "<p>Phase stage number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "todos",
            "description": "<p>List of todos</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "todos.id",
            "description": "<p>To-do ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "todos.description",
            "description": "<p>To-do task description</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "todos.is_checked",
            "description": "<p>To-do checkmark (1: checked, 0: not checked)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 20,\n         \"description\": \"Meeting # 1 @ Office\",\n         \"is_checked\": 1\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/dagang/todos",
    "title": "Dagang - Update Project Todo",
    "version": "0.1.0",
    "name": "UpdateProjectTodoDagang",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Todo ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "todo",
            "description": "<p>Todo Text</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/dagang/todos/toggle",
    "title": "Dagang - Toggle Todo Status",
    "version": "0.1.0",
    "name": "UpdateProjectTodoDagangToggle",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "dagang"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Todo ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo status toggled\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/investasi/todos",
    "title": "Investasi - Update Project Todo",
    "version": "0.1.0",
    "name": "UpdateProjectTodoInvestasi",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Todo ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "todo",
            "description": "<p>Todo Text</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/investasi/todos/toggle",
    "title": "Investasi - Toggle Todo Status",
    "version": "0.1.0",
    "name": "UpdateProjectTodoInvestasiToggle",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "investasi"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Todo ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo status toggled\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/sosial/todos",
    "title": "Sosial - Update Project Todo",
    "version": "0.1.0",
    "name": "UpdateProjectTodoSosial",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Todo ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "todo",
            "description": "<p>Todo Text</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/sosial/todos/toggle",
    "title": "Sosial - Toggle Todo Status",
    "version": "0.1.0",
    "name": "UpdateProjectTodoSosialToggle",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "sosial"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Todo ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo status toggled\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/wisata/todos",
    "title": "Wisata - Update Project Todo",
    "version": "0.1.0",
    "name": "UpdateProjectTodoWisata",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Todo ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "todo",
            "description": "<p>Todo Text</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo updated\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/project/wisata/todos/toggle",
    "title": "Wisata - Toggle Todo Status",
    "version": "0.1.0",
    "name": "UpdateProjectTodoWisataToggle",
    "group": "Project_Phase_Todo",
    "permission": [
      {
        "name": "wisata"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pid",
            "description": "<p>Project ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "todo_id",
            "description": "<p>Todo ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"Todo status toggled\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/project-todo.js",
    "groupTitle": "Project_Phase_Todo",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/user",
    "title": "Add New User",
    "version": "0.1.0",
    "name": "AddUser",
    "group": "User",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "BodyParam",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name</p>"
          },
          {
            "group": "BodyParam",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username</p>"
          },
          {
            "group": "BodyParam",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          },
          {
            "group": "BodyParam",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          },
          {
            "group": "BodyParam",
            "type": "Number[]",
            "optional": false,
            "field": "roles",
            "description": "<p>User Roles in ernumeration representation</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"User added\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/user.js",
    "groupTitle": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/user",
    "title": "Delete User",
    "version": "0.1.0",
    "name": "DeleteUser",
    "group": "User",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "BodyParam",
            "type": "Number",
            "optional": false,
            "field": "uid",
            "description": "<p>User ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"User deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/user.js",
    "groupTitle": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "patch",
    "url": "/user",
    "title": "Edit User",
    "version": "0.1.0",
    "name": "EditUser",
    "group": "User",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "parameter": {
      "fields": {
        "Body": [
          {
            "group": "BodyParam",
            "type": "Number",
            "optional": false,
            "field": "uid",
            "description": "<p>User ID</p>"
          },
          {
            "group": "BodyParam",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name</p>"
          },
          {
            "group": "BodyParam",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username</p>"
          },
          {
            "group": "BodyParam",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          },
          {
            "group": "BodyParam",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          },
          {
            "group": "BodyParam",
            "type": "Number[]",
            "optional": false,
            "field": "roles",
            "description": "<p>User Roles in ernumeration representation</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     status: \"200\",\n     message: \"User edited\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/user.js",
    "groupTitle": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/user",
    "title": "Request All User Information",
    "version": "0.1.0",
    "name": "GetAllUser",
    "group": "User",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "users",
            "description": "<p>List of users</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "users.uid",
            "description": "<p>User ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.name",
            "description": "<p>Name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.username",
            "description": "<p>Username</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Success 200",
            "type": "Number[]",
            "optional": false,
            "field": "users.roles",
            "description": "<p>User Roles in enumeration representation</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": \"5\",\n         \"name\": \"John Doe\",\n         \"username\": \"johndoe\",\n         \"email\": \"johndoe@email.com\",\n         \"roles\": [\n             1,\n             2\n         ]\n     }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/user.js",
    "groupTitle": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/user/:uid",
    "title": "Request Specific User Information",
    "version": "0.1.0",
    "name": "GetUser",
    "group": "User",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "uid",
            "description": "<p>User ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.name",
            "description": "<p>Name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.username",
            "description": "<p>Username</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Success 200",
            "type": "Number[]",
            "optional": false,
            "field": "users.roles",
            "description": "<p>User Roles in ernumeration representation</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     \"name\": \"John Doe\",\n     \"username\": \"johndoe\",\n     \"email\": \"johndoe@email.com\",\n     \"roles\": [\n         1,\n         2\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/user.js",
    "groupTitle": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/user/search",
    "title": "Request User Id Based on Username",
    "version": "0.1.0",
    "name": "GetUserIdByUsername",
    "group": "User",
    "permission": [
      {
        "name": "Login"
      }
    ],
    "parameter": {
      "fields": {
        "Query": [
          {
            "group": "QueryParam",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Username of the searched user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User ID</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n   {\n      \"id\": 2\n   }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "src/controllers/user.js",
    "groupTitle": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "TokenHeader",
            "optional": false,
            "field": "x-access-token",
            "description": "<p>User's token</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "401:Unauthorized",
            "description": "<p>API call not authorized, need correct authorization (see permission)</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "403:Forbidden",
            "description": "<p>API call not authenticated, need to login</p>"
          }
        ],
        "Error 5xx": [
          {
            "group": "Error5",
            "optional": false,
            "field": "500:InternalServerError",
            "description": "<p>Error in server part, please redo</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 UNAUTHORIZED\n{\n    code: 401,\n    message: \"UNAUTHORIZED\"\n}",
          "type": "json"
        }
      ]
    }
  }
] });
