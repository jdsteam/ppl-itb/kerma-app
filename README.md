# Sistem Informasi Kerjasama

## Quickstart

Clone the repository

```bash
git clone https://gitlab.com/jdsteam/ppl-itb/kerma-app.git
cd kerma-app
```

Create `.env` file from `.env-sample`

```bash
cp .env-sample .env
```

Edit `.env` to be as your credentials using your fovourite text editor and save it

```bash
nano .env
```

Run container with `docker-compose`

```bash
sudo docker-compose up -d
```

Check if the containers run successfully

```bash
sudo docker ps
```

you should see something like this


```bash
CONTAINER ID        IMAGE                           COMMAND                  CREATED             STATUS              PORTS                    NAMES
05f08b718f07        asatrya/kermaapp-frontend:1.0   "/usr/local/bin/entr…"   27 seconds ago      Up 25 seconds       0.0.0.0:3001->3000/tcp   kerma-app_frontend_1
32933e06ae43        asatrya/kermaapp-backend:1.0    "/usr/local/bin/entr…"   28 seconds ago      Up 26 seconds       0.0.0.0:3000->3000/tcp   kerma-app_backend_1
4dbb4d135ade        asatrya/mysql:5.6               "docker-entrypoint.s…"   30 seconds ago      Up 27 seconds       0.0.0.0:3306->3306/tcp   kerma-app_db_1

```
