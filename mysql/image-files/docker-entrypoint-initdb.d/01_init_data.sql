--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'test','test','test@gmail.com','test','2019-05-09 08:59:48','2019-05-13 14:36:49'),(2,'nico','nico','nico@nico.com','4444','2019-05-11 06:08:27','2019-05-11 06:08:27');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;