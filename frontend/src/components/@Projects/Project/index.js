import React from 'react';
import { Redirect } from 'react-router-dom';

export default class Project extends React.Component {

    state = {
        toDetail: false
    }

    constructor() {
        super();

        this.goToDetail = this.goToDetail.bind(this);
    }

    goToDetail() {
        this.setState({
            toDetail: true
        });
    }

    render() {
        if (this.state.toDetail) {
            let going = '/proyek/' + this.props.activeLingkup + '/detail/' + this.props.project.pid;
            return <Redirect to={going}/>
        }
        else return (
            <div className='box-user p-2 m-1' onClick={this.goToDetail}>
                <div>
                    <div className='text-primary text-large font-weight-semibold'>{this.props.project.name}</div>
                    <div className='text-small'>PIC : {this.props.project.pic}</div>
                    <div className='text-small'>Tahap : {this.props.project.curr_phase_stage}</div>
                    <div className='text-small'>Deadline : {this.props.project.due_date}</div>

                    <div className='text-small mt-2 font-weight-semibold'>Terakhir diubah : <label className='text-accent font-weight-semibold'>{this.props.project.updated_at}</label></div>
                </div>
            </div>
        );
    }

}