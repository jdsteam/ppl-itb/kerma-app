import React from 'react';
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

import Page from '../Page';
import NavigationBar from '../Navbar';
import Project from './Project';

export default class Projects extends React.Component {

    state = {
        user : null,
        projects: null,
        currentProjects: [],
        toAddProject : false,
        currentPage: 0,
        totalPages: 0,
        status: 1,
        phase: 0,
        sort: 1,
        participate: 0,
        load: false
    }

    componentDidMount() {
        const cookies = new Cookies();
        let head = cookies.get('token');

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        const { id_lingkup } = this.props.match.params;

        let data = ['investasi', 'sosial', 'dagang', 'wisata'];

        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + data[id_lingkup - 1] +"?"
            + "status=" + this.state.status + "&"
            + "phase=" + this.state.phase + "&"
            + "sort=" + this.state.sort + "&"
            + "participate=" + this.state.participate;

        const getProjects = {
            method: 'GET',
            headers: header,
            url
        };
        axios(getProjects)
        .then((response) => {
            console.log(response);
            let curr = response.data.length;
            let current = [];
            if (curr.length > 5) {
                current = response.data;
            } else current = response.data.slice(0, 5);
            this.setState({
                projects: response.data,
                currentProjects: current
            });
        })
        .catch((error) => {
            console.log(error.response.data);
        });
    }

    componentDidUpdate() {
        if (this.state.load) {
            const cookies = new Cookies();
            let head = cookies.get('token');

            let header = {};
            header['Content-Type'] = 'application/json';
            header['x-access-token'] = head;

            const { id_lingkup } = this.props.match.params;

            let data = ['investasi', 'sosial', 'dagang', 'wisata'];

            let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + data[id_lingkup - 1] +"?"
                + "status=" + this.state.status + "&"
                + "phase=" + this.state.phase + "&"
                + "sort=" + this.state.sort + "&"
                + "participate=" + this.state.participate;

            const getProjects = {
                method: 'GET',
                headers: header,
                url
            };
            axios(getProjects)
            .then((response) => {
                console.log(response);
                let curr = response.data.length;
                let current = [];
                if (curr.length > 5) {
                    current = response.data;
                } else current = response.data.slice(0, 5);
                this.setState({
                    projects: response.data,
                    currentProjects: current,
                    load: false
                });
            })
            .catch((error) => {
                alert(error);
            });
        }
    }


    constructor () {
        super();

        const cookies = new Cookies();
        let uid = cookies.get('uid');
        let head = cookies.get('token');

        this.goToAddProject = this.goToAddProject.bind(this);
        this.onPageChanged = this.onPageChanged.bind(this);
        this.handleMyProject = this.handleMyProject.bind(this);
        this.handleProjectStatus = this.handleProjectStatus.bind(this);
        this.handleSortBy = this.handleSortBy.bind(this);
        this.handleTahapan = this.handleTahapan.bind(this);

        if(!uid) return;
        var url = process.env.REACT_APP_API_URL + `/api/v1/user/` + uid;

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        const getUser = {
            method: 'GET',
            headers: header,
            url
        };

        axios(getUser)
          .then((response) => {
            console.log(response);
            this.setState({
                user: response.data
            });
          })
          .catch((error) => {
              alert(error);
          });
  
    }

    handleMyProject() {
        let element = document.getElementById('my-project');
        let participate = element.checked ? 1 : 0;
        this.setState({
            participate: participate,
            load:true
        });
    }

    handleSortBy(event) {
        let element = event.nativeEvent.target.selectedIndex + 1;
        this.setState({
            sort: element,
            load:true
        });
    }

    handleTahapan(event) {
        let element = event.nativeEvent.target.selectedIndex;
        this.setState({
            phase: element,
            load:true
        });
    }

    handleProjectStatus(event) {
        let element = event.nativeEvent.target.selectedIndex + 1;
        this.setState({
            status: element,
            load:true
        });
    }

    goToAddProject() {
        this.setState({
            toAddProject: true
        });
    }

    onPageChanged(data) {
        const { projects } = this.state;
        const { currentPage, totalPages, pageLimit } = data;

        const offset = (currentPage - 1) * pageLimit;
        const currentProjects = projects.slice(offset, offset + pageLimit);

        this.setState({
            currentPage: currentPage,
            currentProjects: currentProjects,
            totalPages: totalPages
          })
      
    }

    loadProjects() {
        let result = [];

        this.state.currentProjects.map(project => {
            result.push(<Project activeLingkup={this.props.match.params.id_lingkup} project={project}></Project>);
        });

        return result;
    }

    render() {

        const cookies = new Cookies();
        const tokenCookie = cookies.get('token');

        if (!tokenCookie) return <Redirect to='/login' />
        else if (this.state.user == null || this.state.projects == null) return <div></div>
        else if (this.state.toAddProject) {
            let going = '/proyek/' + this.props.match.params.id_lingkup + "/add";
            return <Redirect to={going} />
        } 

        if (!this.state.user.roles.includes(1)
            && !this.state.user.roles.includes(2)
            && !this.state.user.roles.includes(3)
            && !this.state.user.roles.includes(4)) {
                return <Redirect to='/admin'/>
            }
        
        let isAdmin = this.state.user.roles.includes(5);

        return (
            <div>
                <NavigationBar activeLingkup={this.props.match.params.id_lingkup} isUser={true} isAdmin={isAdmin} roles={this.state.user.roles} active="Daftar Proyek"></NavigationBar>
                <div className='d-flex justify-content-center mt-3'>
                    <div className='d-flex m-4 three-quarter justify-content-between'>
                        <div className='text-accent font-weight-semibold text-xlarge'>Daftar Proyek</div>
                        <button className='btn add-user' onClick={this.goToAddProject}>Tambah Proyek</button>
                    </div>
                </div>

                <div className='d-flex justify-content-center'>
                    <div className='d-flex three-quarter'>
                        <div>
                            <div className='d-flex'>
                                <label className='mt-2 text-primary' for="sel1">Tahapan:</label>
                                <select onChange={this.handleTahapan} className="ml-2 form-control" id="sel1">
                                    <option>Semua</option>
                                    <option>Tahap Paparan</option>
                                    <option>Tahap Identifikasi Data/Potensi</option>
                                    <option>Tahap Penentuan Jenis/Model</option>
                                    <option>Tahap Penandatangan MOU/PKS</option>
                                    <option>Tahap Implementasi Kegiatan</option>
                                    <option>Tahap Monitoring dan Laporan</option>
                                    <option>Tahap Ekspansi / Peningkatan</option>
                                </select>
                            </div>
                            <div className='d-flex mt-2'>
                                <label className='mt-2 text-primary' for="sel3">Status:</label>
                                <select onChange={this.handleProjectStatus} className="ml-2 form-control" id="sel3">
                                    <option>Sedang Berjalan</option>
                                    <option>Selesai</option>
                                    <option>Batal</option>
                                    <option>Pending</option>
                                </select>
                            </div>
                            <div className='d-flex mt-2'>
                                <label className='mt-2 text-primary' for="sel2">Urutkan Berdasarkan:</label>
                                <select className="ml-2 form-control" onChange={this.handleSortBy} id="sel2">
                                    <option>Terbaru</option>
                                    <option>Terpasif</option>
                                    <option>Tahapan</option>
                                    <option>Nama Proyek</option>
                                </select>
                            </div>
                            <div className="checkbox mt-1">
                            <label for='my-project'><div className='text-primary'>Tampilkan proyek terlibat</div></label><input onChange={this.handleMyProject} className='ml-2' type="checkbox" id='my-project' value=""/>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='d-flex justify-content-center mt-3'>
                    <div className='three-quarter'>
                        {this.loadProjects()}
                    </div>
                </div>

                <div className='d-flex justify-content-center mt-3 mb-3'>
                    <Page participants={this.state.projects} onPageChanged={this.onPageChanged}></Page>
                </div>
            </div>
        );
    }
}