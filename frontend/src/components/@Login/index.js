import React from 'react';
import axios from 'axios';
import queryString from 'querystring';
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router-dom';

import './index.scss';
import logo from './../../assets/photo/logo.png';

const center_horizontal = "d-flex justify-content-center";

export default class Login extends React.Component {

    state = {
    redirect: false
    }

    constructor () {
        super();
        this.login = this.login.bind(this);
    }

    login() {

        var url = process.env.REACT_APP_API_URL + `/api/v1/login`;

        let username = document.getElementById('username').value;
        let password = document.getElementById('password').value;

        let body = {
            username: username,
            password: password
        };
      

        const logInOptions = {
            method: 'POST',
            data: queryString.stringify(body),
            url,
        };

        if (username.length == 0 || password.length == 0) {
            alert("Silahkan Isi form terlebih dahulu");
            return;
        }

        axios(logInOptions)
          .then((response) => {
            const cookies = new Cookies();
            cookies.set('token', response.data.token, { path: '/' });
            cookies.set('uid', response.data.uid, { path: '/' });
            this.setState({
                redirect: true
            });
          })
          .catch((error) => {
              alert(error.response.data.message);
          });
    }

    render() {
        const cookies = new Cookies();
        const tokenCookie = cookies.get('token');
        
        if (this.state.redirect || tokenCookie) return <Redirect to='/' />
        else return (
            <div className="full-height background-secondary">
                <div className="full-height d-flex justify-content-center align-items-center">
                    <div className='full-width'>
                        <div className={center_horizontal}>
                            <img src={logo} className="logo" alt="logo"/>
                        </div>
                        <div className={center_horizontal}>
                            <div className="mt-4 box-login p-2 half">
                                <div className='text-primary font-weight-semibold text-xlarge d-flex justify-content-center'>Selamat Datang!</div>
                                <div>
                                    <div className={center_horizontal}>
                                        <input required type="text" id="username" placeholder="Username" className="half box-input p-1 mt-4"/>
                                    </div>
                                    <div className={center_horizontal}>
                                        <input required type="password" id="password" placeholder="Password" className="half box-input p-1 mt-2"/>
                                    </div>
                                    <div className={center_horizontal}>
                                        <input type="submit" className="mt-4 btn btn-login" value="Masuk" onClick={this.login}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='center-horizontal'><a className='ml-4 mt-1 p-1' target='#blank' href='http://localhost:3001/observe'>
                            <input type="button" className="mt-2 btn btn-add" value='Lihat Data Proyek'/>
                        </a></div>
                    </div>
                </div>
            </div>
        );
    }
}