/* App.js */
import React from 'react';
import { Doughnut } from 'react-chartjs-2';


export default class DoughnutChart extends React.Component {	
	render() {
		let data = {
            labels: [
                'Proyek Selesai',
                'Proyek Baru',
                'Proyek Berlangsung',
                'Proyek Berhenti'
            ],
            datasets: [{
                data: this.props.dataProyek,
                backgroundColor: [
                '#9CCC65',
                '#36A2EB',
                '#FFCE56',
                '#ef5350'
                ],
                hoverBackgroundColor: [
                '#9CCC65',
                '#36A2EB',
                '#FFCE56',
                '#ef5350'
                ]
            }]
        };
		
		return (
		<div>
			<Doughnut data={data} 
                height={110}/>
		</div>
		);
	}
}