/* App.js */
import React from 'react';
import { Bar } from 'react-chartjs-2';


export default class ColumnChart extends React.Component {	
	render() {
		let data = {
						labels: ['Tahap 1', 'Tahap 2', 'Tahap 3', 'Tahap 4', 'Tahap 5', 'Tahap 6', 'Tahap 7'],
						datasets: [
						{
							label: 'Tahapan',
							backgroundColor: 'rgba(255,99,132,0.2)',
							borderColor: 'rgba(255,99,132,1)',
							borderWidth: 1,
							hoverBackgroundColor: 'rgba(255,99,132,0.4)',
							hoverBorderColor: 'rgba(255,99,132,1)',
							data: this.props.dataTahapan
						}
						]
					};
		
		return (
		<div>
			<Bar
				data={data}
				width={100}
				height={225	}
				options={{
					maintainAspectRatio: false,
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					}
				}}
			/>
		</div>
		);
	}
}