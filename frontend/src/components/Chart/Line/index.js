import React from 'react';
import {Line} from 'react-chartjs-2';
import './index.scss';

export default class LineChart extends React.Component {
    render() {

        let data = {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            datasets: [
              {
                label: this.props.chartName,
                fill: false,
                lineTension: 0.1,
                backgroundColor: this.props.baseColor,
                borderColor: this.props.baseColor,
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: this.props.baseColor,
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: this.props.baseColor,
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: this.props.dataPerMonth
              }
            ]
          };

        return (
            <div className='line-box'>
                < Line
                    data={data}
                    height={100}
                />
            </div>
        );
    }
}