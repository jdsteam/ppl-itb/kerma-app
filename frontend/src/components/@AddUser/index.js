import React from 'react';
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

import NavigationBar from '../Navbar';
import './index.scss';

export default class AddUser extends React.Component {
    state = {
        user : null,
        goBack: false
    }

    constructor () {
        super();

        this.addAccount = this.addAccount.bind(this);
        const cookies = new Cookies();
        let uid = cookies.get('uid');
        let head = cookies.get('token');

        if(!uid) return;
        var url = process.env.REACT_APP_API_URL + `/api/v1/user/` + uid;

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        const getUser = {
            method: 'GET',
            headers: header,
            url
        };

        axios(getUser)
          .then((response) => {
            console.log(response);
            this.setState({
                user: response.data
            });
          })
          .catch((error) => {
              alert(error);
          });

    }

    addAccount() {
        let name = document.getElementById('name');
        let username = document.getElementById('username');
        let email = document.getElementById('email');
        let password = document.getElementById('password');
        let roles = [];

        if (name.value.length === 0
            || username.value.length === 0
            || email.value.length === 0
            || password.value.length === 0) {
                alert("Tidak boleh kosong");
                return;
            }

        let ids = ['investasi_checkbox', 'sosial_checkbox', 'dagang_checkbox', 'wisata_checkbox', 'admin_checkbox', 'manajer_checkbox']
        for (let i = 1; i <= 6; i++) {
            let element = document.getElementById(ids[i - 1]);
            if (element.checked) roles.push(i);
        }

        if (roles.length === 0) {
            alert("Tidak boleh kosong");
            return;
        }

        let body = {
            name: name.value,
            username: username.value,
            email: email.value,
            password: password.value,
            roles: roles
        }
        
        const cookies = new Cookies();
        let head = cookies.get('token');

        var url = process.env.REACT_APP_API_URL + `/api/v1/user`;

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let addOptions  = {
            method: 'POST',
            headers: header,
            data: body,
            url
        };

        axios(addOptions)
          .then((response) => {
            console.log(response);
            this.setState({
                goBack: true
            });
          })
          .catch((error) => {
              alert(error);
          });
        
    }

    render() {
        const cookies = new Cookies();
        const tokenCookie = cookies.get('token');

        if (!tokenCookie) return <Redirect to='/login' />
        else if (this.state.user == null) return <div></div>
        else if (this.state.goBack) {
            let going = '/admin/' + this.props.match.params.id_lingkup;
            return <Redirect to={going} />
        }

        if (!this.state.user.roles.includes(5)) return <Redirect to='/'/>

        let isUser = false;
        if (this.state.user.roles.includes(1)
            || this.state.user.roles.includes(2)
            || this.state.user.roles.includes(3)
            || this.state.user.roles.includes(4)) {
                isUser = true;
            }

        return (
            <div>
                <NavigationBar isUser={isUser} isAdmin={true} activeLingkup={this.props.match.params.id_lingkup} roles={this.state.user.roles} active="Admin"></NavigationBar>
                <div className="full-height background-white">
                    <div className='text-accent font-weight-semibold text-xlarge center-horizontal mt-5'>Tambah Akun</div>
                    <form action='#'>
                        <div className='center-horizontal'>
                            <input required type="text" id="name" placeholder="Nama" className="half box-input p-1 mt-4"/>
                        </div>
                        <div className='center-horizontal'>
                            <input required type="text" id="username" placeholder="Username" className="half box-input p-1 mt-2"/>
                        </div>
                        <div className='center-horizontal'>
                            <input required type="email" id="email" placeholder="Email" className="half box-input p-1 mt-2"/>
                        </div>
                        <div className='center-horizontal'>
                            <input required type="text" id="password" placeholder="Password" className="half box-input p-1 mt-2"/>
                        </div>
                        <div className='d-flex justify-content-center mt-3 pl-4'>
                            <div className="half">
                                <div>
                                    <input className="form-check-input" type="checkbox" id="investasi_checkbox"/>
                                    <label className="form-check-label" htmlFor="investasi_checkbox">
                                        Investasi
                                    </label>
                                </div>
                                <div>
                                    <input className="form-check-input" type="checkbox" id="sosial_checkbox"/>
                                    <label className="form-check-label" htmlFor="sosial_checkbox">
                                        Program Sosial
                                    </label>
                                </div>
                                <div>
                                    <input className="form-check-input" type="checkbox" id="dagang_checkbox"/>
                                    <label className="form-check-label" htmlFor="dagang_checkbox">
                                        Komoditas dan Perdagangan
                                    </label>
                                </div>
                                <div>
                                    <input className="form-check-input" type="checkbox" id="wisata_checkbox"/>
                                    <label className="form-check-label" htmlFor="wisata_checkbox">
                                        Pengembangan Wisata
                                    </label>
                                </div>
                                <div>
                                    <input className="form-check-input" type="checkbox" id="admin_checkbox"/>
                                    <label className="form-check-label" htmlFor="admin_checkbox">
                                        Admin
                                    </label>
                                </div>
                                <div>
                                    <input className="form-check-input" type="checkbox" id="manajer_checkbox"/>
                                    <label className="form-check-label" htmlFor="manajer_checkbox">
                                        Manajer
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div className='center-horizontal'>
                        <input type="button" onClick={this.addAccount} className="mt-4 btn btn-add" value="Tambah"/>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}