import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem } from 'reactstrap';
  import Cookies from 'universal-cookie';
  import { Redirect } from 'react-router-dom';

import { ROUTE_ADMIN,ROUTE_DASHBOARD,ROUTE_PROYEK } from './../../routes';
import './index.scss';
import logo from './../../assets/photo/logo.png'; 
import logout from './../../assets/photo/logout.png';

export default class NavigationBar extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.logout = this.logout.bind(this);
    this.select = this.select.bind(this);
    this.state = {
      isOpen: false,
      toLogin: false,
      redirect: null,
      toDashboard: null
    };
  }

  logout() {
    const cookies = new Cookies();
    cookies.remove('token', { path: '/' });
    cookies.remove('uid', { path: '/' });
    this.setState({
      toLogin : true
    });
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  select(event) {
    let data = [
      'Investasi',
      'Program Sosial',
      'Komoditas dan Perdagangan',
      'Pengembangan Wisata'
    ];
    if ((data.indexOf(event.target.innerText) + 1) == this.props.activeLingkup) return;

    let first_project = '/dashboard/' + (data.indexOf(event.target.innerText) + 1);
    console.log(first_project);
    this.setState({
      toDashboard: first_project
    });
  } 

  handleMenu() {
        let menus = [];
        let first_project = '/proyek/' + this.props.activeLingkup;
        console.log(this.props.activeLingkup);
        if (this.props.isUser) {
          menus.push({title:'Rangkuman', to: '/dashboard/' + this.props.activeLingkup});
          menus.push({title:'Daftar Proyek', to: first_project});
        }
        if (this.props.isAdmin) {
          menus.push({title:'Admin', to: '/admin/' + this.props.activeLingkup});
        }

        let result = [];

        for (let i = 0; i < menus.length; i++) {
            let menu = menus[i];
            let componentClass = '';
            
            if (this.props.active === menu.title) componentClass += 'text-accent';
            else componentClass += 'text-white';

            let menuComponent = (
                <NavItem key={i}>
                    <NavLink className={componentClass} href={menu.to}>{menu.title}</NavLink>
                </NavItem>
            );

            result.push(menuComponent);
        }

        return result;
    }
  
  componentDidUpdate() {
    if (this.state.toDashboard != null) {
      this.setState({
        toDashboard: null
      });
    }
  }

  handleDropdown() {
    let items = [
      (<DropdownItem onClick={this.select}>Investasi</DropdownItem>),
      (<DropdownItem onClick={this.select}>Program Sosial</DropdownItem>),
      (<DropdownItem onClick={this.select}>Komoditas dan Perdagangan</DropdownItem>),
      (<DropdownItem onClick={this.select}>Pengembangan Wisata</DropdownItem>),
    ];

    let result = [];

    for (let i = 0; i < this.props.roles.length; i++) {
      result.push(items[this.props.roles[i] - 1]);
    }

    return result;
  }

  render() {
    if (this.state.toLogin) return <Redirect to='/login' />
    else if (this.state.toDashboard != null) {
      return <Redirect to={this.state.toDashboard} />
    }
    else return (
      <div>
        <Navbar className='navigation' light expand="md">
          <NavbarBrand href="/"><img src={ logo } className='img-navbar' alt='logo'/></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className='full-width' navbar>
              {this.handleMenu()}
              <div className='ml-auto d-flex'>
                <UncontrolledDropdown className='mr-2' nav inNavbar>
                  <DropdownToggle className='text-white' nav caret>
                  Lingkup
                  </DropdownToggle>
                  <DropdownMenu right>
                  {this.handleDropdown()}
                  </DropdownMenu>
                </UncontrolledDropdown>
                <img src={ logout } onClick={this.logout} className='logout-icon' alt='logout'/>
              </div>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}