import React from 'react';
import Stepper from 'react-stepper-horizontal';
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

import NavigationBar from '../Navbar';
import editLogo from './../../assets/photo/edit.png';
import rightArrow from './../../assets/photo/right_arrow.png';
import photosIcon from './../../assets/photo/photos.png';
import docs from './../../assets/photo/docs.png';
import binLogo from './../../assets/photo/bin.png';

import './index.scss';

export default class DetailProject extends React.Component {
    state = {
        user : null,
        toEdit: false,
        project: null,
        photos: null,
        documents: null,
        comments: null,
        todolist:null,
        flag: false,
        goBack: false,
        fixPhase: 0
    }

    componentDidMount() {
        const {id_lingkup, id_project } = this.props.match.params;
        const cookies = new Cookies();
        let uid = cookies.get('uid');
        let head = cookies.get('token');
        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let lingkup = data[id_lingkup - 1];

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + `/` + id_project;
        const getProject = {
            method: 'GET',
            headers: header,
            url
        }
        axios(getProject)
        .then((response) => {
            console.log(response);
            this.setState({
                project: response.data,
                fixPhase: response.data.curr_phase_stage,
                flag: true
            });
        })
        .catch((error) => {
            alert(error);
        });
    }

    loadAllData() {
        const {id_lingkup, id_project } = this.props.match.params;
        const { curr_phase_stage } = this.state.project;
        const cookies = new Cookies();
        let uid = cookies.get('uid');
        let head = cookies.get('token');
        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let lingkup = data[id_lingkup - 1];

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;
        let url = '';
        const getData = {
            method: 'GET',
            headers: header,
            url
        }

        this.loadTodosData(getData, lingkup, id_project, curr_phase_stage);
        this.loadCommentData(getData, lingkup, id_project, curr_phase_stage);
        this.loadPhotosData(getData, lingkup, id_project, curr_phase_stage);
        this.loadDocumentsData(getData, lingkup, id_project, curr_phase_stage);
    }

    loadTodosData(getData, lingkup, id_project, curr_phase_stage) {
        getData.url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + `/` + id_project + '/todos/' + curr_phase_stage;
        axios(getData)
        .then((response) => {
            console.log(response);
            this.setState({
                todolist: response.data,
                flag: false
            });
        })
        .catch((error) => {
            alert(error);
        });
    }

    loadCommentData(getData, lingkup, id_project, curr_phase_stage) {
        getData.url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + `/` + id_project + '/comments/' + curr_phase_stage;
        axios(getData)
        .then((response) => {
            console.log(response);
            this.setState({
                comments: response.data,
                flag: false
            });
        })
        .catch((error) => {
            alert(error);
        });
    }

    loadPhotosData(getData, lingkup, id_project, curr_phase_stage) {
        getData.url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + `/` + id_project + '/photos/' + curr_phase_stage;
        axios(getData)
        .then((response) => {
            console.log(response);
            this.setState({
                photos: response.data,
                flag: false
            });
        })
        .catch((error) => {
            alert(error);
        });
    }

    loadDocumentsData(getData, lingkup, id_project, curr_phase_stage) {
        getData.url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + `/` + id_project + '/documents/' + curr_phase_stage;
        axios(getData)
        .then((response) => {
            console.log(response);
            this.setState({
                documents: response.data,
                flag: false
            });
        })
        .catch((error) => {
            alert(error);
        });
    }

    constructor () {
        super();

        const cookies = new Cookies();
        let uid = cookies.get('uid');
        let head = cookies.get('token');

        this.goToEdit = this.goToEdit.bind(this);
        this.loadPhotos = this.loadPhotos.bind(this);
        this.loadDocuments = this.loadDocuments.bind(this);
        this.loadComments = this.loadComments.bind(this);
        this.loadToDoList = this.loadToDoList.bind(this);
        this.deletePhoto = this.deletePhoto.bind(this);
        this.deleteDocument = this.deleteDocument.bind(this);
        this.addTodos = this.addTodos.bind(this);
        this.addPhoto = this.addPhoto.bind(this);
        this.addDocument = this.addDocument.bind(this);
        this.addComment = this.addComment.bind(this);
        this.nextPhase = this.nextPhase.bind(this);
        this.changePhase = this.changePhase.bind(this);

        if(!uid) return;
        var url = process.env.REACT_APP_API_URL + `/api/v1/user/` + uid;

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        const getUser = {
            method: 'GET',
            headers: header,
            url
        };

        axios(getUser)
          .then((response) => {
            console.log(response);
            this.setState({
                user: response.data
            });
          })
          .catch((error) => {
              alert(error);
          });

    }

    goToEdit() {
        this.setState({
            toEdit: true
        });
    }

    loadPhotos() {
        let result = [];

        let {photos} = this.state;
        let canEdit = this.state.project.status === 1;

        let btn;

        for (let i = 0; i < photos.length; i++) {
            let photo = photos[i];
            if (canEdit) btn = (<button onClick={() => this.deletePhoto(photo.id)} className='btn btn-danger text-small mt-4 mb-2'>Hapus</button>);
            result.push(
                <div className='half p-2'>
                    <div className='box-user p-1 center-horizontal'>
                        <img className='icon-detail mt-2' src={photosIcon} />
                        <a target='#blank' href={photo.photos}><div></div>{photo.name}</a>
                        <div>
                            {btn}
                        </div>
                    </div>
                </div>
            );
            btn = '';
        }

        return result;
    }

    deletePhoto(id) {
        const {id_lingkup, id_project } = this.props.match.params;
        const { curr_phase_stage } = this.state.project;
        const cookies = new Cookies();
        let head = cookies.get('token');
        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let lingkup = data[id_lingkup - 1];

        let body = {
            pid: id_project,
            photo_id: id
        }

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + '/photos';
        const deleteData = {
            method: 'DELETE',
            headers: header,
            data:body,
            url
        }
        axios(deleteData)
        .then((response) => {
            console.log(response);
            const getData = {
                method: 'GET',
                headers: header,
                url
            }
            this.loadPhotosData(getData, lingkup, id_project, curr_phase_stage);
        })
        .catch((error) => {
            alert(error);
        });
    }

    loadDocuments() {
        let result = [];

        let {documents} = this.state;
        let canEdit = this.state.project.status === 1;

        let btn;

        for (let i = 0; i < documents.length; i++) {
            let document = documents[i];
            if (canEdit) btn = (<button onClick={() => this.deleteDocument(document.id)} className='btn btn-danger text-small mt-4 mb-2'>Hapus</button>);
            result.push(
                <div className='half p-2'>
                    <div className='box-user p-1 center-horizontal'>
                        <img className='icon-detail mt-2' src={docs} />
                        <a target='#blank' href={document.document}><div></div>{document.name}</a>
                        <div>
                            {btn}
                        </div>
                    </div>
                </div>
            );
            btn = '';
        }

        return result;
    }

    deleteDocument(id) {
        const {id_lingkup, id_project } = this.props.match.params;
        const { curr_phase_stage } = this.state.project;
        const cookies = new Cookies();
        let head = cookies.get('token');
        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let lingkup = data[id_lingkup - 1];

        let body = {
            pid: id_project,
            document_id: id
        }

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + '/documents';
        const deleteData = {
            method: 'DELETE',
            headers: header,
            data:body,
            url
        }
        axios(deleteData)
        .then((response) => {
            console.log(response);
            const getData = {
                method: 'GET',
                headers: header,
                url
            }
            this.loadDocumentsData(getData, lingkup, id_project, curr_phase_stage);
        })
        .catch((error) => {
            alert(error);
        });
    }

    loadComments() {
        let result = [];

        let {comments} = this.state;

        for (let i = 0; i < comments.length; i++) {
            let comment = comments[i];
            result.push(
                <div className='half p-1'>
                    <div className='box-user p-1'>
                        {/* <div className='text-primary font-weight-semibold text-medium mb-1'>faizzuns</div> */}
                        <div className='text-small'>{comment.comment}</div>
                    </div>
                </div>
            );
        }

        return result;
    }

    loadToDoList() {
        let result = [];

        let {todolist} = this.state;
        let canEdit = this.state.project.status === 1;

        for (let i = 0; i < todolist.length; i++) {
            let item = todolist[i];
            item.checked = item.is_checked === 1;
            let btn;
            let input = (<input className="form-check-input" onChange={() => this.updateTodo(item.id)} defaultChecked={item.checked} type="checkbox" id={item.id} disabled/>);
            if (canEdit) {
                btn = (<button onClick={() => this.deleteToDoList(item.id)} className='btn btn-hapus mt-1'><img src={binLogo} className='bin-icon-small'/></button>);
                input = (<input className="form-check-input" onChange={() => this.updateTodo(item.id)} defaultChecked={item.checked} type="checkbox" id={item.id}/>);
            }
            result.push(
                <div className='mb-3 d-flex'>
                    <div className='mr-4 ml-1'>
                        {btn}
                    </div>
                    
                    <label className="form-check-label ml-2 mt-2" htmlFor={item.id}>
                        {input}
                        {item.description}
                    </label>
                    
                </div>
            );
        }

        return result;
    }

    updateTodo(id) {
        let element = document.getElementById(id);
        element.disabled = true;
        
        const {id_lingkup, id_project } = this.props.match.params;
        const { curr_phase_stage } = this.state.project;
        const cookies = new Cookies();
        let head = cookies.get('token');
        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let lingkup = data[id_lingkup - 1];

        let body = {
            pid: id_project,
            todo_id: id
        }

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + '/todos/toggle';
        const updateTodos = {
            method: 'PATCH',
            headers: header,
            data:body,
            url
        }
        axios(updateTodos)
        .then((response) => {
            console.log(response);
            element.disabled = false;
        })
        .catch((error) => {
            element.disabled = false;
            element.checked = !element.checked;
            alert(error);
        });
    }

    deleteToDoList(id) {
        const {id_lingkup, id_project } = this.props.match.params;
        const { curr_phase_stage } = this.state.project;
        const cookies = new Cookies();
        let head = cookies.get('token');
        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let lingkup = data[id_lingkup - 1];

        let body = {
            pid: id_project,
            todo_id: id
        }

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + '/todos';
        const deleteData = {
            method: 'DELETE',
            headers: header,
            data:body,
            url
        }
        axios(deleteData)
        .then((response) => {
            console.log(response);
            const getData = {
                method: 'GET',
                headers: header,
                url
            }
            this.loadTodosData(getData, lingkup, id_project, curr_phase_stage);
        })
        .catch((error) => {
            alert(error);
        });
    }

    addTodos() {
        let element = document.getElementById('todos').value;
        if (element.length === 0) return;

        const {id_lingkup, id_project } = this.props.match.params;
        const { curr_phase_stage } = this.state.project;
        const cookies = new Cookies();
        let head = cookies.get('token');
        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let lingkup = data[id_lingkup - 1];

        let body = {
            pid: id_project,
            phase: curr_phase_stage,
            todo: element
        }

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + '/todos';
        const sendData = {
            method: 'POST',
            headers: header,
            data:body,
            url
        }
        axios(sendData)
        .then((response) => {
            console.log(response);
            const getData = {
                method: 'GET',
                headers: header,
                url
            }
            this.loadTodosData(getData, lingkup, id_project, curr_phase_stage);
        })
        .catch((error) => {
            alert(error);
        });
    }

    addComment() {
        let element = document.getElementById('komentar').value;
        if (element.length === 0) return;

        const {id_lingkup, id_project } = this.props.match.params;
        const { curr_phase_stage } = this.state.project;
        const cookies = new Cookies();
        let head = cookies.get('token');
        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let lingkup = data[id_lingkup - 1];

        let body = {
            pid: id_project,
            phase: curr_phase_stage,
            comment: element
        }

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + '/comments';
        const sendData = {
            method: 'POST',
            headers: header,
            data:body,
            url
        }
        axios(sendData)
        .then((response) => {
            console.log(response);
            const getData = {
                method: 'GET',
                headers: header,
                url
            }
            this.loadCommentData(getData, lingkup, id_project, curr_phase_stage);
        })
        .catch((error) => {
            alert(error);
        });
    }

    addPhoto() {
        let element = document.getElementById('photos_name').value;
        let link = document.getElementById('photos_link').value;
        if (element.length === 0 || link.length === 0) return;

        const {id_lingkup, id_project } = this.props.match.params;
        const { curr_phase_stage } = this.state.project;
        const cookies = new Cookies();
        let head = cookies.get('token');
        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let lingkup = data[id_lingkup - 1];

        let body = {
            pid: id_project,
            phase: curr_phase_stage,
            name: element,
            photo: link
        }

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + '/photos';
        const sendData = {
            method: 'POST',
            headers: header,
            data:body,
            url
        }
        axios(sendData)
        .then((response) => {
            console.log(response);
            const getData = {
                method: 'GET',
                headers: header,
                url
            }
            this.loadPhotosData(getData, lingkup, id_project, curr_phase_stage);
        })
        .catch((error) => {
            alert(error);
        });
    }

    addDocument() {
        let element = document.getElementById('document_name').value;
        let link = document.getElementById('document_link').value;
        if (element.length === 0 || link.length === 0) return;

        const {id_lingkup, id_project } = this.props.match.params;
        const { curr_phase_stage } = this.state.project;
        const cookies = new Cookies();
        let head = cookies.get('token');
        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let lingkup = data[id_lingkup - 1];

        let body = {
            pid: id_project,
            phase: curr_phase_stage,
            name: element,
            document: link
        }

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + '/documents';
        const sendData = {
            method: 'POST',
            headers: header,
            data:body,
            url
        }
        axios(sendData)
        .then((response) => {
            console.log(response);
            const getData = {
                method: 'GET',
                headers: header,
                url
            }
            this.loadDocumentsData(getData, lingkup, id_project, curr_phase_stage);
        })
        .catch((error) => {
            alert(error);
        });
    }

    nextPhase() {
        const {id_lingkup, id_project } = this.props.match.params;
        const { curr_phase_stage } = this.state.project;
        const cookies = new Cookies();
        let head = cookies.get('token');
        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let lingkup = data[id_lingkup - 1];

        let body = { pid: id_project }
        
        let extension = '';
        let goBack = false;
        if (curr_phase_stage < 7) {
            extension = '/phase';
            body.new_phase = curr_phase_stage + 1;
        } else {
            extension = '/status';
            body.new_status = 2;
            goBack = true;
        }

        let pr = this.state.project;
        pr.curr_phase_stage = this.state.fixPhase + 1;

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + extension;
        const nextPhase = {
            method: 'PATCH',
            headers: header,
            data:body,
            url
        }
        axios(nextPhase)
        .then((response) => {
            console.log(response);
            this.setState({
                project: pr,
                fixPhase: pr.curr_phase_stage,
                photos: null,
                documents: null,
                comments: null,
                todolist:null,
                flag: true,
                goBack: goBack
            });
        })
        .catch((error) => {
            alert(error);
        });
    }
    
    changePhase(i) {
        let phaseChoosen = this.state.project.curr_phase_stage + i;
        if (phaseChoosen === 0
            || phaseChoosen === 8
            || phaseChoosen > this.state.fixPhase) {
                alert('Kamu tidak bisa melihat fase tersebut');
            } else {
                let newProject = this.state.project;
                newProject.curr_phase_stage = phaseChoosen;
                this.setState({
                    project: newProject,
                    photos: null,
                    documents: null,
                    comments: null,
                    todolist:null,
                    flag: true
                })
            }
    }

    render() {
        const cookies = new Cookies();
        const tokenCookie = cookies.get('token');

        if (!tokenCookie) return <Redirect to='/login' />
        else if (this.state.user == null
                || this.state.project == null) return <div></div>
        else if (this.state.goBack) {
            let going = '/proyek/' + this.props.match.params.id_lingkup;
            return <Redirect to={going}/>
        }
        else if (this.state.toEdit) {
            let {id_lingkup, id_project} = this.props.match.params;
            let going = '/proyek/' + id_lingkup + '/edit/' + id_project;
            return <Redirect to={going}/>
        }

        if (this.state.flag) this.loadAllData();
        if (this.state.todolist == null
            || this.state.comments == null
            || this.state.photos == null
            || this.state.documents == null) {
                return <div></div>;
            }

        if (!this.state.user.roles.includes(1)
            && !this.state.user.roles.includes(2)
            && !this.state.user.roles.includes(3)
            && !this.state.user.roles.includes(4)) {
                return <Redirect to='/admin'/>
            }
        
        let isAdmin = this.state.user.roles.includes(5);
        let { project } = this.state;
        let tahapan = [
            'Paparan',
            'Identifikasi Data/Potensi',
            'Penentuan Jenis/Model',
            'Penandatangan MOU/PKS',
            'Implementasi Kegiatan',
            'Monitoring dan Laporan',
            'Ekspansi / Peningkatan'
        ];
        let key = project.curr_phase_stage;
        let dueDate = project.phase_deadline[key].split(" ")[0];

        let next = this.state.project.curr_phase_stage < 7 ? 'Ke Tahap Selanjutnya' : 'Selesaikan Proyek';

        let buttons = [,,,,];
        let completed = '';
        if (project.status === 1) buttons = [
            (<button onClick={this.nextPhase} className='btn btn-info'>{ next }<img src={rightArrow} className='edit-icon ml-2'/></button>),
            (<button onClick={this.addTodos} className="mt-2 mb-3 btn btn-info text-small">Tambah Todolist</button>),
            (<button onClick={this.addComment} className="mb-3 btn btn-info mb-5">Tambah</button>),
            (<button onClick={this.addDocument} className="mt-2 mb-3 btn btn-info text-small">Tambah Dokumen</button>),
            (<button onClick={this.addPhoto} className="mt-2 mb-3 btn btn-info text-small">Tambah Foto</button>)
        ];

        if ((this.state.fixPhase !== 0 && this.state.fixPhase !== project.curr_phase_stage) || project.status === 2) completed = '(Completed)';

        return (
            <div>
                <NavigationBar activeLingkup={this.props.match.params.id_lingkup} isUser={true} isAdmin={isAdmin} roles={this.state.user.roles} active="Daftar Proyek"></NavigationBar>
                <div className='d-flex justify-content-center mt-3'>
                    <div className='three-quarter'>
                        <div className='text-accent center-horizontal text-xlarge font-weight-semibold'>{project.name}</div>
                        <div className='center-horizontal mt-2'>
                            <button className='btn btn-add' onClick={this.goToEdit}>Edit Proyek<img src={editLogo} className='edit-icon ml-2'/></button>
                        </div>
                        <div className='center-horizontal mt-1'>
                            {buttons[0]}
                        </div>
                        <div className='center-horizontal mt-5 mb-5'>
                            <div className='text-primary text-large'>Tahapan {project.curr_phase_stage}</div>
                            <Stepper 
                                steps={ [{title: ''}, {title: ''}, {title: ''}, {title: ''},{title: ''}, {title: ''}, {title: ''}] } 
                                activeStep={ this.state.fixPhase - 1 }
                                activeColor='#ffad33'/>
                            <div className='d-flex justify-content-between'>
                                <button onClick={() => this.changePhase(-1)} className="mt-2 mb-3 btn btn-info text-small ml-4">Prev</button>
                                <button onClick={() => this.changePhase(1)} className="mt-2 mb-3 btn btn-info text-small mr-4">Next</button>
                            </div>
                            <div className='mt-1 text-large text-secondary'>Tahap {tahapan[project.curr_phase_stage - 1]} {completed}</div>
                            <div className='mt-1 text-primary'>Deadline : <label className='font-weight-bold'>{dueDate}</label></div>
                        </div>

                        <div className='text-primary text-large mt-4'>To do list</div>
                        <div className='divider'></div>
                        <div className='box-user pl-1 pt-1 pb-1 pr-2 mt-1'>
                            {this.loadToDoList()}   
                        </div>
                        <input required type="text" id="todos" placeholder='Tambahkan Todolist Anda . . .' className="half box-input p-1 mr-1 mt-1"/>
                        <div>{buttons[1]}</div>

                        <div className='text-primary text-large mt-4'>Komentar</div>
                        <div className='divider'></div>
                        <div>
                            {this.loadComments()}
                        </div>
                        <textarea required id="komentar" className="three-quarter box-input p-1 mt-3" placeholder='Komentar Anda . . .'/>
                        <div>{buttons[2]}</div>

                        <div className='text-primary text-large mt-5'>Dokumen</div>
                        <div className='divider'></div>
                        <div className='mt-1'>
                            <input required type="text" id="document_name" placeholder='Nama File' className="half box-input p-1 mr-1"/>
                            <input required type="text" id="document_link" placeholder='Link Google Drive' className="half box-input p-1 mr-1 mt-1"/>
                            <div>{buttons[3]}</div>
                        </div>
                        <div className='d-flex flex-wrap full-width mb-5'>
                            {this.loadDocuments()}
                        </div>

                        <div className='text-primary text-large mt-4'>Foto</div>
                        <div className='divider mb-1'></div>
                        <div className='mt-1'>
                            <input required type="text" id="photos_name" placeholder='Nama File' className="half box-input p-1 mr-1"/>
                            <input required type="text" id="photos_link" placeholder='Link Google Drive' className="half box-input p-1 mr-1 mt-1"/>
                            <div>{buttons[4]}</div>
                        </div>
                        <div className='d-flex flex-wrap full-width mb-5'>
                            {this.loadPhotos()}
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}