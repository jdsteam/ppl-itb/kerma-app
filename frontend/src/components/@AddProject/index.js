import React from 'react';
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

import NavigationBar from '../Navbar';
import './index.scss';

export default class AddProject extends React.Component {
    state = {
        user : null,
        changeLingkup: -1,
        goBack: false
    }

    componentDidUpdate() {
        if (this.state.changeLingkup !== -1) {
            this.setState({
                changeLingkup: -1
            });
        }
    }

    constructor () {
        super();

        const cookies = new Cookies();
        let uid = cookies.get('uid');
        let head = cookies.get('token');

        this.handleLingkup = this.handleLingkup.bind(this);
        this.addProject = this.addProject.bind(this);

        if(!uid) return;
        var url = process.env.REACT_APP_API_URL + `/api/v1/user/` + uid;

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        const getUser = {
            method: 'GET',
            headers: header,
            url
        };

        axios(getUser)
          .then((response) => {
            console.log(response);
            this.setState({
                user: response.data
            });
          })
          .catch((error) => {
              alert(error);
          });
    }

    handleLingkup(event) {
        let data = ['Investasi', 'Program Sosial', 'Komoditas dan Perdagangan', 'Pengembangan Wisata'];
        let element = data.indexOf(event.nativeEvent.target.value) + 1;
        
        this.setState({
            changeLingkup: element
        });
    }

    valuevalidation(text) {
        return text.length !== 0;
    }

    addProject() {
        const cookies = new Cookies();
        let uid = cookies.get('uid');
        let head = cookies.get('token');
        let { id_lingkup } = this.props.match.params;

        let projectName = document.getElementById('name').value;
        let desc = document.getElementById('desc').value;
        let pic = uid;
        let dueDate1 = document.getElementById('tahap_1').value;
        let dueDate2 = document.getElementById('tahap_2').value;
        let dueDate3 = document.getElementById('tahap_3').value;
        let dueDate4 = document.getElementById('tahap_4').value;
        let dueDate5 = document.getElementById('tahap_5').value;
        let dueDate6 = document.getElementById('tahap_6').value;
        let dueDate7 = document.getElementById('tahap_7').value;
        let companyName = document.getElementById('name_mitra').value;
        let companyAddress = document.getElementById('addr').value;
        let cpName = document.getElementById('cp_name').value;
        let cpEmail = document.getElementById('cp_email').value;
        let cpPhone = document.getElementById('cp_phone').value;
        let country = document.getElementById('company_country').value;

        if (
            this.valuevalidation(projectName)
            && this.valuevalidation(desc)
            && this.valuevalidation(dueDate1)
            && this.valuevalidation(dueDate2)
            && this.valuevalidation(dueDate3)
            && this.valuevalidation(dueDate3)
            && this.valuevalidation(dueDate4)
            && this.valuevalidation(dueDate5)
            && this.valuevalidation(dueDate6)
            && this.valuevalidation(dueDate7)
            && this.valuevalidation(companyName)
            && this.valuevalidation(companyAddress)
            && this.valuevalidation(cpName)
            && this.valuevalidation(cpEmail)
            && this.valuevalidation(cpPhone)
            && this.valuevalidation(country)
        ) {
            let body = {
                name: projectName,
                pic_name: this.state.user.name,
                description: desc,
                due_date1: dueDate1,
                due_date2: dueDate2,
                due_date3: dueDate3,
                due_date4: dueDate4,
                due_date5: dueDate5,
                due_date6: dueDate6,
                due_date7: dueDate7,
                cp_name: cpName,
                company_address: companyAddress,
                cp_email: cpEmail,
                cp_phone: cpPhone,
                company_name: companyName,
                country: country,
                perangkat: []
            }

            let data = ['investasi', 'sosial', 'dagang', 'wisata'];

            let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + data[id_lingkup - 1];

            let header = {};
            header['Content-Type'] = 'application/json';
            header['x-access-token'] = head;

            const addProject = {
                method: 'POST',
                headers: header,
                data: body,
                url
            };
            console.log(addProject);

            axios(addProject)
            .then((response) => {
                console.log(response);
                this.setState({
                    goBack: true
                });
            })
            .catch((error) => {
                alert(error);
            });

        }
        else alert("Isi terlebih dahulu");

    }

    render() {
        const cookies = new Cookies();
        const tokenCookie = cookies.get('token');

        if (!tokenCookie) return <Redirect to='/login' />
        else if (this.state.user == null) return <div></div>
        else if (this.state.goBack) {
            let going = '/proyek/' + this.props.match.params.id_lingkup;
            return <Redirect to={going} />
        }
        else if (this.state.changeLingkup !== -1) {
            let going = '/proyek/' + this.state.changeLingkup + '/add';
            return <Redirect to={going}/>
        }

        if (!this.state.user.roles.includes(1)
            && !this.state.user.roles.includes(2)
            && !this.state.user.roles.includes(3)
            && !this.state.user.roles.includes(4)) {
                return <Redirect to='/admin'/>
            }
        
        let isAdmin = this.state.user.roles.includes(5);
        let lingkup = this.props.match.params.id_lingkup;

        let lingkupOption = [];
        let data = ['Investasi', 'Program Sosial', 'Komoditas dan Perdagangan', 'Pengembangan Wisata'];

        for (let i = 1; i <=4; i++) {
            if (this.state.user.roles.includes(i)){
                if (i == lingkup) {
                    lingkupOption.push(<option selected='true'>{data[i - 1]}</option>)
                }
                else lingkupOption.push(<option>{data[i - 1]}</option>)
            }
        }


        return (
            <div>
                <NavigationBar activeLingkup={this.props.match.params.id_lingkup} isUser={true} isAdmin={isAdmin} roles={this.state.user.roles} active="Daftar Proyek"></NavigationBar>
                {this.loadCountry()};
                <form>
                    <div className='d-flex justify-content-center mt-3'>
                        <div className='three-quarter'>
                            <div className='text-accent font-weight-semibold text-xlarge'>Tambah Proyek</div>
                            <select className="ml-2 form-control" onChange={this.handleLingkup} id="sel1">
                                {lingkupOption}
                            </select>
                            
                            <div className='d-flex justify-content-center text-accent text-large mt-4'>Detail Proyek</div>
                            <div className='divider'></div>

                            <div>
                                <div className='text-primary text-small font-weight-semibold mt-2'>Nama Proyek</div>
                                <input required type="text" id="name" className="half box-input p-1"/>
                            </div>

                            <div>
                                <div className='text-primary text-small font-weight-semibold mt-2'>Deskripsi</div>
                                <textarea required id="desc" className="half box-input description-height p-1"/>
                            </div>

                            <div>
                                <div className='text-primary text-small font-weight-semibold mt-2'>Deadline</div>
                                <div><label className='mr-2 tahap-width'>Tahap 1 :</label><input required type="date" id="tahap_1" className="box-input p-1"/></div>
                                <div><label className='mr-2 tahap-width'>Tahap 2 :</label><input required type="date" id="tahap_2" className="box-input p-1"/></div>
                                <div><label className='mr-2 tahap-width'>Tahap 3 :</label><input required type="date" id="tahap_3" className="box-input p-1"/></div>
                                <div><label className='mr-2 tahap-width'>Tahap 4 :</label><input required type="date" id="tahap_4" className="box-input p-1"/></div>
                                <div><label className='mr-2 tahap-width'>Tahap 5 :</label><input required type="date" id="tahap_5" className="box-input p-1"/></div>
                                <div><label className='mr-2 tahap-width'>Tahap 6 :</label><input required type="date" id="tahap_6" className="box-input p-1"/></div>
                                <div><label className='mr-2 tahap-width'>Tahap 7 :</label><input required type="date" id="tahap_7" className="box-input p-1"/></div>
                                <div className='mt-2'>
                                    <div>Tahap 1 : Paparan</div>
                                    <div>Tahap 2 : Identifikasi Data/Potensi</div>
                                    <div>Tahap 3 : Penentuan Jenis/Model</div>
                                    <div>Tahap 4 : Penandatangan MOU/PKS</div>
                                    <div>Tahap 5 : Implementasi Kegiatan</div>
                                    <div>Tahap 6 : Monitoring dan Laporan</div>
                                    <div>Tahap 7 : Ekspansi / Peningkatan</div>
                                </div>
                            </div>

                            <div className='d-flex justify-content-center text-accent text-large mt-4'>Mitra Kerjasama</div>
                            <div className='divider'></div>

                            <div>
                                <div className='text-primary text-small font-weight-semibold mt-2'>Nama Mitra</div>
                                <input required type="text" id="name_mitra" className="half box-input p-1"/>
                            </div>

                            <div>
                                <div className='text-primary text-small font-weight-semibold mt-2'>Alamat</div>
                                <textarea required id="addr" className="half box-input p-1"/><br/>
                                <input required type="text" id="company_country" list='list_country' placeholder='Asal Negara' className="half box-input p-1 mt-1"/>
                            </div>

                            <div>
                                <div className='text-primary text-small font-weight-semibold mt-2'>Kontak</div>
                                <input required type="text" id="cp_name" placeholder='Name' className="half box-input p-1 mr-1"/>
                                <input required type="text" id="cp_email" placeholder='Email' className="half box-input p-1 mr-1 mt-1"/>
                                <input required type="text" id="cp_phone" placeholder='Phone' className="half box-input p-1 mt-1"/>
                            </div>

                            <div className='center-horizontal'>
                                <input type="button" onClick={this.addProject} className="m-4 btn btn-add" value="Tambah Proyek"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }

    loadCountry() {
        return (
            <datalist id='list_country'>
                <option value="Afganistan"/>>
                <option value="Afrika Selatan"/>
                <option value="Afrika Tengah"/>
                <option value="Albania"/>
                <option value="Aljazair"/>
                <option value="Amerika Serikat"/>
                <option value="Andorra"/>
                <option value="Angola"/>
                <option value="Antigua dan Barbuda"/>
                <option value="Arab Saudi"/>
                <option value="Argentina"/>
                <option value="Armenia"/>
                <option value="Australia"/>
                <option value="Austria"/>
                <option value="Azerbaijan"/>
                <option value="Bahama"/>
                <option value="Bahrain"/>
                <option value="Bangladesh"/>
                <option value="Barbados"/>
                <option value="Belanda"/>
                <option value="Belarus"/>
                <option value="Belgia"/>
                <option value="Belize"/>
                <option value="Benin"/>
                <option value="Bhutan"/>
                <option value="Bolivia"/>
                <option value="Bosnia dan Herzegovina"/>
                <option value="Botswana"/>
                <option value="Brasil"/>
                <option value="Britania Raya"/>
                <option value="Brunei Darussalam"/>
                <option value="Bulgaria"/>
                <option value="Burkina Faso"/>
                <option value="Burundi"/>
                <option value="Ceko"/>
                <option value="Chad"/>
                <option value="Chili"/>
                <option value="China"/>
                <option value="Denmark"/>
                <option value="Djibouti"/>
                <option value="Dominika"/>
                <option value="Ekuador"/>
                <option value="El Salvador"/>
                <option value="Eritrea"/>
                <option value="Estonia"/>
                <option value="Ethiopia"/>
                <option value="Fiji"/>
                <option value="Filipina"/>
                <option value="Finlandia"/>
                <option value="Gabon"/>
                <option value="Gambia"/>
                <option value="Georgia"/>
                <option value="Ghana"/>
                <option value="Grenada"/>
                <option value="Guatemala"/>
                <option value="Guinea"/>
                <option value="Guinea Bissau"/>
                <option value="Guinea Khatulistiwa"/>
                <option value="Guyana"/>
                <option value="Haiti"/>
                <option value="Honduras"/>
                <option value="Hongaria"/>
                <option value="India"/>
                <option value="Indonesia"/>
                <option value="Irak"/>
                <option value="Iran"/>
                <option value="Irlandia"/>
                <option value="Islandia"/>
                <option value="Israel"/>
                <option value="Italia"/>
                <option value="Jamaika"/>
                <option value="Jepang"/>
                <option value="Jerman"/>
                <option value="Kamboja"/>
                <option value="Kamerun"/>
                <option value="Kanada"/>
                <option value="Kazakhstan"/>
                <option value="Kenya"/>
                <option value="Kirgizstan"/>
                <option value="Kiribati"/>
                <option value="Kolombia"/>
                <option value="Komoro"/>
                <option value="Republik Kongo"/>
                <option value="Korea Selatan"/>
                <option value="Korea Utara"/>
                <option value="Kosta Rika"/>
                <option value="Kroasia"/>
                <option value="Kuba"/>
                <option value="Kuwait"/>
                <option value="Laos"/>
                <option value="Latvia"/>
                <option value="Lebanon"/>
                <option value="Lesotho"/>
                <option value="Liberia"/>
                <option value="Libya"/>
                <option value="Liechtenstein"/>
                <option value="Lituania"/>
                <option value="Luksemburg"/>
                <option value="Madagaskar"/>
                <option value="Makedonia"/>
                <option value="Maladewa"/>
                <option value="Malawi"/>
                <option value="Malaysia"/>
                <option value="Mali"/>
                <option value="Malta"/>
                <option value="Maroko"/>
                <option value="Marshall"/>
                <option value="Mauritania"/>
                <option value="Mauritius"/>
                <option value="Meksiko"/>
                <option value="Mesir"/>
                <option value="Mikronesia"/>
                <option value="Moldova"/>
                <option value="Monako"/>
                <option value="Mongolia"/>
                <option value="Montenegro"/>
                <option value="Mozambik"/>
                <option value="Myanmar"/>
                <option value="Namibia"/>
                <option value="Nauru"/>
                <option value="Nepal"/>
                <option value="Niger"/>
                <option value="Nigeria"/>
                <option value="Nikaragua"/>
                <option value="Norwegia"/>
                <option value="Oman"/>
                <option value="Pakistan"/>
                <option value="Palau"/>
                <option value="Panama"/>
                <option value="Pantai Gading"/>
                <option value="Papua Nugini"/>
                <option value="Paraguay"/>
                <option value="Perancis"/>
                <option value="Peru"/>
                <option value="Polandia"/>
                <option value="Portugal"/>
                <option value="Qatar"/>
                <option value="Republik Demokratik Kongo"/>
                <option value="Republik Dominika"/>
                <option value="Rumania"/>
                <option value="Rusia"/>
                <option value="Rwanda"/>
                <option value="Saint Kitts and Nevis"/>
                <option value="Saint Lucia"/>
                <option value="Saint Vincent and the Grenadines"/>
                <option value="Samoa"/>
                <option value="San Marino"/>
                <option value="Sao Tome and Principe"/>
                <option value="Selandia Baru"/>
                <option value="Senegal"/>
                <option value="Serbia"/>
                <option value="Seychelles"/>
                <option value="Sierra Leone"/>
                <option value="Singapura"/>
                <option value="Siprus"/>
                <option value="Slovenia"/>
                <option value="Slowakia"/>
                <option value="Solomon"/>
                <option value="Somalia"/>
                <option value="Spanyol"/>
                <option value="Sri Lanka"/>
                <option value="Sudan"/>
                <option value="Sudan Selatan"/>
                <option value="Suriah"/>
                <option value="Suriname"/>
                <option value="Swaziland"/>
                <option value="Swedia"/>
                <option value="Swiss"/>
                <option value="Tajikistan"/>
                <option value="Tanjung Verde"/>
                <option value="Tanzania"/>
                <option value="Thailand"/>
                <option value="Timor Leste"/>
                <option value="Togo"/>
                <option value="Tonga"/>
                <option value="Trinidad and Tobago"/>
                <option value="Tunisia"/>
                <option value="Turki"/>
                <option value="Turkmenistan"/>
                <option value="Tuvalu"/>
                <option value="Uganda"/>
                <option value="Ukraina"/>
                <option value="Uni Emirat Arab"/>
                <option value="Uruguay"/>
                <option value="Uzbekistan"/>
                <option value="Vanuatu"/>
                <option value="Venezuela"/>
                <option value="Vietnam"/>
                <option value="Yaman"/>
                <option value="Yordania"/>
                <option value="Yunani"/>
                <option value="Zambia"/>
                <option value="Zimbabwe"/>
            </datalist>
        );
    }
}