import React from 'react';
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import DoughnutChart from '../Chart/Doughnut';

export default class Observer extends React.Component {

    state = {
        data: null
    }

    constructor() {
        super();
        let url = process.env.REACT_APP_API_URL + '/api/v1/dashboard/observer';

        const getObserve = {
            method: 'GET',
            url
        };

        axios(getObserve)
        .then((response) => {
            console.log(response);
            this.setState({
                data: response.data
            });
        })
        .catch((error) => {
            alert(error);
        });

    }

    render() {
        if (this.state.data == null) return <div></div>
        let data = [undefined, this.state.data.ongoing, this.state.data.new, undefined];
        return (
            <div className='mt-5'>
                <DoughnutChart
                    dataProyek={data}>
                </DoughnutChart>
                <div>
                    <div className='m-2'>Data diatas hanya memperlihatkan Proyek Baru dan Proyek yang sedang berjalan</div>
                    <div className='m-2'>Proyek sedang berlangsung : {this.state.data.ongoing}</div>
                    <div className='m-2'>Proyek Baru : {this.state.data.new}</div>
                </div>
            </div>
        )
    }
}