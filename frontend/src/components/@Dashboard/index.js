import React from 'react';
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

import NavigationBar from '../Navbar';
import LineChart from '../Chart/Line';
import ColumnChart from '../Chart/Column';
import DoughnutChart from '../Chart/Doughnut';
import './index.scss';

export default class Dashboard extends React.Component {

    state = {
        user : null,
        monthlyPhase: null,
        monthlyStatus: null,
        annuallyCompleted: null,
        annuallyNew: null,
        annuallyOnGoing: null,
        annuallyCancelled: null
    }

    loadMonthlyPhases() {
        const cookies = new Cookies();
        const { id_lingkup } = this.props.match.params;
        let head = cookies.get('token');

        if (!id_lingkup) return;

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let url = process.env.REACT_APP_API_URL + `/api/v1/dashboard/` + data[id_lingkup - 1] + `/monthly/phases`;
        const getPhases = {
            method: 'GET',
            headers: header,
            url
        };

        axios(getPhases)
        .then((response) => {
        console.log(response);
        this.setState({
            monthlyPhase: response.data
        });
        })
        .catch((error) => {
            alert(error);
        });
    }

    loadMonthlyStatus() {
        const cookies = new Cookies();
        const { id_lingkup } = this.props.match.params;
        let head = cookies.get('token');

        if (!id_lingkup) return;

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let url = process.env.REACT_APP_API_URL + `/api/v1/dashboard/` + data[id_lingkup - 1] + `/monthly/status`;
        const getStatus = {
            method: 'GET',
            headers: header,
            url
        };

        axios(getStatus)
        .then((response) => {
        console.log(response);
        this.setState({
            monthlyStatus: response.data
        });
        })
        .catch((error) => {
            alert(error);
        });
    }

    loadAnnuallyCompleted() {
        const cookies = new Cookies();
        const { id_lingkup } = this.props.match.params;
        let head = cookies.get('token');

        if (!id_lingkup) return;

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let url = process.env.REACT_APP_API_URL + `/api/v1/dashboard/` + data[id_lingkup - 1] + `/annually/status/completed`;
        const getStatus = {
            method: 'GET',
            headers: header,
            url
        };

        axios(getStatus)
        .then((response) => {
        console.log(response);
        this.setState({
            annuallyCompleted: response.data
        });
        })
        .catch((error) => {
            alert(error);
        });
    }

    loadAnnuallyNew() {
        const cookies = new Cookies();
        const { id_lingkup } = this.props.match.params;
        let head = cookies.get('token');

        if (!id_lingkup) return;

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let url = process.env.REACT_APP_API_URL + `/api/v1/dashboard/` + data[id_lingkup - 1] + `/annually/status/new`;
        const getStatus = {
            method: 'GET',
            headers: header,
            url
        };

        axios(getStatus)
        .then((response) => {
        console.log(response);
        this.setState({
            annuallyNew: response.data
        });
        })
        .catch((error) => {
            alert(error);
        });
    }

    loadAnnuallyOnGoing() {
        const cookies = new Cookies();
        const { id_lingkup } = this.props.match.params;
        let head = cookies.get('token');

        if (!id_lingkup) return;

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let url = process.env.REACT_APP_API_URL + `/api/v1/dashboard/` + data[id_lingkup - 1] + `/annually/status/ongoing`;
        const getStatus = {
            method: 'GET',
            headers: header,
            url
        };

        axios(getStatus)
        .then((response) => {
        console.log(response);
        this.setState({
            annuallyOnGoing: response.data
        });
        })
        .catch((error) => {
            alert(error);
        });
    }

    loadAnnuallyCancelled() {
        const cookies = new Cookies();
        const { id_lingkup } = this.props.match.params;
        let head = cookies.get('token');

        if (!id_lingkup) return;

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let url = process.env.REACT_APP_API_URL + `/api/v1/dashboard/` + data[id_lingkup - 1] + `/annually/status/cancelled`;
        const getStatus = {
            method: 'GET',
            headers: header,
            url
        };

        axios(getStatus)
        .then((response) => {
        console.log(response);
        this.setState({
            annuallyCancelled: response.data
        });
        })
        .catch((error) => {
            alert(error);
        });
    }

    constructor () {
        super();

        const cookies = new Cookies();
        let uid = cookies.get('uid');
        let head = cookies.get('token');

        this.onDownload = this.onDownload.bind(this);

        if(!uid) return;
        var url = process.env.REACT_APP_API_URL + `/api/v1/user/` + uid;

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        const getUser = {
            method: 'GET',
            headers: header,
            url
        };

        axios(getUser)
          .then((response) => {
            console.log(response);
            this.setState({
                user: response.data
            });
          })
          .catch((error) => {
              alert(error);
          });
    }

    onDownload() {
        const cookies = new Cookies();
        let head = cookies.get('token');

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let url = process.env.REACT_APP_API_URL + `/api/v1/data/countrycsv`;

        const getExcel = {
            method: 'GET',
            headers: header,
            url
        };

        axios(getExcel)
          .then((response) => {
            console.log(response);
          })
          .catch((error) => {
              alert(error);
          });
    }

    render() {

        const cookies = new Cookies();
        const tokenCookie = cookies.get('token');

        if (!tokenCookie) return <Redirect to='/login' />
        else if (this.state.user == null) return <div></div>

        const { id_lingkup } = this.props.match.params;
        if (!id_lingkup) {
            let going = '/dashboard/' + this.state.user.roles[0];
            return <Redirect to={going} />
        }

        if (this.state.monthlyPhase == null) {
            this.loadMonthlyPhases();
            return <div></div>
        }
        else if (this.state.monthlyStatus == null) {
            this.loadMonthlyStatus();
            return <div></div>
        }
        else if (this.state.annuallyCompleted == null) {
            this.loadAnnuallyCompleted();
            return <div></div>
        }
        else if (this.state.annuallyNew == null) {
            this.loadAnnuallyNew();
            return <div></div>
        }
        else if (this.state.annuallyOnGoing == null) {
            this.loadAnnuallyOnGoing();
            return <div></div>
        }
        else if (this.state.annuallyCancelled == null) {
            this.loadAnnuallyCancelled();
            return <div></div>
        }

        if (!this.state.user.roles.includes(1)
            && !this.state.user.roles.includes(2)
            && !this.state.user.roles.includes(3)
            && !this.state.user.roles.includes(4)) {
                return <Redirect to='/admin'/>
            }
        
        let isAdmin = this.state.user.roles.includes(5);

        const {annuallyCompleted, annuallyNew, annuallyOnGoing, annuallyCancelled} = this.state;

        let goTo = this.state.user.roles[0];
        if (this.props.match.params.id_lingkup) goTo = this.props.match.params.id_lingkup;

        return (
            <div>
                <NavigationBar activeLingkup={goTo} isUser={true} isAdmin={isAdmin} roles={this.state.user.roles} active="Rangkuman"></NavigationBar>
                <div><a className='ml-4 mt-4 p-1' target='#blank' href={process.env.REACT_APP_API_URL + '/api/v1/data/countrycsv'}>
                    <input type="button" className="mt-2 btn btn-info" value='Download Proyek/Negara'/>
                </a></div>
                <div><a className='ml-4 mt-1 p-1' target='#blank' href='http://localhost:3001/observe'>
                    <input type="button" className="mt-2 btn btn-add" value='Lihat Data Proyek'/>
                </a></div>
                <div className='text-center text-primary text-xlarge'>Rangkuman Bulan ini</div>
                <div className='box-dashboard'>
                    <ColumnChart
                        dataTahapan={this.state.monthlyPhase.project_count}>
                    </ColumnChart>
                    <div className='ml-4'>
                        <div>Tahap 1 : Paparan</div>
                        <div>Tahap 2 : Identifikasi Data/Potensi</div>
                        <div>Tahap 3 : Penentuan Jenis/Model</div>
                        <div>Tahap 4 : Penandatangan MOU/PKS</div>
                        <div>Tahap 5 : Implementasi Kegiatan</div>
                        <div>Tahap 6 : Monitoring dan Laporan</div>
                        <div>Tahap 7 : Ekspansi / Peningkatan</div>
                    </div>
                    <div className='mt-3'></div>
                    <DoughnutChart
                        dataProyek={this.state.monthlyStatus.project_count}>
                    </DoughnutChart>
                </div>
                <div className='text-center text-primary text-xlarge mt-5'>Rangkuman Keseluruhan</div>
                <div className='box-dashboard'>
                    <div className='text-center text-accent mt-4'>Proyek Selesai</div>
                    <LineChart 
                        dataPerMonth={annuallyCompleted.project_count} 
                        chartName='Proyek Selesai'
                        baseColor='rgba(75,192,192,1)'>
                    </LineChart>

                    <div className='text-center text-accent mt-2'>Proyek Baru</div>
                    <LineChart
                        dataPerMonth={annuallyNew.project_count}
                        chartName='Proyek Baru'
                        baseColor='rgba(192,75,192,1)'>
                    </LineChart>

                    <div className='text-center text-accent mt-2'>Proyek Berlanjut</div>
                    <LineChart
                        dataPerMonth={annuallyOnGoing.project_count}
                        chartName='Proyek Berlanjut'
                        baseColor='rgba(192,192,75,1)'>
                    </LineChart>

                    <div className='text-center text-accent mt-2'>Proyek Berhenti</div>
                    <LineChart
                        dataPerMonth={annuallyCancelled.project_count}
                        chartName='Proyek Berhenti'
                        baseColor='rgba(60,80,75,1)'>
                    </LineChart>
                </div>
            </div>
        );
    }
}