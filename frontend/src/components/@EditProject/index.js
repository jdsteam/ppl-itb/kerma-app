import React from 'react';
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

import NavigationBar from '../Navbar';
import binLogo from './../../assets/photo/bin.png';

import './index.scss';

export default class EditProject extends React.Component {

    state = {
        user : null,
        participates: null,
        project: null,
        goBack: false,
        deleted: false
    }

    componentDidMount() {
        const { id_lingkup, id_project } = this.props.match.params;
        const cookies = new Cookies();
        let head = cookies.get('token');

        let data = ['investasi', 'sosial', 'dagang', 'wisata'];

        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + data[id_lingkup - 1] + `/` + id_project + '/edit';

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        const getProject = {
            method: 'GET',
            headers: header,
            url
        };

        axios(getProject)
        .then((response) => {
            console.log(response);
            this.setState({
                project: response.data
            });
        })
        .catch((error) => {
            alert(error);
        });

        url = process.env.REACT_APP_API_URL + `/api/v1/project/` + data[id_lingkup - 1] + `/` + id_project + '/participants';
        const getParticipants = {
            method: 'GET',
            headers: header,
            url
        };
        axios(getParticipants)
        .then((response) => {
            console.log(response);
            this.setState({
                participates: response.data
            });
        })
        .catch((error) => {
            alert(error);
        });
    }

    constructor () {
        super();

        const cookies = new Cookies();
        let uid = cookies.get('uid');
        let head = cookies.get('token');

        this.edit = this.edit.bind(this);
        this.addUser = this.addUser.bind(this);
        this.deleteProject = this.deleteProject.bind(this);
        this.cancelProject = this.cancelProject.bind(this);
        this.changePending = this.changePending.bind(this);

        if(!uid) return;
        var url = process.env.REACT_APP_API_URL + `/api/v1/user/` + uid;

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        const getUser = {
            method: 'GET',
            headers: header,
            url
        };

        axios(getUser)
          .then((response) => {
            console.log(response);
            this.setState({
                user: response.data
            });
          })
          .catch((error) => {
              alert(error);
          });

    }

    valuevalidation(text) {
        return text !== 0;
    }

    deleteUser(id) {
        const { id_lingkup, id_project } = this.props.match.params;
        const cookies = new Cookies();
        let head = cookies.get('token');

        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + data[id_lingkup - 1] + '/participant';

        let body = {
            pid: this.props.match.params.id_project,
            uid: id
        };

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        const deleteUser = {
            method: 'DELETE',
            headers: header,
            data: body,
            url
        };

        axios(deleteUser)
        .then((response) => {
            console.log(response);
            url = process.env.REACT_APP_API_URL + `/api/v1/project/` + data[id_lingkup - 1] + `/` + id_project + '/participants';
            const getParticipants = {
                method: 'GET',
                headers: header,
                url
            };
            axios(getParticipants)
            .then((response) => {
                console.log(response);
                this.setState({
                    participates: response.data
                });
            })
            .catch((error) => {
                alert(error);
            });
            
        })
        .catch((error) => {
            alert(error);
        });
    }

    loadUsers() {
        let result = [];

        const cookies = new Cookies();
        let uid = cookies.get('uid');

        this.state.participates.map(user => {

            let deleteTag = user.uid != uid ?
            (
                <div className='mr-2 ml-2'>
                    <button onClick={() => this.deleteUser(user.uid)} className='btn btn-hapus mt-1 mb-1'><img src={binLogo} className='bin-icon'/></button>
                </div>
            ) : <div></div>;

            let tagUser = (
                <div className='box-user mb-1 pl-1'>
                    <div className='d-flex justify-content-between'>
                        <div>
                            <div className='text-large font-weight-semibold text-primary'>{user.name}</div>
                            <div className='text-small font-weight-semibold text-primary'>{user.username}</div>
                        </div>
                        {deleteTag}
                    </div>
                </div>
            );
            result.push(tagUser);
        });

        return result;
    }

    edit() {
        const cookies = new Cookies();
        let uid = cookies.get('uid');
        let head = cookies.get('token');
        let { id_lingkup, id_project } = this.props.match.params;

        let projectName = document.getElementById('name').value;
        let desc = document.getElementById('desc').value;
        let pic = uid;
        let dueDate1 = document.getElementById('tahap_1').value;
        let dueDate2 = document.getElementById('tahap_2').value;
        let dueDate3 = document.getElementById('tahap_3').value;
        let dueDate4 = document.getElementById('tahap_4').value;
        let dueDate5 = document.getElementById('tahap_5').value;
        let dueDate6 = document.getElementById('tahap_6').value;
        let dueDate7 = document.getElementById('tahap_7').value;
        let companyName = document.getElementById('name_mitra').value;
        let companyAddress = document.getElementById('addr').value;
        let cpName = document.getElementById('cp_name').value;
        let cpEmail = document.getElementById('cp_email').value;
        let cpPhone = document.getElementById('cp_phone').value;
        let country = document.getElementById('company_country').value;
        let updatedAt = document.getElementById('updated_at').value;

        if (
            this.valuevalidation(projectName)
            && this.valuevalidation(desc)
            && this.valuevalidation(dueDate1)
            && this.valuevalidation(dueDate2)
            && this.valuevalidation(dueDate3)
            && this.valuevalidation(dueDate3)
            && this.valuevalidation(dueDate4)
            && this.valuevalidation(dueDate5)
            && this.valuevalidation(dueDate6)
            && this.valuevalidation(dueDate7)
            && this.valuevalidation(companyName)
            && this.valuevalidation(companyAddress)
            && this.valuevalidation(cpName)
            && this.valuevalidation(cpEmail)
            && this.valuevalidation(cpPhone)
            && this.valuevalidation(country)
            && this.valuevalidation(updatedAt)
        ) {
            let perangkat = [];
            for (let x = 0; x < this.state.project.list_perangkat.length; x++) {
                let checkElement = document.getElementById(x);
                if (checkElement.checked) perangkat.push(x + 1);
            }
            console.log(perangkat);
            let body = {
                name: projectName,
                pic_name: this.state.project.pic,
                description: desc,
                due_date1: dueDate1,
                due_date2: dueDate2,
                due_date3: dueDate3,
                due_date4: dueDate4,
                due_date5: dueDate5,
                due_date6: dueDate6,
                due_date7: dueDate7,
                cp_name: cpName,
                company_address: companyAddress,
                cp_email: cpEmail,
                cp_phone: cpPhone,
                company_name: companyName,
                pid: id_project,
                country: country,
                latest_update: updatedAt,
                perangkat:perangkat
            }

            let data = ['investasi', 'sosial', 'dagang', 'wisata'];

            let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + data[id_lingkup - 1] + '/edit';

            let header = {};
            header['Content-Type'] = 'application/json';
            header['x-access-token'] = head;

            const addProject = {
                method: 'PATCH',
                headers: header,
                data: body,
                url
            };

            console.log(addProject);

            axios(addProject)
            .then((response) => {
                alert('SAVED');
            })
            .catch((error) => {
                alert(error);
            });

        }
        else alert("Isi terlebih dahulu");
    }

    cancelProject() {
        const {id_lingkup, id_project } = this.props.match.params;
        const cookies = new Cookies();
        let head = cookies.get('token');
        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let lingkup = data[id_lingkup - 1];

        let body = { pid: parseInt(id_project) , new_status: 3}
        
        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + '/status';
        const nextPhase = {
            method: 'PATCH',
            headers: header,
            data:body,
            url
        }
        console.log(nextPhase);
        axios(nextPhase)
        .then((response) => {
            console.log(response);
            this.setState({
                deleted: true
            })
        })
        .catch((error) => {
            console.log(error.message);
        });
    }

    addUser() {
        let element = document.getElementById('add_user').value;
        if (element.length !== 0) {
            const { id_lingkup, id_project } = this.props.match.params;
            const cookies = new Cookies();
            let head = cookies.get('token');

            let data = ['investasi', 'sosial', 'dagang', 'wisata'];
            let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + data[id_lingkup - 1] + '/participant';

            let body = {
                pid: this.props.match.params.id_project,
                participants: [element]
            };

            let header = {};
            header['Content-Type'] = 'application/json';
            header['x-access-token'] = head;

            const addUser = {
                method: 'POST',
                headers: header,
                data: body,
                url
            };

            console.log(addUser);

            axios(addUser)
            .then((response) => {
                console.log(response);
                url = process.env.REACT_APP_API_URL + `/api/v1/project/` + data[id_lingkup - 1] + `/` + id_project + '/participants';
                const getParticipants = {
                    method: 'GET',
                    headers: header,
                    url
                };
                axios(getParticipants)
                .then((response) => {
                    console.log(response);
                    this.setState({
                        participates: response.data
                    });
                })
                .catch((error) => {
                    alert(error);
                });
                
            })
            .catch((error) => {
                alert(error);
            });
        } else alert('Isi terlebi dahulu');
    }

    deleteProject() {
        const { id_lingkup, id_project } = this.props.match.params;
        const cookies = new Cookies();
        let head = cookies.get('token');

        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + data[id_lingkup - 1];

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let body = {pid: id_project};

        const deleteProject = {
            method: 'DELETE',
            headers: header,
            data: body,
            url
        };

        axios(deleteProject)
          .then((response) => {
            this.setState({
                deleted: true
            });
          })
          .catch((error) => {
              alert(error);
          });
    }

    loadODP(start, count) {
        let { project } = this.state;

        let result = [];

        let curr = project.list_perangkat.slice(start, count);
        for (let i = 0 + start; i < curr.length + start; i++) {
            let checked = project.perangkat.includes(i+1);
            let elmt = (
                <div>
                    <input className="form-check-input" defaultChecked={checked} type="checkbox" id={i}/>
                    <label className="form-check-label" htmlFor={i}>
                        {project.list_perangkat[i]}
                    </label>
                </div>
            );
            result.push(elmt);
        }

        return result;
    }

    changePending(i) {
        const {id_lingkup, id_project } = this.props.match.params;
        const cookies = new Cookies();
        let head = cookies.get('token');
        let data = ['investasi', 'sosial', 'dagang', 'wisata'];
        let lingkup = data[id_lingkup - 1];

        let body = { pid: parseInt(id_project) , new_status: i}
        
        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        let url = process.env.REACT_APP_API_URL + `/api/v1/project/` + lingkup + '/status';
        const nextPhase = {
            method: 'PATCH',
            headers: header,
            data:body,
            url
        }
        console.log(nextPhase);
        axios(nextPhase)
        .then((response) => {
            console.log(response);
            this.setState({
                deleted: true
            })
        })
        .catch((error) => {
            console.log(error.message);
        });
    }

    render() {
        const cookies = new Cookies();
        const tokenCookie = cookies.get('token');

        if (!tokenCookie) return <Redirect to='/login' />
        else if (this.state.deleted) {
            let going = '/proyek/' + this.props.match.params.id_lingkup;
            return <Redirect to={going} />
        }
        else if (this.state.user == null || this.state.project == null || this.state.participates == null) return <div></div>
        else if (this.state.goBack) {
            let going = '/proyek/' + this.props.match.params.id_lingkup + '/detail/' + this.props.match.params.id_project;
            return <Redirect to={going} />
        }

        if (!this.state.user.roles.includes(1)
            && !this.state.user.roles.includes(2)
            && !this.state.user.roles.includes(3)
            && !this.state.user.roles.includes(4)) {
                return <Redirect to='/admin'/>
            }
        
        let isAdmin = this.state.user.roles.includes(5);
        
        const { project } = this.state;
        let dueDate = [];

        for (let key in project.phase_deadline) dueDate.push(project.phase_deadline[key].split(" ")[0]);
        let updateAt = project.updated_at.split("T")[0];
        
        let button = ['','',''];
        if (project.status === 1) {
            button = [
                (<input type="button" className="mt-4 btn btn-add quarter" onClick={this.edit} defaultValue="Simpan"/>),
                (<input type="button" onClick={this.cancelProject} className="mt-2 btn btn-info quarter" value='Cancel Project'/>),
                (<input type="button" className="ml-2 btn btn-info" onClick={this.addUser} value="Tambah Anggota"/>),
                (<input type="button" onClick={() => this.changePending(4)} className="mt-2 btn btn-add quarter" value='Pending Project'/>)
            ];
        } else if (project.status === 4) {
            button.push((<input type="button" onClick={() => this.changePending(1)} className="mt-2 btn btn-add quarter" value='Unpending'/>));
        } else button.push('');

        return (
            <div>
                <NavigationBar activeLingkup={this.props.match.params.id_lingkup} isUser={true} isAdmin={isAdmin} roles={this.state.user.roles} active="Daftar Proyek"></NavigationBar>
                {this.loadCountry()}
                <form>
                    <div className='d-flex justify-content-center mt-3'>
                        <div className='three-quarter'>
                            <div className='text-accent font-weight-semibold text-xlarge'>Edit Proyek</div>
                            
                            <div className='d-flex justify-content-center text-accent text-large mt-4'>Detail Proyek</div>
                            <div className='divider'></div>

                            <div>
                                <div className='text-primary text-small font-weight-semibold mt-2'>Nama Proyek</div>
                                <input required type="text" id="name" defaultValue={project.name} className="half box-input p-1"/>
                            </div>

                            <div>
                                <div className='text-primary text-small font-weight-semibold mt-2'>Deskripsi</div>
                                <textarea required id="desc" defaultValue={project.description} className="half box-input description-height p-1"/>
                            </div>

                            <div>
                                <div className='text-primary text-small font-weight-semibold mt-2'>Deadline</div>
                                <div><label className='mr-2 tahap-width'>Tahap 1 :</label><input defaultValue={dueDate[0]} required type="date" id="tahap_1" className="box-input p-1"/></div>
                                <div><label className='mr-2 tahap-width'>Tahap 2 :</label><input defaultValue={dueDate[1]} required type="date" id="tahap_2" className="box-input p-1"/></div>
                                <div><label className='mr-2 tahap-width'>Tahap 3 :</label><input defaultValue={dueDate[2]} required type="date" id="tahap_3" className="box-input p-1"/></div>
                                <div><label className='mr-2 tahap-width'>Tahap 4 :</label><input defaultValue={dueDate[3]} required type="date" id="tahap_4" className="box-input p-1"/></div>
                                <div><label className='mr-2 tahap-width'>Tahap 5 :</label><input defaultValue={dueDate[4]} required type="date" id="tahap_5" className="box-input p-1"/></div>
                                <div><label className='mr-2 tahap-width'>Tahap 6 :</label><input defaultValue={dueDate[5]} required type="date" id="tahap_6" className="box-input p-1"/></div>
                                <div><label className='mr-2 tahap-width'>Tahap 7 :</label><input defaultValue={dueDate[6]} required type="date" id="tahap_7" className="box-input p-1"/></div>
                                <div className='mt-2'>
                                    <div>Tahap 1 : Paparan</div>
                                    <div>Tahap 2 : Identifikasi Data/Potensi</div>
                                    <div>Tahap 3 : Penentuan Jenis/Model</div>
                                    <div>Tahap 4 : Penandatangan MOU/PKS</div>
                                    <div>Tahap 5 : Implementasi Kegiatan</div>
                                    <div>Tahap 6 : Monitoring dan Laporan</div>
                                    <div>Tahap 7 : Ekspansi / Peningkatan</div>
                                </div>
                            </div>

                            <div>
                                <div className='text-primary text-small font-weight-semibold mt-5'>Waktu Terakhir Proyek Diubah</div>
                                <input required type="date" id="updated_at" defaultValue={updateAt} className="box-input p-1"/>
                            </div>

                            <div className='d-flex justify-content-center text-accent text-large mt-4'>Mitra Kerjasama</div>
                            <div className='divider'></div>

                            <div>
                                <div className='text-primary text-small font-weight-semibold mt-2'>Nama Mitra</div>
                                <input required type="text" defaultValue={project.company_name} id="name_mitra" className="half box-input p-1"/>
                            </div>

                            <div>
                                <div className='text-primary text-small font-weight-semibold mt-2'>Alamat</div>
                                <textarea required id="addr" defaultValue={project.company_address} className="half box-input p-1"/><br/>
                                <input required type="text" id="company_country" list='list_country' defaultValue={project.country} placeholder='Asal Negara' className="half box-input p-1 mt-1"/>
                            </div>

                            <div>
                                <div className='text-primary text-small font-weight-semibold mt-2'>Kontak</div>
                                <input required type="text" id="cp_name" defaultValue={project.cp_name} placeholder='Name' className="half box-input p-1 mr-1"/>
                                <input required type="text" id="cp_email" defaultValue={project.cp_email} placeholder='Email' className="half box-input p-1 mr-1 mt-1"/>
                                <input required type="text" id="cp_phone" defaultValue={project.cp_phone} placeholder='Phone' className="half box-input p-1 mt-1"/>
                            </div>

                            <div className='d-flex justify-content-center text-accent text-large mt-4'>ODP Terkait</div>
                            <div className='divider'></div>

                            <div className='d-flex flex-wrap pt-2'>
                                <div id='group1' className='half'>
                                    {this.loadODP(0,22)}
                                </div>
                                <div id='group2' className='half'>
                                    {this.loadODP(22,44)}
                                </div>
                            </div>

                            <div className='center-horizontal mt-4'>
                                {button[0]}
                            </div> 
                            <div className='center-horizontal'>
                                {button[3]}
                            </div> 
                            <div className='center-horizontal'>
                                {button[1]}
                            </div> 
                            <div className='center-horizontal'>
                                <input type="button" onClick={this.deleteProject} className="mt-2 mb-5 btn btn-hapus quarter" value='Hapus'/>
                            </div>
                        </div>
                    </div>
                </form>
                <form>
                    <div className='d-flex justify-content-center mt-5'>
                        <div className='three-quarter mt-5'>
                            <div className='d-flex justify-content-center text-accent text-large mt-4'>Anggota Tim Proyek</div>
                            <div className='divider'></div>

                            <div className='mt-3 mb-4 d-flex'>
                                <input required type="text" id="add_user" placeholder='Username' className="half box-input p-1"/>
                                {button[2]}
                            </div>
                        </div>
                    </div>
                </form>
                <div className='d-flex justify-content-center'>
                    <div className='three-quarter mb-5'>
                        {this.loadUsers()}
                    </div>
                </div>
            </div>
        );
    }

    loadCountry() {
        return (
            <datalist id='list_country'>
                <option value="Afganistan"/>>
                <option value="Afrika Selatan"/>
                <option value="Afrika Tengah"/>
                <option value="Albania"/>
                <option value="Aljazair"/>
                <option value="Amerika Serikat"/>
                <option value="Andorra"/>
                <option value="Angola"/>
                <option value="Antigua dan Barbuda"/>
                <option value="Arab Saudi"/>
                <option value="Argentina"/>
                <option value="Armenia"/>
                <option value="Australia"/>
                <option value="Austria"/>
                <option value="Azerbaijan"/>
                <option value="Bahama"/>
                <option value="Bahrain"/>
                <option value="Bangladesh"/>
                <option value="Barbados"/>
                <option value="Belanda"/>
                <option value="Belarus"/>
                <option value="Belgia"/>
                <option value="Belize"/>
                <option value="Benin"/>
                <option value="Bhutan"/>
                <option value="Bolivia"/>
                <option value="Bosnia dan Herzegovina"/>
                <option value="Botswana"/>
                <option value="Brasil"/>
                <option value="Britania Raya"/>
                <option value="Brunei Darussalam"/>
                <option value="Bulgaria"/>
                <option value="Burkina Faso"/>
                <option value="Burundi"/>
                <option value="Ceko"/>
                <option value="Chad"/>
                <option value="Chili"/>
                <option value="China"/>
                <option value="Denmark"/>
                <option value="Djibouti"/>
                <option value="Dominika"/>
                <option value="Ekuador"/>
                <option value="El Salvador"/>
                <option value="Eritrea"/>
                <option value="Estonia"/>
                <option value="Ethiopia"/>
                <option value="Fiji"/>
                <option value="Filipina"/>
                <option value="Finlandia"/>
                <option value="Gabon"/>
                <option value="Gambia"/>
                <option value="Georgia"/>
                <option value="Ghana"/>
                <option value="Grenada"/>
                <option value="Guatemala"/>
                <option value="Guinea"/>
                <option value="Guinea Bissau"/>
                <option value="Guinea Khatulistiwa"/>
                <option value="Guyana"/>
                <option value="Haiti"/>
                <option value="Honduras"/>
                <option value="Hongaria"/>
                <option value="India"/>
                <option value="Indonesia"/>
                <option value="Irak"/>
                <option value="Iran"/>
                <option value="Irlandia"/>
                <option value="Islandia"/>
                <option value="Israel"/>
                <option value="Italia"/>
                <option value="Jamaika"/>
                <option value="Jepang"/>
                <option value="Jerman"/>
                <option value="Kamboja"/>
                <option value="Kamerun"/>
                <option value="Kanada"/>
                <option value="Kazakhstan"/>
                <option value="Kenya"/>
                <option value="Kirgizstan"/>
                <option value="Kiribati"/>
                <option value="Kolombia"/>
                <option value="Komoro"/>
                <option value="Republik Kongo"/>
                <option value="Korea Selatan"/>
                <option value="Korea Utara"/>
                <option value="Kosta Rika"/>
                <option value="Kroasia"/>
                <option value="Kuba"/>
                <option value="Kuwait"/>
                <option value="Laos"/>
                <option value="Latvia"/>
                <option value="Lebanon"/>
                <option value="Lesotho"/>
                <option value="Liberia"/>
                <option value="Libya"/>
                <option value="Liechtenstein"/>
                <option value="Lituania"/>
                <option value="Luksemburg"/>
                <option value="Madagaskar"/>
                <option value="Makedonia"/>
                <option value="Maladewa"/>
                <option value="Malawi"/>
                <option value="Malaysia"/>
                <option value="Mali"/>
                <option value="Malta"/>
                <option value="Maroko"/>
                <option value="Marshall"/>
                <option value="Mauritania"/>
                <option value="Mauritius"/>
                <option value="Meksiko"/>
                <option value="Mesir"/>
                <option value="Mikronesia"/>
                <option value="Moldova"/>
                <option value="Monako"/>
                <option value="Mongolia"/>
                <option value="Montenegro"/>
                <option value="Mozambik"/>
                <option value="Myanmar"/>
                <option value="Namibia"/>
                <option value="Nauru"/>
                <option value="Nepal"/>
                <option value="Niger"/>
                <option value="Nigeria"/>
                <option value="Nikaragua"/>
                <option value="Norwegia"/>
                <option value="Oman"/>
                <option value="Pakistan"/>
                <option value="Palau"/>
                <option value="Panama"/>
                <option value="Pantai Gading"/>
                <option value="Papua Nugini"/>
                <option value="Paraguay"/>
                <option value="Perancis"/>
                <option value="Peru"/>
                <option value="Polandia"/>
                <option value="Portugal"/>
                <option value="Qatar"/>
                <option value="Republik Demokratik Kongo"/>
                <option value="Republik Dominika"/>
                <option value="Rumania"/>
                <option value="Rusia"/>
                <option value="Rwanda"/>
                <option value="Saint Kitts and Nevis"/>
                <option value="Saint Lucia"/>
                <option value="Saint Vincent and the Grenadines"/>
                <option value="Samoa"/>
                <option value="San Marino"/>
                <option value="Sao Tome and Principe"/>
                <option value="Selandia Baru"/>
                <option value="Senegal"/>
                <option value="Serbia"/>
                <option value="Seychelles"/>
                <option value="Sierra Leone"/>
                <option value="Singapura"/>
                <option value="Siprus"/>
                <option value="Slovenia"/>
                <option value="Slowakia"/>
                <option value="Solomon"/>
                <option value="Somalia"/>
                <option value="Spanyol"/>
                <option value="Sri Lanka"/>
                <option value="Sudan"/>
                <option value="Sudan Selatan"/>
                <option value="Suriah"/>
                <option value="Suriname"/>
                <option value="Swaziland"/>
                <option value="Swedia"/>
                <option value="Swiss"/>
                <option value="Tajikistan"/>
                <option value="Tanjung Verde"/>
                <option value="Tanzania"/>
                <option value="Thailand"/>
                <option value="Timor Leste"/>
                <option value="Togo"/>
                <option value="Tonga"/>
                <option value="Trinidad and Tobago"/>
                <option value="Tunisia"/>
                <option value="Turki"/>
                <option value="Turkmenistan"/>
                <option value="Tuvalu"/>
                <option value="Uganda"/>
                <option value="Ukraina"/>
                <option value="Uni Emirat Arab"/>
                <option value="Uruguay"/>
                <option value="Uzbekistan"/>
                <option value="Vanuatu"/>
                <option value="Venezuela"/>
                <option value="Vietnam"/>
                <option value="Yaman"/>
                <option value="Yordania"/>
                <option value="Yunani"/>
                <option value="Zambia"/>
                <option value="Zimbabwe"/>
            </datalist>
        );
    }
}