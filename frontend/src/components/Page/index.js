import React, { Component, Fragment } from 'react';

const LEFT_PAGE = 'LEFT';
const RIGHT_PAGE = 'RIGHT';

/**
 * Helper method for creating a range of numbers
 * range(1, 5) => [1, 2, 3, 4, 5]
 */
const range = (from, to, step = 1) => {
  let i = from;
  const range = [];

  while (i <= to) {
  range.push(i);
  i += step;
  }

  return range;
}

export default class Page extends Component {
  constructor(props) {
    super(props);

    this.state = {
      totalRecords: 0,
      pageLimit: 5,
      pageNeighbours: 1,
      totalPages: 0,
      currentPage: 1,
    }

    this.fetchPageNumbers = this.fetchPageNumbers.bind(this);
  }

  componentDidMount() {
    let totalRecords = this.props.participants.length;
    let totalPages = Math.ceil(totalRecords/this.state.pageLimit);

    this.setState({
      totalRecords: totalRecords,
      totalPages: totalPages,
    })
  }

  fetchPageNumbers() {
    const { totalPages, currentPage, pageNeighbours } = this.state;
    const totalNumbers = (pageNeighbours*2) + 3;
    const totalBlocks = totalNumbers + 2;

    if (totalPages > totalBlocks) {
      const startPage = Math.max(2, currentPage - pageNeighbours);
      const endPage = Math.min(totalPages - 1, currentPage + pageNeighbours);

      let pages = range(startPage, endPage);

      const hasLeftSpill = startPage > 2;
      const hasRightSpill = (totalPages - endPage) > 1;
      const spillOffset = totalNumbers - (pages.length + 1);

      switch (true) {
        // handle: (1) < {5 6} [7] {8 9} (10)
        case (hasLeftSpill && !hasRightSpill): {
          const extraPages = range(startPage - spillOffset, startPage - 1);
          pages = [LEFT_PAGE, ...extraPages, ...pages];
          break;
        }

        // handle: (1) {2 3} [4] {5 6} > (10)
        case (!hasLeftSpill && hasRightSpill): {
          const extraPages = range(endPage + 1, endPage + spillOffset);
          pages = [...pages, ...extraPages, RIGHT_PAGE];
          break;
        }

        // handle: (1) < {4 5} [6] {7 8} > (10)
        case (hasLeftSpill && hasRightSpill):
        default: {
          pages = [LEFT_PAGE, ...pages, RIGHT_PAGE];
          break;
        }
      }

      return [1, ...pages, totalPages];
    }

    return range(1, totalPages);
  }

  gotoPage = page => {
    const { onPageChanged = f => f } = this.props;
    const { totalPages, pageLimit, totalRecords } = this.state;

    const currentPage = Math.max(0, Math.min(page, totalPages));

    const paginationData = {
      currentPage,
      totalPages,
      pageLimit,
      totalRecords,
    };

    this.setState({ currentPage }, () => onPageChanged(paginationData));
  }

  handleClick = page => evt => {
    evt.preventDefault();
    this.gotoPage(page);
  }

  handleMoveLeft = evt => {
    const { pageNeighbours, currentPage } = this.state;
    evt.preventDefault();
    this.gotoPage(currentPage - (pageNeighbours * 2) - 1);
  }

  handleMoveRight = evt => {
    const { pageNeighbours, currentPage } = this.state;
    evt.preventDefault();
    this.gotoPage(currentPage + (pageNeighbours * 2) + 1);
  }

  render() {
    const { totalRecords, totalPages, currentPage } = this.state;
    if (!totalRecords || totalPages === 1) {
      return(
        <div></div>
      )
    }

    const pages = this.fetchPageNumbers();

    return (
      <Fragment>
        <nav aria-label="Countries Pagination">
          <ul className="pagination">
            { pages.map((page, index) => {
              if (page === LEFT_PAGE) return (
                <li key={index} className="page-item">
                <a className="page-link" href="#" aria-label="Previous" onClick={this.handleMoveLeft}>
                  <span aria-hidden="true">&laquo;</span>
                  <span className="sr-only">Previous</span>
                </a>
                </li>
              );

              if (page === RIGHT_PAGE) return (
                <li key={index} className="page-item">
                <a className="page-link" href="#" aria-label="Next" onClick={this.handleMoveRight}>
                  <span aria-hidden="true">&raquo;</span>
                  <span className="sr-only">Next</span>
                </a>
                </li>
              );

              return (
                <li key={index} className={`page-item${ currentPage === page ? ' active' : ''}`}>
                <a className="page-link" href="#" onClick={ this.handleClick(page) }>{ page }</a>
                </li>
              );
            }) }

          </ul>
        </nav>
      </Fragment>
    )
  }
}