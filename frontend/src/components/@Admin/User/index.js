import React from 'react';
import { Redirect } from 'react-router-dom';

import './index.scss';
import editLogo from './../../../assets/photo/edit.png';

export default class User extends React.Component {

    state = {
        toLogin: false
    }

    constructor () {
        super();

        this.goToEdit = this.goToEdit.bind(this);
    }

    loadRoles() {
        let type = [
            'Investasi',
            'Program Sosial',
            'Komoditas dan Perdagangan',
            'Pengembangan Pariwisata',
            'Admin',
            'Manajer'
        ];

        let result = []
        this.props.user.roles.map(rol => {
            result.push(<div className='text-primary text-small font-weight-bold'>{type[rol - 1]}</div>)
        })

        return result;
    }

    goToEdit() {
        this.setState({
            toLogin: true
        });
    }

    render() {

        let url = '/admin/' + this.props.lingkup + '/edit/' + this.props.user.uid;
        if (this.state.toLogin) return <Redirect to={url}/>
        else return (
            <div className='box-user p-2 m-1 d-flex justify-content-between'>
                <div>
                    <div className='text-primary font-weight-semibold'>{this.props.user.name}</div>
                    <div className='text-primary text-small'>{this.props.user.username}</div>
                    <div className='text-primary text-small'>{this.props.user.email}</div>
                </div>
                <div>
                    {this.loadRoles()}
                </div>
                <div>
                    <button onClick={this.goToEdit} className='btn add-user mt-1'><img src={editLogo} className='edit-icon'/></button>
                </div>
            </div>
        );
    }
}