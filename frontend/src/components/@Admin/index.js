import React from 'react';
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

import NavigationBar from '../Navbar';
import Page from '../Page';
import User from './User';

import './index.scss';

export default class Admin extends React.Component {

    state = {
        user : null,
        users: null,
        currentUsers: [],
        toAddAccount : false,
        currentPage: 0,
        totalPages: 0
    }

    constructor () {
        super();

        this.goToAddAccount = this.goToAddAccount.bind(this);
        this.onPageChanged = this.onPageChanged.bind(this);

        const cookies = new Cookies();
        let uid = cookies.get('uid');
        let head = cookies.get('token');

        if(!uid) return;
        var url = process.env.REACT_APP_API_URL + `/api/v1/user/` + uid;

        let header = {};
        header['Content-Type'] = 'application/json';
        header['x-access-token'] = head;

        const getUser = {
            method: 'GET',
            headers: header,
            url
        };

        axios(getUser)
          .then((response) => {
            console.log(response);
            this.setState({
                user: response.data
            });
          })
          .catch((error) => {
              alert(error);
          });
        
        url = process.env.REACT_APP_API_URL + '/api/v1/user';
        let allUsers = {
            method: 'GET',
            headers: header,
            url
        }

        axios(allUsers)
          .then((response) => {
            console.log(response);
            let curr = response.data.length;
            let current = [];
            if (curr.length > 5) {
                current = response.data;
            } else current = response.data.slice(0, 5);
            this.setState({
                users: response.data,
                currentUsers: current
            });
          })
          .catch((error) => {
              alert(error);
          });

    }

    onPageChanged(data) {
        const { users } = this.state;
        const { currentPage, totalPages, pageLimit } = data;

        const offset = (currentPage - 1) * pageLimit;
        const currentUsers = users.slice(offset, offset + pageLimit);

        this.setState({
            currentPage: currentPage,
            currentUsers: currentUsers,
            totalPages: totalPages
          })
      
    }

    goToAddAccount() {
        this.setState({
            toAddAccount: true
        });
    }

    loadUsers() {
        let result = [];
        this.state.currentUsers.map(user => {
            result.push(<User lingkup={this.props.match.params.id_lingkup} user={user}></User>);
        });

        return result;
    }

    render() {

        const cookies = new Cookies();
        const tokenCookie = cookies.get('token');

        if (!tokenCookie) return <Redirect to='/login' />
        else if (this.state.user == null || this.state.users == null) return <div></div>
        else if (this.state.toAddAccount) {
            let going = '/admin/' + this.props.match.params.id_lingkup + '/add';
            return <Redirect to={going} />
        }

        if (!this.state.user.roles.includes(5)) return <Redirect to='/'/>

        let isUser = false;
        if (this.state.user.roles.includes(1)
            || this.state.user.roles.includes(2)
            || this.state.user.roles.includes(3)
            || this.state.user.roles.includes(4)) {
                isUser = true;
            }

        return (
            <div>
                <NavigationBar isUser={isUser} isAdmin={true} activeLingkup={this.props.match.params.id_lingkup} roles={this.state.user.roles} active="Admin"></NavigationBar>
                <div className='d-flex justify-content-center mt-3'>
                    <div className='d-flex m-4 three-quarter justify-content-between'>
                        <div className='text-accent font-weight-semibold text-xlarge'>Daftar Akun</div>
                        <button className='btn add-user' onClick={this.goToAddAccount}>Tambah Akun</button>
                    </div>
                </div>
                <div className='d-flex justify-content-center mt-3'>
                    <div className='three-quarter'>
                        {this.loadUsers()}
                    </div>
                </div>
                <div className='d-flex justify-content-center mt-3 mb-3'>
                    <Page participants={this.state.users} onPageChanged={this.onPageChanged}></Page>
                </div>
            </div>
        );
    }
}