exports.ROUTE_HOME = '/';
exports.ROUTE_ABOUT = '/about';
exports.ROUTE_LOGIN = '/login';
exports.ROUTE_PROYEK = '/proyek/:id_lingkup';
exports.ROUTE_ADMIN = '/admin/:id_lingkup';
exports.ROUTE_DASHBOARD = '/dashboard/:id_lingkup';
exports.ROUTE_OBSERVE = '/observe';

exports.ROUTE_ADD_USER = exports.ROUTE_ADMIN + '/add';
exports.ROUTE_EDIT_USER = exports.ROUTE_ADMIN + '/edit/:id_user';
exports.ROUTE_ADD_PROJECT = exports.ROUTE_PROYEK + '/add';
exports.ROUTE_EDIT_PROJECT = exports.ROUTE_PROYEK + '/edit/:id_project';
exports.ROUTE_DETAIL_PROJECT = exports.ROUTE_PROYEK + '/detail/:id_project';