import React, { Component } from 'react';
import { Route, Switch } from 'react-router';
import * as routes from './../routes';

import Login from './../components/@Login';
import Admin from './../components/@Admin';
import Dashboard from './../components/@Dashboard';
import Projects from './../components/@Projects';
import AddUser from './../components/@AddUser';
import EditUser from './../components/@EditUser';
import AddProject from './../components/@AddProject';
import EditProject from './../components/@EditProject';
import DetailProject from './../components/@DetailProject';
import Observe from './../components/@Observer';

export default class Router extends Component {
  render() {
    return (
      <Switch>
        <Route path={routes.ROUTE_OBSERVE} component={Observe} />
        <Route path={routes.ROUTE_ADD_USER} component={AddUser}/>
        <Route path={routes.ROUTE_EDIT_USER} component={EditUser}/>
        <Route path={routes.ROUTE_DETAIL_PROJECT} component={DetailProject}/>
        <Route path={routes.ROUTE_ADD_PROJECT} component={AddProject}/>
        <Route path={routes.ROUTE_EDIT_PROJECT} component={EditProject}/>
        <Route path={routes.ROUTE_LOGIN} component={Login}/>
        <Route path={routes.ROUTE_ADMIN} component={Admin}/>
        <Route path={routes.ROUTE_DASHBOARD} component={Dashboard}/>
        <Route path={routes.ROUTE_PROYEK} component={Projects}/>
        <Route path={routes.ROUTE_HOME} component={Dashboard} />
      </Switch>
    );
  }
}